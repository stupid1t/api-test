package com.bird;

import com.ulisesbocchio.jasyptspringboot.annotation.EnableEncryptableProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import tk.mybatis.spring.annotation.MapperScan;

/**
 * 启动程序
 *
 * @author bird
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableAspectJAutoProxy(exposeProxy = true)
@MapperScan("com.bird.**.mapper")
@EnableEncryptableProperties
public class BirdApplication {

	public static void main(String[] args) {

		SpringApplication.run(BirdApplication.class, args);
		System.out.println("(♥◠‿◠)ﾉﾞ  启动成功 ");
		// 密码加密生成
		//JasyptPBEStringEncryptionCLI.main(new String[]{"input=root", "password=bird", "algorithm=PBEWithMD5AndDES"});
	}
}
