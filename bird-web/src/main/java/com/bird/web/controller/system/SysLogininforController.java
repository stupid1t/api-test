package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.framework.domain.Result;
import com.bird.system.domain.entity.SysLogininforEntity;
import com.bird.system.domain.params.SysLogininforQueryParams;
import com.bird.system.service.ISysLogininforService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统访问记录
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/logininfor")
public class SysLogininforController extends BaseController {
	@Autowired
	private ISysLogininforService logininforService;

	@PreAuthorize("@ss.hasPermi('monitor:logininfor:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysLogininforEntity>> list(SysLogininforQueryParams logininfor) {
		startPage();
		List<SysLogininforEntity> list = logininforService.selectLogininforList(logininfor);
		return page(list);
	}

	@Log(title = "登陆日志", businessType = BusinessType.EXPORT)
	@PreAuthorize("@ss.hasPermi('monitor:logininfor:export')")
	@GetMapping("/export")
	public Result export(SysLogininforQueryParams logininfor) {
		List<SysLogininforEntity> list = logininforService.selectLogininforList(logininfor);
		return null;
	}

	@PreAuthorize("@ss.hasPermi('monitor:logininfor:remove')")
	@Log(title = "登陆日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{infoIds}")
	public Result remove(@PathVariable Long[] infoIds) {
		return ok(logininforService.deleteLogininforByIds(infoIds));
	}

	@PreAuthorize("@ss.hasPermi('monitor:logininfor:remove')")
	@Log(title = "登陆日志", businessType = BusinessType.CLEAN)
	@DeleteMapping("/clean")
	public Result clean() {
		logininforService.cleanLogininfor();
		return ok();
	}
}
