package com.bird.web.controller.system.api;

import com.bird.common.constant.Constants;
import com.bird.framework.domain.Result;
import com.bird.framework.security.service.WxLoginService;
import com.bird.system.domain.params.WxLoginBodyParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 登录验证
 *
 * @author bird
 */
@RestController
@RequestMapping(Constants.Api.MAP)
public class LoginController {

	@Autowired
	private WxLoginService loginService;

	/**
	 * 登录方法
	 *
	 * @return 结果
	 */
	@PostMapping("/login")
	public Result login(@RequestBody WxLoginBodyParam loginBody) {
		Result ajax = Result.ok();
		// 生成令牌
		String token = loginService.login(loginBody.getCode());
		ajax.put(Constants.System.TOKEN, token);
		return ajax;
	}
}
