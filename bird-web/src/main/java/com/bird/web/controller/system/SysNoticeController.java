package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.SysNoticeEntity;
import com.bird.system.domain.params.SysNoticeQueryParams;
import com.bird.system.domain.params.SysNoticeUpdateParams;
import com.bird.system.service.ISysNoticeService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 公告 信息操作处理
 *
 * @author bird
 */
@RestController
@RequestMapping("/system/notice")
public class SysNoticeController extends BaseController {
	@Autowired
	private ISysNoticeService noticeService;

	/**
	 * 获取通知公告列表
	 */
	@PreAuthorize("@ss.hasPermi('system:notice:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysNoticeEntity>> list(SysNoticeQueryParams notice) {
		startPage();
		List<SysNoticeEntity> list = noticeService.selectNoticeList(notice);
		return page(list);
	}

	/**
	 * 根据通知公告编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:notice:query')")
	@GetMapping(value = "/{noticeId}")
	public Result getInfo(@PathVariable Long noticeId) {
		return ok(noticeService.selectNoticeById(noticeId));
	}

	/**
	 * 新增通知公告
	 */
	@PreAuthorize("@ss.hasPermi('system:notice:add')")
	@Log(title = "通知公告", businessType = BusinessType.INSERT)
	@PostMapping
	public Result add(@Validated @RequestBody SysNoticeUpdateParams notice) {
		SysNoticeEntity covert = notice.covert();
		covert.setCreateBy(SecurityUtils.getUsername());
		return ok(noticeService.insertNotice(covert));
	}

	/**
	 * 修改通知公告
	 */
	@PreAuthorize("@ss.hasPermi('system:notice:edit')")
	@Log(title = "通知公告", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result edit(@Validated @RequestBody SysNoticeUpdateParams notice) {
		SysNoticeEntity covert = notice.covert();
		covert.setUpdateBy(SecurityUtils.getUsername());
		return ok(noticeService.updateNotice(covert));
	}

	/**
	 * 删除通知公告
	 */
	@PreAuthorize("@ss.hasPermi('system:notice:remove')")
	@Log(title = "通知公告", businessType = BusinessType.DELETE)
	@DeleteMapping("/{noticeId}")
	public Result remove(@PathVariable Long noticeId) {
		return ok(noticeService.deleteNoticeById(noticeId));
	}
}
