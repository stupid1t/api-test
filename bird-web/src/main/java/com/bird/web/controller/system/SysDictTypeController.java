package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.constant.Constants;
import com.bird.common.enums.BusinessType;
import com.bird.common.exception.CustomException;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.framework.utils.redis.RedisCache;
import com.bird.system.domain.entity.SysDictTypeEntity;
import com.bird.system.domain.params.SysDictTypeQueryParams;
import com.bird.system.domain.params.SysDictTypeUpdateParams;
import com.bird.system.service.ISysDictTypeService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;
import java.util.List;

/**
 * 数据字典信息
 *
 * @author bird
 */
@RestController
@RequestMapping("/system/dict/type")
public class SysDictTypeController extends BaseController {

	private static final cn.hutool.log.Log LOGGER = cn.hutool.log.Log.get();

	@Autowired
	private ISysDictTypeService dictTypeService;

	@Autowired
	private RedisCache redisCache;

	@PreAuthorize("@ss.hasPermi('system:dict:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysDictTypeEntity>> list(SysDictTypeQueryParams dictType) {
		startPage();
		List<SysDictTypeEntity> list = dictTypeService.selectDictTypeList(dictType);
		return page(list);
	}

	@Log(title = "字典类型", businessType = BusinessType.EXPORT)
	@PreAuthorize("@ss.hasPermi('system:dict:export')")
	@GetMapping("/export")
	public Result export(SysDictTypeQueryParams dictType) {
		List<SysDictTypeEntity> list = dictTypeService.selectDictTypeList(dictType);
		return null;
	}

	/**
	 * 查询字典类型详细
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:query')")
	@GetMapping(value = "/{dictId}")
	public Result getInfo(@PathVariable Long dictId) {
		return ok(dictTypeService.selectDictTypeById(dictId));
	}

	/**
	 * 新增字典类型
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:add')")
	@Log(title = "字典类型", businessType = BusinessType.INSERT)
	@PostMapping
	public Result add(@Validated @RequestBody SysDictTypeUpdateParams dict) {
		if (Constants.User.NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dict))) {
			throw new CustomException("新增字典'" + dict.getDictName() + "'失败，字典类型已存在");
		}
		SysDictTypeEntity covert = dict.covert();
		covert.setCreateBy(SecurityUtils.getUsername());
		return ok(dictTypeService.insertDictType(covert));
	}

	/**
	 * 修改字典类型
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:edit')")
	@Log(title = "字典类型", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result edit(@Validated @RequestBody SysDictTypeUpdateParams dict) {
		if (Constants.User.NOT_UNIQUE.equals(dictTypeService.checkDictTypeUnique(dict))) {
			throw new CustomException("修改字典'" + dict.getDictName() + "'失败，字典类型已存在");
		}
		SysDictTypeEntity covert = dict.covert();
		covert.setUpdateBy(SecurityUtils.getUsername());
		return ok(dictTypeService.updateDictType(covert));
	}

	/**
	 * 删除字典类型
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:remove')")
	@Log(title = "字典类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{dictIds}")
	public Result remove(@PathVariable Long[] dictIds) {
		return ok(dictTypeService.deleteDictTypeByIds(dictIds));
	}

	/**
	 * 获取字典选择框列表
	 */
	@GetMapping("/optionselect")
	public Result optionselect() {
		List<SysDictTypeEntity> dictTypes = dictTypeService.selectDictTypeAll();
		return ok(dictTypes);
	}

	/**
	 * 获取字典选择框列表
	 */
	@PostMapping("/cache/refresh")
	public Result<String> cacheRefresh() {
		Collection<String> keys = redisCache.keys(Constants.System.CACHE_KEY_PREFIX + Constants.System.CACHE_DICT + "*");
		keys.forEach(key -> {
			LOGGER.info("删除字典Key:{}", key);
			redisCache.deleteObject(key);
		});
		return ok("缓存刷新成功!");
	}
}
