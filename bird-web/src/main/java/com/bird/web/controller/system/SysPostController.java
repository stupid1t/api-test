package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.constant.Constants;
import com.bird.common.enums.BusinessType;
import com.bird.common.exception.CustomException;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.SysPostEntity;
import com.bird.system.domain.params.SysPostQueryParams;
import com.bird.system.domain.params.SysPostUpdateParams;
import com.bird.system.service.ISysPostService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 岗位信息操作处理
 *
 * @author bird
 */
@RestController
@RequestMapping("/system/post")
public class SysPostController extends BaseController {
	@Autowired
	private ISysPostService postService;

	/**
	 * 获取岗位列表
	 */
	@PreAuthorize("@ss.hasPermi('system:post:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysPostEntity>> list(SysPostQueryParams post) {
		startPage();
		List<SysPostEntity> list = postService.selectPostList(post);
		return page(list);
	}

	@Log(title = "岗位管理", businessType = BusinessType.EXPORT)
	@PreAuthorize("@ss.hasPermi('system:config:export')")
	@GetMapping("/export")
	public Result export(SysPostQueryParams post) {
		List<SysPostEntity> list = postService.selectPostList(post);
		return null;
	}

	/**
	 * 根据岗位编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:post:query')")
	@GetMapping(value = "/{postId}")
	public Result getInfo(@PathVariable Long postId) {
		return ok(postService.selectPostById(postId));
	}

	/**
	 * 新增岗位
	 */
	@PreAuthorize("@ss.hasPermi('system:post:add')")
	@Log(title = "岗位管理", businessType = BusinessType.INSERT)
	@PostMapping
	public Result add(@Validated @RequestBody SysPostUpdateParams post) {
		if (Constants.User.NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
			throw new CustomException("新增岗位'" + post.getPostName() + "'失败，岗位名称已存在");
		} else if (Constants.User.NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
			throw new CustomException("新增岗位'" + post.getPostName() + "'失败，岗位编码已存在");
		}
		SysPostEntity covert = post.covert();
		covert.setCreateBy(SecurityUtils.getUsername());
		return ok(postService.insertPost(covert));
	}

	/**
	 * 修改岗位
	 */
	@PreAuthorize("@ss.hasPermi('system:post:edit')")
	@Log(title = "岗位管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result edit(@Validated @RequestBody SysPostUpdateParams post) {
		if (Constants.User.NOT_UNIQUE.equals(postService.checkPostNameUnique(post))) {
			throw new CustomException("修改岗位'" + post.getPostName() + "'失败，岗位名称已存在");
		} else if (Constants.User.NOT_UNIQUE.equals(postService.checkPostCodeUnique(post))) {
			throw new CustomException("修改岗位'" + post.getPostName() + "'失败，岗位编码已存在");
		}
		SysPostEntity covert = post.covert();
		covert.setUpdateBy(SecurityUtils.getUsername());
		return ok(postService.updatePost(covert));
	}

	/**
	 * 删除岗位
	 */
	@PreAuthorize("@ss.hasPermi('system:post:remove')")
	@Log(title = "岗位管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{postIds}")
	public Result remove(@PathVariable Long[] postIds) {
		return ok(postService.deletePostByIds(postIds));
	}

	/**
	 * 获取岗位选择框列表
	 */
	@GetMapping("/optionselect")
	public Result optionselect() {
		List<SysPostEntity> posts = postService.selectPostAll();
		return ok(posts);
	}
}
