package com.bird.web.controller.quartz;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.framework.domain.Result;
import com.bird.quartz.common.exception.TaskException;
import com.bird.quartz.domain.entity.SysJobEntity;
import com.bird.quartz.service.ISysJobService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 调度任务信息操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/job")
public class SysJobController extends BaseController {
	@Autowired
	private ISysJobService jobService;

	/**
	 * 查询定时任务列表
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysJobEntity>> list(SysJobEntity sysJob) {
		startPage();
		List<SysJobEntity> list = jobService.selectJobList(sysJob);
		return page(list);
	}

	/**
	 * 获取定时任务详细信息
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:query')")
	@GetMapping(value = "/{jobId}")
	public Result getInfo(@PathVariable("jobId") Long jobId) {
		return ok(jobService.selectJobById(jobId));
	}

	/**
	 * 新增定时任务
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:add')")
	@Log(title = "定时任务", businessType = BusinessType.INSERT)
	@PostMapping
	public Result add(@RequestBody SysJobEntity sysJob) throws SchedulerException, TaskException {
		return ok(jobService.insertJob(sysJob));
	}

	/**
	 * 修改定时任务
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:edit')")
	@Log(title = "定时任务", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result edit(@RequestBody SysJobEntity sysJob) throws SchedulerException, TaskException {
		return ok(jobService.updateJob(sysJob));
	}

	/**
	 * 定时任务状态修改
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:changeStatus')")
	@Log(title = "定时任务", businessType = BusinessType.UPDATE)
	@PutMapping("/changeStatus")
	public Result changeStatus(@RequestBody SysJobEntity job) throws SchedulerException {
		SysJobEntity newJob = jobService.selectJobById(job.getJobId());
		newJob.setStatus(job.getStatus());
		return ok(jobService.changeStatus(newJob));
	}

	/**
	 * 定时任务立即执行一次
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:changeStatus')")
	@Log(title = "定时任务", businessType = BusinessType.UPDATE)
	@PutMapping("/run")
	public Result run(@RequestBody SysJobEntity job) throws SchedulerException {
		jobService.run(job);
		return ok();
	}

	/**
	 * 删除定时任务
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:remove')")
	@Log(title = "定时任务", businessType = BusinessType.DELETE)
	@DeleteMapping("/{jobIds}")
	public Result remove(@PathVariable Long[] jobIds) throws SchedulerException, TaskException {
		jobService.deleteJobByIds(jobIds);
		return ok();
	}
}
