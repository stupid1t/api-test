package com.bird.web.controller.system;

import com.bird.common.constant.Constants;
import com.bird.common.utils.ServletUtil;
import com.bird.framework.domain.Result;
import com.bird.framework.security.service.SysLoginService;
import com.bird.framework.security.service.SysPermissionService;
import com.bird.framework.security.service.TokenService;
import com.bird.system.domain.bo.LoginUser;
import com.bird.system.domain.entity.SysMenuEntity;
import com.bird.system.domain.entity.SysUserEntity;
import com.bird.system.domain.params.LoginBodyParam;
import com.bird.system.service.ISysMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;

/**
 * 登录验证
 *
 * @author bird
 */
@RestController
public class SysLoginController {
	@Autowired
	private SysLoginService loginService;

	@Autowired
	private ISysMenuService menuService;

	@Autowired
	private SysPermissionService permissionService;

	@Autowired
	private TokenService tokenService;

	/**
	 * 登录方法
	 *
	 * @return 结果
	 */
	@PostMapping("/login")
	public Result login(@RequestBody LoginBodyParam loginBody) {
		Result ajax = Result.ok();
		// 生成令牌
		String token = loginService.login(loginBody.getUsername(), loginBody.getPassword(), loginBody.getCode(),
				loginBody.getUuid());
		ajax.put(Constants.System.TOKEN, token);
		return ajax;
	}

	/**
	 * 获取用户信息
	 *
	 * @return 用户信息
	 */
	@GetMapping("getInfo")
	public Result getInfo() {
		LoginUser loginUser = tokenService.getLoginUser(ServletUtil.getRequest());
		SysUserEntity user = loginUser.getUser(SysUserEntity.class);
		// 角色集合
		Set<String> roles = permissionService.getRolePermission(user);
		// 权限集合
		Set<String> permissions = permissionService.getMenuPermission(user);
		Result ajax = Result.ok();
		ajax.put("user", user);
		ajax.put("roles", roles);
		ajax.put("permissions", permissions);
		return ajax;
	}

	/**
	 * 获取路由信息
	 *
	 * @return 路由信息
	 */
	@GetMapping("getRouters")
	public Result getRouters() {
		LoginUser loginUser = tokenService.getLoginUser(ServletUtil.getRequest());
		// 用户信息
		SysUserEntity user = loginUser.getUser(SysUserEntity.class);
		List<SysMenuEntity> menus = menuService.selectMenuTreeByUserId(user.getId());
		return Result.ok(menuService.buildMenus(menus));
	}
}
