package com.bird.web.controller.system;

import com.bird.framework.domain.Result;
import com.bird.framework.domain.server.Server;
import com.bird.web.controller.common.BaseController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 服务器监控
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/server")
public class ServerController extends BaseController {
	@PreAuthorize("@ss.hasPermi('monitor:server:list')")
	@GetMapping()
	public Result getInfo() throws Exception {
		Server server = new Server();
		server.copyTo();
		return ok(server);
	}
}
