package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.SysCodeRuleEntity;
import com.bird.system.domain.params.SysCodeRuleQueryParam;
import com.bird.system.domain.params.SysCodeRuleUpdateParam;
import com.bird.system.domain.vo.SysCodeRuleListPcVO;
import com.bird.system.service.ISysCodeRuleService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 系统编码规则配置Controller
 *
 * @author bird
 * @date 2020-07-22
 */
@Api(tags = "系统编码规则配置")
@RestController
@RequestMapping("/system/codeRule")
public class SysCodeRuleController extends BaseController {

	@Autowired
	private ISysCodeRuleService sysCodeRuleService;

	@ApiOperation("查询系统编码规则配置列表")
	@PreAuthorize("@ss.hasPermi('system:codeRule:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysCodeRuleListPcVO>> list(SysCodeRuleQueryParam queryParams) {
		startPage();
		List<SysCodeRuleListPcVO> list = sysCodeRuleService.selectListPC(queryParams);
		return page(list);
	}

	@ApiOperation("获取系统编码规则配置详细信息")
	@PreAuthorize("@ss.hasPermi('system:codeRule:query')")
	@GetMapping(value = "/{id}")
	public Result<SysCodeRuleEntity> info(@PathVariable("id") Long id) {
		return ok(sysCodeRuleService.selectById(id));
	}

	@ApiOperation("新增系统编码规则配置")
	@PreAuthorize("@ss.hasPermi('system:codeRule:add')")
	@Log(title = "系统编码规则配置", businessType = BusinessType.INSERT)
	@PostMapping
	public Result<Long> add(@RequestBody SysCodeRuleUpdateParam updateParam) {
		return ok(sysCodeRuleService.insertPC(updateParam, SecurityUtils.getLoginUser().getUser().getId()));
	}

	@ApiOperation("修改系统编码规则配置")
	@PreAuthorize("@ss.hasPermi('system:codeRule:edit')")
	@Log(title = "系统编码规则配置", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result<Integer> edit(@RequestBody SysCodeRuleUpdateParam updateParam) {
		return ok(sysCodeRuleService.updateByIdPC(updateParam));
	}

	@ApiOperation("删除系统编码规则配置")
	@PreAuthorize("@ss.hasPermi('system:codeRule:remove')")
	@Log(title = "系统编码规则配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public Result<Integer> remove(@PathVariable Long[] ids) {
		return ok(sysCodeRuleService.deleteByIds(ids));
	}
}
