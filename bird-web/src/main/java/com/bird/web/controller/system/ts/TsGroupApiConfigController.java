package com.bird.web.controller.system.ts;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.common.utils.covert.ConfigIdCovert;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.params.TsGroupApiConfigQueryParam;
import com.bird.system.domain.params.TsGroupApiConfigUpdateParam;
import com.bird.system.domain.vo.TsGroupApiConfigInfoPcVo;
import com.bird.system.domain.vo.TsGroupApiConfigListPcVO;
import com.bird.system.service.ITsGroupApiConfigService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 请求任务配置Controller
 *
 * @author bird
 * @date 2020-09-23
 */
@Api(tags = "请求任务配置")
@RestController
@RequestMapping("/system/reqConfig")
public class TsGroupApiConfigController extends BaseController {

	@Autowired
	private ITsGroupApiConfigService tsGroupApiConfigService;
	
	@ApiOperation("查询请求任务配置列表")
	@PreAuthorize("@ss.hasPermi('system:reqConfig:list')")
	@GetMapping("/list")
	public Result<PageInfo<TsGroupApiConfigListPcVO>> list(TsGroupApiConfigQueryParam queryParams) {
		startPage();
		queryParams.setCreSb(SecurityUtils.getLoginUser().getUserId());
		List<TsGroupApiConfigListPcVO> list = tsGroupApiConfigService.selectListPC(queryParams);
		return page(list);
	}

	@ApiOperation("获取请求任务配置详细信息")
	@PreAuthorize("@ss.hasPermi('system:reqConfig:query')")
	@GetMapping(value = "/{id}")
	public Result<TsGroupApiConfigInfoPcVo> info(@PathVariable("id") Long id) {
		return ok(tsGroupApiConfigService.selectByIdPc(id));
	}

	@ApiOperation("新增请求任务配置")
	@PreAuthorize("@ss.hasPermi('system:reqConfig:add')")
	@Log(title = "请求任务配置", businessType = BusinessType.INSERT)
	@PostMapping
	public Result<Long> add(@RequestBody TsGroupApiConfigUpdateParam updateParam) {
		return ok(tsGroupApiConfigService.insertPC(updateParam, SecurityUtils.getLoginUser().getUserId()));
	}

	@ApiOperation("修改请求任务配置")
	@PreAuthorize("@ss.hasPermi('system:reqConfig:edit')")
	@Log(title = "请求任务配置", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result<Integer> edit(@RequestBody TsGroupApiConfigUpdateParam updateParam) {
		return ok(tsGroupApiConfigService.updateByIdPC(updateParam, SecurityUtils.getLoginUser().getUserId()));
	}

	@ApiOperation("删除请求任务配置")
	@PreAuthorize("@ss.hasPermi('system:reqConfig:remove')")
	@Log(title = "请求任务配置", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public Result<Integer> remove(@PathVariable Long[] ids) {
		// 删除本地缓存
		for (Long id : ids) {
			ConfigIdCovert.existUser.remove(id);
		}
		return ok(tsGroupApiConfigService.deleteByIds(ids));
	}

	@ApiOperation("开始执行")
	@PreAuthorize("@ss.hasPermi('system:reqConfig:start')")
	@PostMapping(value = "/start/{id}")
	public Result start(@PathVariable("id") Long id) {
		tsGroupApiConfigService.start(id);
		return ok();
	}

	@ApiOperation("暂停执行")
	@PreAuthorize("@ss.hasPermi('system:reqConfig:pause')")
	@PostMapping(value = "/pause/{id}")
	public Result pause(@PathVariable("id") Long id) {
		tsGroupApiConfigService.pause(id);
		return ok();
	}

	@ApiOperation("继续执行")
	@PreAuthorize("@ss.hasPermi('system:reqConfig:continue')")
	@PostMapping(value = "/continue/{id}")
	public Result continueExcute(@PathVariable("id") Long id) {
		tsGroupApiConfigService.continueExcute(id);
		return ok();
	}

	@ApiOperation("终止执行")
	@PreAuthorize("@ss.hasPermi('system:reqConfig:stop')")
	@PostMapping(value = "/stop/{id}")
	public Result stop(@PathVariable("id") Long id) {
		tsGroupApiConfigService.stop(id);
		return ok();
	}
}
