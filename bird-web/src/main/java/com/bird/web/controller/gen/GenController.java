package com.bird.web.controller.gen;

import cn.hutool.core.convert.Convert;
import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.gen.domain.entity.GenTable;
import com.bird.gen.domain.entity.GenTableColumn;
import com.bird.gen.domain.params.GenTableQueryParams;
import com.bird.gen.domain.params.GenTableUpdateParams;
import com.bird.gen.service.IGenTableColumnService;
import com.bird.gen.service.IGenTableService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 代码生成 操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/tool/gen")
public class GenController extends BaseController {

	@Autowired
	private IGenTableService genTableService;

	@Autowired
	private IGenTableColumnService genTableColumnService;

	/**
	 * 查询代码生成列表
	 */
	@PreAuthorize("@ss.hasPermi('tool:gen:list')")
	@GetMapping("/list")
	public Result<PageInfo<GenTable>> genList(GenTableQueryParams genTable) {
		startPage();
		List<GenTable> list = genTableService.selectGenTableList(genTable);
		return page(list);
	}

	/**
	 * 修改代码生成业务
	 */
	@PreAuthorize("@ss.hasPermi('tool:gen:query')")
	@GetMapping(value = "/{talbleId}")
	public Result getInfo(@PathVariable Long talbleId) {
		GenTable table = genTableService.selectGenTableById(talbleId);
		List<GenTableColumn> list = genTableColumnService.selectGenTableColumnListByTableId(talbleId);
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("info", table);
		map.put("rows", list);
		return ok(map);
	}

	/**
	 * 查询数据库列表
	 */
	@PreAuthorize("@ss.hasPermi('tool:gen:list')")
	@GetMapping("/db/list")
	public Result<PageInfo<GenTable>> dataList(GenTableQueryParams genTable) {
		startPage();
		List<GenTable> list = genTableService.selectDbTableList(genTable);
		return page(list);
	}

	/**
	 * 查询数据表字段列表
	 */
	@PreAuthorize("@ss.hasPermi('tool:gen:list')")
	@GetMapping(value = "/column/{talbleId}")
	public Result<PageInfo<GenTableColumn>> columnList(Long tableId) {
		List<GenTableColumn> list = genTableColumnService.selectGenTableColumnListByTableId(tableId);
		return page(list);
	}

	/**
	 * 导入表结构（保存）
	 */
	@PreAuthorize("@ss.hasPermi('tool:gen:list')")
	@Log(title = "代码生成", businessType = BusinessType.IMPORT)
	@PostMapping("/importTable")
	public Result importTableSave(String tables) {
		String[] tableNames = Convert.toStrArray(tables);
		// 查询表信息
		List<GenTable> tableList = genTableService.selectDbTableListByNames(tableNames);
		genTableService.importGenTable(tableList, SecurityUtils.getUsername());
		return ok();
	}

	/**
	 * 修改保存代码生成业务
	 */
	@PreAuthorize("@ss.hasPermi('tool:gen:edit')")
	@Log(title = "代码生成", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result editSave(@Validated @RequestBody GenTableUpdateParams genTable) {
		System.out.println(genTable.getParams().size());
		genTableService.validateEdit(genTable);
		genTableService.updateGenTable(genTable.covert());
		return ok();
	}

	@PreAuthorize("@ss.hasPermi('tool:gen:remove')")
	@Log(title = "代码生成", businessType = BusinessType.DELETE)
	@DeleteMapping("/{tableIds}")
	public Result remove(@PathVariable Long[] tableIds) {
		genTableService.deleteGenTableByIds(tableIds);
		return ok();
	}

	/**
	 * 预览代码
	 */
	@PreAuthorize("@ss.hasPermi('tool:gen:preview')")
	@GetMapping("/preview/{tableId}")
	public Result preview(@PathVariable("tableId") Long tableId) throws IOException {
		Map<String, String> dataMap = genTableService.previewCode(tableId);
		return ok(dataMap);
	}

	/**
	 * 生成代码
	 */
	@PreAuthorize("@ss.hasPermi('tool:gen:code')")
	@Log(title = "代码生成", businessType = BusinessType.GENCODE)
	@GetMapping("/genCode/{tableName}")
	public void genCode(HttpServletResponse response, @PathVariable("tableName") String tableName) throws IOException {
		byte[] data = genTableService.generatorCode(tableName);
		genCode(response, data);
	}

	/**
	 * 批量生成代码
	 */
	@PreAuthorize("@ss.hasPermi('tool:gen:code')")
	@Log(title = "代码生成", businessType = BusinessType.GENCODE)
	@GetMapping("/batchGenCode")
	public void batchGenCode(HttpServletResponse response, String tables) throws IOException {
		String[] tableNames = Convert.toStrArray(tables);
		byte[] data = genTableService.generatorCode(tableNames);
		genCode(response, data);
	}

	/**
	 * 生成zip文件
	 */
	private void genCode(HttpServletResponse response, byte[] data) throws IOException {
		response.reset();
		response.setHeader("Content-Disposition", "attachment; filename=\"ruoyi.zip\"");
		response.addHeader("Content-Length", "" + data.length);
		response.setContentType("application/octet-stream; charset=UTF-8");
		IOUtils.write(data, response.getOutputStream());
	}
}