package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.framework.domain.Result;
import com.bird.system.domain.entity.SysOperLogEntity;
import com.bird.system.domain.params.SysOperLogQueryParams;
import com.bird.system.service.ISysOperLogService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 操作日志记录
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/operlog")
public class SysOperlogController extends BaseController {
	@Autowired
	private ISysOperLogService operLogService;

	@PreAuthorize("@ss.hasPermi('monitor:operlog:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysOperLogEntity>> list(SysOperLogQueryParams operLog) {
		startPage();
		List<SysOperLogEntity> list = operLogService.selectOperLogList(operLog);
		return page(list);
	}

	@Log(title = "操作日志", businessType = BusinessType.EXPORT)
	@PreAuthorize("@ss.hasPermi('monitor:operlog:export')")
	@GetMapping("/export")
	public Result export(SysOperLogQueryParams operLog) {
		List<SysOperLogEntity> list = operLogService.selectOperLogList(operLog);
		return null;
	}

	@PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
	@DeleteMapping("/{operIds}")
	public Result remove(@PathVariable Long[] operIds) {
		return ok(operLogService.deleteOperLogByIds(operIds));
	}

	@Log(title = "操作日志", businessType = BusinessType.CLEAN)
	@PreAuthorize("@ss.hasPermi('monitor:operlog:remove')")
	@DeleteMapping("/clean")
	public Result clean() {
		operLogService.cleanOperLog();
		return ok();
	}
}
