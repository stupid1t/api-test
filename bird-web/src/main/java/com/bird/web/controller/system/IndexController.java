package com.bird.web.controller.system;

import com.alibaba.fastjson.JSONObject;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.service.ICommonService;
import com.bird.web.controller.common.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

/**
 * 服务器监控
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/system/index")
public class IndexController extends BaseController {

	@Autowired
	private ICommonService commonService;

	@GetMapping("/info")
	public Result getInfo() throws Exception {
		Map<String, Object> params = new HashMap<>();
		params.put("userId", SecurityUtils.getLoginUser().getUserId());
		String sql = " select " +
				" (select count(*) from ts_phone_data where cre_sb = #{params.userId}) totalData," +
				" (select count(*) from ts_group_api where cre_sb = #{params.userId}) totalApi," +
				" (select count(*) from ts_group_api_config where cre_sb = #{params.userId}) totalConfig," +
				" (select count(*) from ts_group_api_config where cre_sb = #{params.userId} and req_status = 3) totalConfigIng";
		JSONObject data = commonService.queryJson(sql, params);
		return ok(data);
	}
}
