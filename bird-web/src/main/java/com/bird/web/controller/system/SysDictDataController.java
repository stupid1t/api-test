package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.SysDictDataEntity;
import com.bird.system.domain.params.SysDictDataQueryParams;
import com.bird.system.domain.params.SysDictDataUpdateParams;
import com.bird.system.service.ISysDictDataService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 数据字典信息
 *
 * @author bird
 */
@RestController
@RequestMapping("/system/dict/data")
public class SysDictDataController extends BaseController {
	@Autowired
	private ISysDictDataService dictDataService;

	@PreAuthorize("@ss.hasPermi('system:dict:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysDictDataEntity>> list(SysDictDataQueryParams dictData) {
		startPage();
		List<SysDictDataEntity> list = dictDataService.selectDictDataList(dictData);
		return page(list);
	}

	@Log(title = "字典数据", businessType = BusinessType.EXPORT)
	@PreAuthorize("@ss.hasPermi('system:dict:export')")
	@GetMapping("/export")
	public Result export(SysDictDataQueryParams dictData) {
		List<SysDictDataEntity> list = dictDataService.selectDictDataList(dictData);
		return null;
	}

	/**
	 * 查询字典数据详细
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:query')")
	@GetMapping(value = "/{dictCode}")
	public Result getInfo(@PathVariable Long dictCode) {
		return ok(dictDataService.selectDictDataById(dictCode));
	}

	/**
	 * 根据字典类型查询字典数据信息
	 */
	@GetMapping(value = "/dictType/{dictType}")
	public Result dictType(@PathVariable String dictType) {
		return ok(dictDataService.selectDictDataByType(dictType));
	}

	/**
	 * 新增字典类型
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:add')")
	@Log(title = "字典数据", businessType = BusinessType.INSERT)
	@PostMapping
	public Result add(@Validated @RequestBody SysDictDataUpdateParams dict) {
		SysDictDataEntity covert = dict.covert();
		covert.setCreateBy(SecurityUtils.getUsername());
		return ok(dictDataService.insertDictData(covert));
	}

	/**
	 * 修改保存字典类型
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:edit')")
	@Log(title = "字典数据", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result edit(@Validated @RequestBody SysDictDataUpdateParams dict) {
		SysDictDataEntity covert = dict.covert();
		covert.setUpdateBy(SecurityUtils.getUsername());
		return ok(dictDataService.updateDictData(covert));
	}

	/**
	 * 删除字典类型
	 */
	@PreAuthorize("@ss.hasPermi('system:dict:remove')")
	@Log(title = "字典类型", businessType = BusinessType.DELETE)
	@DeleteMapping("/{dictCodes}")
	public Result remove(@PathVariable Long[] dictCodes) {
		return ok(dictDataService.deleteDictDataByIds(dictCodes));
	}
}
