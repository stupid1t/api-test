package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.framework.domain.Result;
import com.bird.system.domain.entity.SysErrorLogEntity;
import com.bird.system.domain.params.SysErrorLogQueryParam;
import com.bird.system.domain.vo.SysErrorLogListPcVO;
import com.bird.system.service.ISysErrorLogService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import com.github.stupdit1t.excel.Column;
import com.github.stupdit1t.excel.ExcelUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 异常日志Controller
 *
 * @author bird
 * @date 2020-07-15
 */
@Api(tags = "异常日志")
@RestController
@RequestMapping("/system/errorLog")
public class SysErrorLogController extends BaseController {

	@Autowired
	private ISysErrorLogService sysErrorLogService;

	@ApiOperation("查询异常日志列表")
	@PreAuthorize("@ss.hasPermi('system:errorLog:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysErrorLogListPcVO>> list(SysErrorLogQueryParam queryParams) {
		startPage();
		List<SysErrorLogListPcVO> list = sysErrorLogService.selectListPC(queryParams);
		return page(list);
	}

	@ApiOperation("导出异常日志")
	@Log(title = "异常日志", businessType = BusinessType.EXPORT)
	@GetMapping("/export/excel")
	public void export(SysErrorLogQueryParam queryParams) {
		Column[] columns = {
				Column.field("requestUri"),
				Column.field("requestMethod"),
				Column.field("ip"),
				Column.field("errorSimpleInfo"),
				Column.field("createDate"),
				Column.field("creatorName"),
		};
		String[] header = {"请求地址", "请求方式", "IP地址", "简要信息", "异常时间", "创建人"};
		Workbook workbook = ExcelUtils.createWorkbook(sysErrorLogService.selectListPC(queryParams), ExcelUtils.ExportRules.simpleRule(columns, header), true);
		export(workbook, "异常日志.xlsx");
	}

	@ApiOperation("获取异常日志详细信息")
	@PreAuthorize("@ss.hasPermi('system:errorLog:query')")
	@GetMapping(value = "/{id}")
	public Result<SysErrorLogEntity> info(@PathVariable("id") Long id) {
		return ok(sysErrorLogService.selectById(id));
	}

	@ApiOperation("删除异常日志")
	@PreAuthorize("@ss.hasPermi('system:errorLog:remove')")
	@Log(title = "异常日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public Result<Integer> remove(@PathVariable Long[] ids) {
		return ok(sysErrorLogService.deleteByIds(ids));
	}


}
