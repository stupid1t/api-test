package com.bird.web.controller.system;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.annotation.Log;
import com.bird.common.constant.Constants;
import com.bird.common.enums.BusinessType;
import com.bird.common.utils.StringUtil;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.redis.RedisCache;
import com.bird.system.domain.bo.LoginUser;
import com.bird.system.domain.entity.SysUserOnlineEntity;
import com.bird.system.enums.LoginTypeEnum;
import com.bird.system.service.ISysUserOnlineService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * 在线用户监控
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/online")
public class SysUserOnlineController extends BaseController {
	@Autowired
	private ISysUserOnlineService userOnlineService;

	@Autowired
	private RedisCache redisCache;

	@PreAuthorize("@ss.hasPermi('monitor:online:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysUserOnlineEntity>> list(String ipaddr, String userName) {
		Collection<String> keys = redisCache.keys(Constants.System.CACHE_KEY_PREFIX + Constants.System.LOGIN_TOKEN_KEY + "*");
		List<SysUserOnlineEntity> userOnlineList = new ArrayList<SysUserOnlineEntity>();
		for (String key : keys) {
			LoginUser user = redisCache.getCacheObject(key);
			if (StringUtil.isNotEmpty(ipaddr) && StringUtil.isNotEmpty(userName)) {
				if (StringUtil.equals(ipaddr, user.getIpaddr()) && StringUtil.equals(userName, user.getUsername())) {
					userOnlineList.add(userOnlineService.selectOnlineByInfo(ipaddr, userName, user));
				}
			} else if (StringUtil.isNotEmpty(ipaddr)) {
				if (StringUtil.equals(ipaddr, user.getIpaddr())) {
					userOnlineList.add(userOnlineService.selectOnlineByIpaddr(ipaddr, user));
				}
			} else if (StringUtil.isNotEmpty(userName) && ObjectUtil.isNotNull(user.getUser())) {
				if (StringUtil.equals(userName, user.getUsername())) {
					userOnlineList.add(userOnlineService.selectOnlineByUserName(userName, user));
				}
			} else {
				userOnlineList.add(userOnlineService.loginUserToUserOnline(user));
			}
		}
		Collections.reverse(userOnlineList);
		userOnlineList.removeAll(Collections.singleton(null));
		return page(userOnlineList);
	}

	/**
	 * 强退用户
	 */
	@PreAuthorize("@ss.hasPermi('monitor:online:forceLogout')")
	@Log(title = "在线用户", businessType = BusinessType.DELETE)
	@DeleteMapping("/{tokenId}")
	public Result forceLogout(@PathVariable String tokenId) {

		LoginTypeEnum[] loginTypeEnums = LoginTypeEnum.values();
		for (LoginTypeEnum loginTypeEnum : loginTypeEnums) {
			String key = Constants.System.CACHE_KEY_PREFIX + Constants.System.LOGIN_TOKEN_KEY + loginTypeEnum.name() + ":" + tokenId;
			redisCache.deleteObject(key);
		}
		return ok();
	}
}
