package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.constant.Constants;
import com.bird.common.enums.BusinessType;
import com.bird.common.exception.CustomException;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.SysConfigEntity;
import com.bird.system.domain.params.SysConfigQueryParams;
import com.bird.system.domain.params.SysConfigUpdateParams;
import com.bird.system.service.ISysConfigService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 参数配置 信息操作处理
 *
 * @author bird
 */
@RestController
@RequestMapping("/system/config")
public class SysConfigController extends BaseController {

	@Autowired
	private ISysConfigService configService;

	/**
	 * 获取参数配置列表
	 */
	@PreAuthorize("@ss.hasPermi('system:config:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysConfigEntity>> list(SysConfigQueryParams config) {
		startPage();
		List<SysConfigEntity> list = configService.selectConfigList(config);
		return page(list);
	}

	@Log(title = "参数管理", businessType = BusinessType.EXPORT)
	@PreAuthorize("@ss.hasPermi('system:config:export')")
	@GetMapping("/export")
	public Result export(SysConfigQueryParams config) {
		List<SysConfigEntity> list = configService.selectConfigList(config);
		return null;
	}

	/**
	 * 根据参数编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:config:query')")
	@GetMapping(value = "/{configId}")
	public Result getInfo(@PathVariable Long configId) {
		return ok(configService.selectById(configId));
	}

	/**
	 * 根据参数键名查询参数值
	 */
	@GetMapping(value = "/configKey/{configKey}")
	public Result getConfigKey(@PathVariable String configKey) {
		return ok((Object) configService.selectConfigByKey(configKey));
	}

	/**
	 * 新增参数配置
	 */
	@PreAuthorize("@ss.hasPermi('system:config:add')")
	@Log(title = "参数管理", businessType = BusinessType.INSERT)
	@PostMapping
	public Result add(@Validated @RequestBody SysConfigUpdateParams config) {
		if (Constants.User.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
			throw new CustomException("新增参数'" + config.getConfigName() + "'失败，参数键名已存在");
		}
		config.setCreateBy(SecurityUtils.getUsername());
		return ok(configService.insert(config.covert()));
	}

	/**
	 * 修改参数配置
	 */
	@PreAuthorize("@ss.hasPermi('system:config:edit')")
	@Log(title = "参数管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result edit(@Validated @RequestBody SysConfigUpdateParams config) {
		if (Constants.User.NOT_UNIQUE.equals(configService.checkConfigKeyUnique(config))) {
			throw new CustomException("修改参数'" + config.getConfigName() + "'失败，参数键名已存在");
		}
		config.setUpdateBy(SecurityUtils.getUsername());
		return ok(configService.updateById(config.covert()));
	}

	/**
	 * 删除参数配置
	 */
	@PreAuthorize("@ss.hasPermi('system:config:remove')")
	@Log(title = "参数管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{configIds}")
	public Result remove(@PathVariable Long[] configIds) {
		return ok(configService.deleteByIds(configIds));
	}
}
