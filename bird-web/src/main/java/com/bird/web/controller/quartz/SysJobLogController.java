package com.bird.web.controller.quartz;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.framework.domain.Result;
import com.bird.quartz.domain.entity.SysJobLogEntity;
import com.bird.quartz.service.ISysJobLogService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 调度日志操作处理
 *
 * @author ruoyi
 */
@RestController
@RequestMapping("/monitor/jobLog")
public class SysJobLogController extends BaseController {
	@Autowired
	private ISysJobLogService jobLogService;

	/**
	 * 查询定时任务调度日志列表
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysJobLogEntity>> list(SysJobLogEntity sysJobLog) {
		startPage();
		List<SysJobLogEntity> list = jobLogService.selectJobLogList(sysJobLog);
		return page(list);
	}

	/**
	 * 根据调度编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:query')")
	@GetMapping(value = "/{configId}")
	public Result getInfo(@PathVariable Long jobLogId) {
		return ok(jobLogService.selectJobLogById(jobLogId));
	}


	/**
	 * 删除定时任务调度日志
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:remove')")
	@Log(title = "定时任务调度日志", businessType = BusinessType.DELETE)
	@DeleteMapping("/{jobLogIds}")
	public Result remove(@PathVariable Long[] jobLogIds) {
		return ok(jobLogService.deleteJobLogByIds(jobLogIds));
	}

	/**
	 * 清空定时任务调度日志
	 */
	@PreAuthorize("@ss.hasPermi('monitor:job:remove')")
	@Log(title = "调度日志", businessType = BusinessType.CLEAN)
	@DeleteMapping("/clean")
	public Result clean() {
		jobLogService.cleanJobLog();
		return ok();
	}
}
