package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.common.utils.Assert;
import com.bird.common.utils.ServletUtil;
import com.bird.framework.config.BirdConfig;
import com.bird.framework.domain.Result;
import com.bird.framework.security.service.TokenService;
import com.bird.framework.utils.FileUploadUtils;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.bo.LoginUser;
import com.bird.system.domain.entity.SysUserEntity;
import com.bird.system.service.ISysUserService;
import com.bird.web.controller.common.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * 个人信息 业务处理
 *
 * @author bird
 */
@RestController
@RequestMapping("/system/user/profile")
public class SysProfileController extends BaseController {
	@Autowired
	private ISysUserService userService;

	@Autowired
	private TokenService tokenService;

	/**
	 * 个人信息
	 */
	@GetMapping
	public Result profile() {
		LoginUser loginUser = tokenService.getLoginUser(ServletUtil.getRequest());
		SysUserEntity user = loginUser.getUser(SysUserEntity.class);
		Result ajax = ok();
		ajax.put("user", userService.selectUserById(user.getId()));
		ajax.put("roleGroup", userService.selectUserRoleGroup(loginUser.getUsername()));
		ajax.put("postGroup", userService.selectUserPostGroup(loginUser.getUsername()));
		return ajax;
	}

	/**
	 * 修改用户
	 */
	@Log(title = "个人信息", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result updateProfile(@RequestBody SysUserEntity user) {
		return ok(userService.updateUserProfile(user));
	}

	/**
	 * 重置密码
	 */
	@Log(title = "个人信息", businessType = BusinessType.UPDATE)
	@PutMapping("/updatePwd")
	public Result updatePwd(String oldPassword, String newPassword) {
		LoginUser loginUser = tokenService.getLoginUser(ServletUtil.getRequest());
		String userName = loginUser.getUsername();
		String password = loginUser.getPassword();
		Assert.isFalse(SecurityUtils.matchesPassword(oldPassword, password), "修改密码失败，旧密码错误");
		Assert.isTrue(SecurityUtils.matchesPassword(newPassword, password), "新密码不能与旧密码相同");
		return ok(userService.resetUserPwd(userName, SecurityUtils.encryptPassword(newPassword)));
	}

	/**
	 * 头像上传
	 */
	@Log(title = "用户头像", businessType = BusinessType.UPDATE)
	@PostMapping("/avatar")
	public Result avatar(@RequestParam("avatarfile") MultipartFile file) throws IOException {
		if (!file.isEmpty()) {
			LoginUser loginUser = tokenService.getLoginUser(ServletUtil.getRequest());
			String avatar = FileUploadUtils.upload(BirdConfig.getAvatarPath(), file);
			if (userService.updateUserAvatar(loginUser.getUsername(), avatar)) {
				Result ajax = ok();
				ajax.put("imgUrl", avatar);
				loginUser.getUser(SysUserEntity.class).setAvatar(avatar);
				tokenService.setLoginUser(loginUser);
				return ajax;
			}
		}
		return no("上传图片异常，请联系管理员");
	}
}
