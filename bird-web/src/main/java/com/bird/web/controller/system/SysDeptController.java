package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.constant.Constants;
import com.bird.common.enums.BusinessType;
import com.bird.common.exception.CustomException;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.SysDeptEntity;
import com.bird.system.domain.params.SysDeptQueryParams;
import com.bird.system.domain.params.SysDeptUpdateParams;
import com.bird.system.service.ISysDeptService;
import com.bird.web.controller.common.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 部门信息
 *
 * @author bird
 */
@RestController
@RequestMapping("/system/dept")
public class SysDeptController extends BaseController {
	@Autowired
	private ISysDeptService deptService;

	/**
	 * 获取部门列表
	 */
	@PreAuthorize("@ss.hasPermi('system:dept:list')")
	@GetMapping("/list")
	public Result list(SysDeptQueryParams dept) {
		List<SysDeptEntity> depts = deptService.selectDeptList(dept);
		return ok(deptService.buildDeptTree(depts));
	}

	/**
	 * 根据部门编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:dept:query')")
	@GetMapping(value = "/{deptId}")
	public Result getInfo(@PathVariable Long deptId) {
		return ok(deptService.selectById(deptId));
	}

	/**
	 * 获取部门下拉树列表
	 */
	@GetMapping("/treeselect")
	public Result treeselect(SysDeptQueryParams dept) {
		List<SysDeptEntity> depts = deptService.selectDeptList(dept);
		return ok(deptService.buildDeptTreeSelect(depts));
	}

	/**
	 * 加载对应角色部门列表树
	 */
	@GetMapping(value = "/roleDeptTreeselect/{roleId}")
	public Result roleDeptTreeselect(@PathVariable("roleId") Long roleId) {
		return ok(deptService.selectDeptListByRoleId(roleId));
	}

	/**
	 * 新增部门
	 */
	@PreAuthorize("@ss.hasPermi('system:dept:add')")
	@Log(title = "部门管理", businessType = BusinessType.INSERT)
	@PostMapping
	public Result add(@Validated @RequestBody SysDeptUpdateParams dept) {
		if (Constants.User.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept))) {
			return ok("新增部门'" + dept.getDeptName() + "'失败，部门名称已存在");
		}
		SysDeptEntity covert = dept.covert();
		covert.setCreateBy(SecurityUtils.getUsername());
		return ok(deptService.insertDept(covert));
	}

	/**
	 * 修改部门
	 */
	@PreAuthorize("@ss.hasPermi('system:dept:edit')")
	@Log(title = "部门管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result edit(@Validated @RequestBody SysDeptUpdateParams dept) {
		if (Constants.User.NOT_UNIQUE.equals(deptService.checkDeptNameUnique(dept))) {
			throw new CustomException("修改部门'" + dept.getDeptName() + "'失败，部门名称已存在");
		} else if (dept.getParentId().equals(dept.getId())) {
			throw new CustomException("修改部门'" + dept.getDeptName() + "'失败，上级部门不能是自己");
		}
		SysDeptEntity covert = dept.covert();
		covert.setUpdateBy(SecurityUtils.getUsername());
		return ok(deptService.updateDept(covert));
	}

	/**
	 * 删除部门
	 */
	@PreAuthorize("@ss.hasPermi('system:dept:remove')")
	@Log(title = "部门管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{deptId}")
	public Result remove(@PathVariable Long deptId) {
		if (deptService.hasChildByDeptId(deptId)) {
			throw new CustomException("存在下级部门,不允许删除");
		}
		if (deptService.checkDeptExistUser(deptId)) {
			throw new CustomException("部门存在用户,不允许删除");
		}
		return ok(deptService.deleteById(deptId));
	}
}
