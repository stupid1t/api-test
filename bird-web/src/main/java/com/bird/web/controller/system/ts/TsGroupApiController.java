package com.bird.web.controller.system.ts;

import com.bird.common.annotation.Log;
import com.bird.common.constant.ValidRule;
import com.bird.common.enums.BusinessType;
import com.bird.common.utils.covert.ApiIdCovert;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.TsGroupApiEntity;
import com.bird.system.domain.params.TsGroupApiQueryParam;
import com.bird.system.domain.params.TsGroupApiUpdateParam;
import com.bird.system.domain.vo.TsGroupApiInfoPcVO;
import com.bird.system.domain.vo.TsGroupApiListPcVO;
import com.bird.system.service.ITsGroupApiService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 组接口Controller
 *
 * @author bird
 * @date 2020-09-23
 */
@Api(tags = "组接口")
@RestController
@Validated
@RequestMapping("/system/reqApi")
public class TsGroupApiController extends BaseController {

	@Autowired
	private ITsGroupApiService tsGroupApiService;

	@ApiOperation("查询组接口列表")
	@PreAuthorize("@ss.hasPermi('system:api:list')")
	@GetMapping("/list")
	public Result<PageInfo<TsGroupApiListPcVO>> list(TsGroupApiQueryParam queryParams) {
		startPage();
		queryParams.setUserId(SecurityUtils.getLoginUser().getUserId());
		List<TsGroupApiListPcVO> list = tsGroupApiService.selectListPC(queryParams);
		return page(list);
	}

	@ApiOperation("获取组接口详细信息")
	@PreAuthorize("@ss.hasPermi('system:api:query')")
	@GetMapping(value = "/{id}")
	public Result<TsGroupApiInfoPcVO> info(@PathVariable("id") Long id) {
		TsGroupApiEntity tsGroupApiInfoPcVO = tsGroupApiService.selectByIdPc(id);
		return ok(tsGroupApiInfoPcVO);
	}

	@ApiOperation("新增组接口")
	@PreAuthorize("@ss.hasPermi('system:api:add')")
	@Log(title = "组接口", businessType = BusinessType.INSERT)
	@PostMapping
	public Result<Long> add(@RequestBody @Validated(ValidRule.Add.class) TsGroupApiUpdateParam updateParam) {
		return ok(tsGroupApiService.insertPC(updateParam, SecurityUtils.getLoginUser().getUserId()));
	}

	@ApiOperation("修改组接口")
	@PreAuthorize("@ss.hasPermi('system:api:edit')")
	@Log(title = "组接口", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result<Integer> edit(@RequestBody @Validated(ValidRule.Update.class) TsGroupApiUpdateParam updateParam) {
		return ok(tsGroupApiService.updateByIdPC(updateParam));
	}

	@ApiOperation("删除组接口")
	@PreAuthorize("@ss.hasPermi('system:api:remove')")
	@Log(title = "组接口", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public Result<Integer> remove(@PathVariable Long[] ids) {
		// 删除本地缓存
		for (Long id : ids) {
			ApiIdCovert.existUser.remove(id);
		}
		return ok(tsGroupApiService.deleteByIds(ids));
	}
}
