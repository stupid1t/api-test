package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.constant.Coder;
import com.bird.common.constant.Constants;
import com.bird.common.enums.BusinessType;
import com.bird.common.exception.CustomException;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.SysRoleEntity;
import com.bird.system.domain.params.SysRoleQueryParams;
import com.bird.system.domain.params.SysRoleUpdateParams;
import com.bird.system.service.ISysRoleService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 角色信息
 *
 * @author bird
 */
@RestController
@RequestMapping("/system/role")
public class SysRoleController extends BaseController {
	@Autowired
	private ISysRoleService roleService;

	@PreAuthorize("@ss.hasPermi('system:role:list')")
	@GetMapping("/list")
	public Result<PageInfo<SysRoleEntity>> list(SysRoleQueryParams role) {
		startPage();
		List<SysRoleEntity> list = roleService.selectRoleList(role);
		return page(list);
	}

	@Log(title = "角色管理", businessType = BusinessType.EXPORT)
	@PreAuthorize("@ss.hasPermi('system:role:export')")
	@GetMapping("/export")
	public Result export(SysRoleQueryParams role) {
		List<SysRoleEntity> list = roleService.selectRoleList(role);
		return null;
	}

	/**
	 * 根据角色编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:role:query')")
	@GetMapping(value = "/{roleId}")
	public Result getInfo(@PathVariable Long roleId) {
		return ok(roleService.selectRoleById(roleId));
	}

	/**
	 * 新增角色
	 */
	@PreAuthorize("@ss.hasPermi('system:role:add')")
	@Log(title = "角色管理", businessType = BusinessType.INSERT)
	@PostMapping
	public Result add(@Validated @RequestBody SysRoleUpdateParams role) {
		if (Constants.User.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
			throw new CustomException(Coder.SysUser.THE_ROLE_NAME_ALREADY_EXISTS.build("新增", role.getRoleName()));
		} else if (Constants.User.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
			throw new CustomException(Coder.SysUser.THE_ROLE_NAME_ALREADY_EXISTS.build("新增", role.getRoleName()));
		}
		SysRoleEntity covert = role.covert();
		covert.setCreateBy(SecurityUtils.getUsername());
		return ok(roleService.insertRole(covert));

	}

	/**
	 * 修改保存角色
	 */
	@PreAuthorize("@ss.hasPermi('system:role:edit')")
	@Log(title = "角色管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result edit(@Validated @RequestBody SysRoleUpdateParams role) {
		roleService.checkRoleAllowed(role);
		if (Constants.User.NOT_UNIQUE.equals(roleService.checkRoleNameUnique(role))) {
			throw new CustomException(Coder.SysUser.THE_ROLE_NAME_ALREADY_EXISTS.build("修改", role.getRoleName()));
		} else if (Constants.User.NOT_UNIQUE.equals(roleService.checkRoleKeyUnique(role))) {
			throw new CustomException(Coder.SysUser.THE_ROLE_NAME_ALREADY_EXISTS.build("修改", role.getRoleName()));
		}
		SysRoleEntity covert = role.covert();
		covert.setUpdateBy(SecurityUtils.getUsername());
		return ok(roleService.updateRole(covert));
	}

	/**
	 * 修改保存数据权限
	 */
	@PreAuthorize("@ss.hasPermi('system:role:edit')")
	@Log(title = "角色管理", businessType = BusinessType.UPDATE)
	@PutMapping("/dataScope")
	public Result dataScope(@RequestBody SysRoleUpdateParams role) {
		roleService.checkRoleAllowed(role);
		SysRoleEntity covert = role.covert();
		return ok(roleService.authDataScope(covert));
	}

	/**
	 * 状态修改
	 */
	@PreAuthorize("@ss.hasPermi('system:role:edit')")
	@Log(title = "角色管理", businessType = BusinessType.UPDATE)
	@PutMapping("/changeStatus")
	public Result changeStatus(@RequestBody SysRoleUpdateParams role) {
		roleService.checkRoleAllowed(role);
		SysRoleEntity covert = role.covert();
		covert.setUpdateBy(SecurityUtils.getUsername());
		return ok(roleService.updateRoleStatus(covert));
	}

	/**
	 * 删除角色
	 */
	@PreAuthorize("@ss.hasPermi('system:role:remove')")
	@Log(title = "角色管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{roleIds}")
	public Result remove(@PathVariable Long[] roleIds) {
		return ok(roleService.deleteRoleByIds(roleIds));
	}

	/**
	 * 获取角色选择框列表
	 */
	@PreAuthorize("@ss.hasPermi('system:role:query')")
	@GetMapping("/optionselect")
	public Result optionselect() {
		return ok(roleService.selectRoleAll());
	}
}