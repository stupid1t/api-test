package com.bird.web.controller.system;

import com.bird.common.annotation.Log;
import com.bird.common.constant.Constants;
import com.bird.common.enums.BusinessType;
import com.bird.common.exception.CustomException;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.SysMenuEntity;
import com.bird.system.domain.params.SysMenuQueryParams;
import com.bird.system.domain.params.SysMenuUpdateParams;
import com.bird.system.service.ISysMenuService;
import com.bird.web.controller.common.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 菜单信息
 *
 * @author bird
 */
@RestController
@RequestMapping("/system/menu")
public class SysMenuController extends BaseController {
	@Autowired
	private ISysMenuService menuService;

	/**
	 * 获取菜单列表
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:list')")
	@GetMapping("/list")
	public Result list(SysMenuQueryParams menu) {
		List<SysMenuEntity> menus = menuService.selectMenuList(menu);
		return ok(menuService.buildMenuTree(menus));
	}

	/**
	 * 根据菜单编号获取详细信息
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:query')")
	@GetMapping(value = "/{menuId}")
	public Result getInfo(@PathVariable Long menuId) {
		return ok(menuService.selectMenuById(menuId));
	}

	/**
	 * 获取菜单下拉树列表
	 */
	@GetMapping("/treeselect")
	public Result treeselect(SysMenuQueryParams dept) {
		List<SysMenuEntity> menus = menuService.selectMenuList(dept);
		return ok(menuService.buildMenuTreeSelect(menus));
	}

	/**
	 * 加载对应角色菜单列表树
	 */
	@GetMapping(value = "/roleMenuTreeselect/{roleId}")
	public Result roleMenuTreeselect(@PathVariable("roleId") Long roleId) {
		return ok(menuService.selectMenuListByRoleId(roleId));
	}

	/**
	 * 新增菜单
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:add')")
	@Log(title = "菜单管理", businessType = BusinessType.INSERT)
	@PostMapping
	public Result add(@Validated @RequestBody SysMenuUpdateParams menu) {
		if (Constants.User.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu))) {
			throw new CustomException("新增菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
		}
		SysMenuEntity covert = menu.covert();
		covert.setCreateBy(SecurityUtils.getUsername());
		return ok(menuService.insertMenu(covert));
	}

	/**
	 * 修改菜单
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:edit')")
	@Log(title = "菜单管理", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result edit(@Validated @RequestBody SysMenuUpdateParams menu) {
		if (Constants.User.NOT_UNIQUE.equals(menuService.checkMenuNameUnique(menu))) {
			throw new CustomException("修改菜单'" + menu.getMenuName() + "'失败，菜单名称已存在");
		}
		SysMenuEntity covert = menu.covert();
		covert.setUpdateBy(SecurityUtils.getUsername());
		return ok(menuService.updateMenu(covert));
	}

	/**
	 * 删除菜单
	 */
	@PreAuthorize("@ss.hasPermi('system:menu:remove')")
	@Log(title = "菜单管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{menuId}")
	public Result remove(@PathVariable("menuId") Long menuId) {
		if (menuService.hasChildByMenuId(menuId)) {
			throw new CustomException("存在子菜单,不允许删除");
		}
		if (menuService.checkMenuExistRole(menuId)) {
			throw new CustomException("菜单已分配,不允许删除");
		}
		return ok(menuService.deleteMenuById(menuId));
	}
}