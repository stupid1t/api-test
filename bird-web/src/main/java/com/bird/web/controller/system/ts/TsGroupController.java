package com.bird.web.controller.system.ts;

import com.bird.common.annotation.Log;
import com.bird.common.enums.BusinessType;
import com.bird.common.utils.covert.GroupIdCovert;
import com.bird.framework.domain.Result;
import com.bird.framework.utils.SecurityUtils;
import com.bird.system.domain.entity.TsGroupEntity;
import com.bird.system.domain.params.TsGroupQueryParam;
import com.bird.system.domain.params.TsGroupUpdateParam;
import com.bird.system.domain.vo.TsGroupListPcVO;
import com.bird.system.service.ITsGroupService;
import com.bird.web.controller.common.BaseController;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 组Controller
 *
 * @author bird
 * @date 2020-09-23
 */
@Api(tags = "组")
@RestController
@RequestMapping("/system/group")
public class TsGroupController extends BaseController {

	@Autowired
	private ITsGroupService tsGroupService;

	@ApiOperation("查询组列表")
	@PreAuthorize("@ss.hasPermi('system:group:list')")
	@GetMapping("/list")
	public Result<PageInfo<TsGroupListPcVO>> list(TsGroupQueryParam queryParams) {
		startPage();
		queryParams.setUserId(SecurityUtils.getLoginUser().getUserId());
		List<TsGroupListPcVO> list = tsGroupService.selectListPC(queryParams);
		return page(list);
	}

	@ApiOperation("获取组详细信息")
	@PreAuthorize("@ss.hasPermi('system:group:query')")
	@GetMapping(value = "/{id}")
	public Result<TsGroupEntity> info(@PathVariable("id") Long id) {
		return ok(tsGroupService.selectById(id));
	}

	@ApiOperation("新增组")
	@PreAuthorize("@ss.hasPermi('system:group:add')")
	@Log(title = "组", businessType = BusinessType.INSERT)
	@PostMapping
	public Result<Long> add(@RequestBody TsGroupUpdateParam updateParam) {
		return ok(tsGroupService.insertPC(updateParam, SecurityUtils.getLoginUser().getUserId()));
	}

	@ApiOperation("修改组")
	@PreAuthorize("@ss.hasPermi('system:group:edit')")
	@Log(title = "组", businessType = BusinessType.UPDATE)
	@PutMapping
	public Result<Integer> edit(@RequestBody TsGroupUpdateParam updateParam) {
		return ok(tsGroupService.updateByIdPC(updateParam));
	}

	@ApiOperation("删除组")
	@PreAuthorize("@ss.hasPermi('system:group:remove')")
	@Log(title = "组", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
	public Result<Integer> remove(@PathVariable Long[] ids) {
		// 删除本地缓存
		for (Long id : ids) {
			GroupIdCovert.existUser.remove(id);
		}
		return ok(tsGroupService.deleteByIds(ids));
	}
}
