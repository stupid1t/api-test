package com.bird.web.importRule;

import com.github.stupdit1t.excel.verify.AbstractVerifyBuidler;
import com.github.stupdit1t.excel.verify.CellVerifyEntity;
import com.github.stupdit1t.excel.verify.StringVerify;

public class PhoneDataVerify extends AbstractVerifyBuidler {

	private static PhoneDataVerify builder = new PhoneDataVerify();

	public static PhoneDataVerify getInstance() {
		return builder;
	}

	/**
	 * 定义列校验实体：提取的字段、提取列、校验规则
	 */
	private PhoneDataVerify() {
		cellEntitys.add(new CellVerifyEntity("theName", "A", new StringVerify("数据描述", true)));
		cellEntitys.add(new CellVerifyEntity("phone", "B", new StringVerify("手机号码", false)));
		// 必须调用
		super.init();
	}
}
