package base;

import com.bird.BirdApplication;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * 测试基类、测试继承此类
 *
 * @author: 625
 * @version: 2019年04月25日 10:00
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BirdApplication.class)
@WebAppConfiguration
public class BaseSpringBootTest {

	@Before
	public void init() {
		System.out.println("开始测试-----------------");
	}

	@After
	public void after() {
		System.out.println("测试结束-----------------");
	}
	
}
