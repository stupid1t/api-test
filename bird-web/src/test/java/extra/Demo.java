package extra;

import java.util.HashMap;
import java.util.Map;

/**
 * @author: 625
 * @version: 2019年12月28日 16:29
 */
public class Demo {

	private Long id;

	private Long actorId;


	private Long movieId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getActorId() {
		return actorId;
	}

	public void setActorId(Long actorId) {
		this.actorId = actorId;
	}

	public Long getMovieId() {
		return movieId;
	}

	public void setMovieId(Long movieId) {
		this.movieId = movieId;
	}


	public static void main(String[] args) {
		Map<String, byte[]> map = new HashMap<>();
		for (int i = 0; i < 10000000; i++) {
			byte[] bytes = new byte[800];
			map.put("1" + i, bytes);
		}
	}
}
