package extra;

import base.BaseSpringBootTest;
import com.bird.common.utils.tkmybatis.TK;
import com.bird.system.domain.entity.SysCodeRuleEntity;
import com.bird.system.service.ISysCodeRuleService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 示例测试类
 *
 * @author: 625
 * @version: 2019年11月20日 21:11
 */
public class DemoTest extends BaseSpringBootTest {

	@Autowired
	private ISysCodeRuleService sysCodeRuleService;


	/**
	 * commonService 测试
	 */
	@Test
	public void commonTest() {
		SysCodeRuleEntity sysCodeRuleEntity = sysCodeRuleService.selectOne(
				TK.select(SysCodeRuleEntity.class)
						.column("id")
						.where()
						.andIsNotNull(SysCodeRuleEntity::getId)
						.end()
						.orderBy(SysCodeRuleEntity::getRulePrefix)
						.desc()
						.limit(1)
		);
		System.out.println(sysCodeRuleEntity.getId());

		String ruleCode = sysCodeRuleService.selectField(SysCodeRuleEntity::getRuleCode,
				TK.select(SysCodeRuleEntity.class).where()
						.andEqualTo(SysCodeRuleEntity::getId, 1L).end()
		);
		System.out.println(ruleCode);
	}

}
