package com.bird.common.constant;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import tk.mybatis.mapper.annotation.KeySql;

import javax.persistence.Id;
import java.io.Serializable;

/**
 * Entity基类
 *
 * @author bird
 */
@Setter
@Getter
public abstract class BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 主键
	 */
	@Id
	@KeySql(useGeneratedKeys = true)
	@ApiModelProperty("主键")
	private Long id;
	
}
