package com.bird.common.constant;

import io.jsonwebtoken.Claims;

/**
 * 通用常量信息
 *
 * @author bird
 */
public interface Constants {

	/**
	 * 系统常量
	 */
	interface System {

		/**
		 * 下划线
		 */
		String UNDER_LINE = "_";

		/**
		 * 逗号
		 */
		String COMMA = ",";

		/**
		 * 空串
		 */
		String BLANK = " ";

		/**
		 * UTF-8 字符集
		 */
		String UTF8 = "UTF-8";

		/**
		 * 通用成功标识
		 */
		String SUCCESS = "0";

		/**
		 * 通用失败标识
		 */
		String FAIL = "1";

		/**
		 * 登录成功
		 */
		String LOGIN_SUCCESS = "Success";

		/**
		 * 注销
		 */
		String LOGOUT = "Logout";

		/**
		 * 登录失败
		 */
		String LOGIN_FAIL = "Error";

		/**
		 * 验证码 redis key
		 */
		String CACHE_CAPTCHA_CODE_KEY = "captcha_codes:";

		/**
		 * 登录用户 redis key
		 */
		String LOGIN_TOKEN_KEY = "login_tokens:";

		/**
		 * 验证码有效期（分钟）
		 */
		Integer CAPTCHA_EXPIRATION = 2;

		/**
		 * 令牌
		 */
		String TOKEN = "token";

		/**
		 * 令牌前缀
		 */
		String TOKEN_PREFIX = "Bearer ";


		/**
		 * 令牌前缀
		 */
		String LOGIN_USER_KEY = "login_user_key";

		/**
		 * 登录类型
		 */
		String LOGIN_TYPE_KEY = "login_type_key";

		/**
		 * 用户ID
		 */
		String JWT_USERID = "userid";

		/**
		 * 用户名称
		 */
		String JWT_USERNAME = Claims.SUBJECT;

		/**
		 * 用户头像
		 */
		String JWT_AVATAR = "avatar";

		/**
		 * 创建时间
		 */
		String JWT_CREATED = "created";

		/**
		 * 用户权限
		 */
		String JWT_AUTHORITIES = "authorities";

		/**
		 * 资源映射路径 前缀
		 */
		String RESOURCE_PREFIX = "/profile";

		/**
		 * 系统停止线程池停止超时时间
		 */
		long SYS_STOP_TIME_OUT = 120;

		/**
		 * 缓存系统标志
		 */
		public static final String CACHE_KEY_PREFIX = "BIRD:";

		/**
		 * 字典缓存管理器
		 */
		String CACHE_DICT = "DictCache";

	}

	/**
	 * 系统配置Key
	 */
	interface SysConfig {

		String EXPORT_PAGE_SIZE = "export_page_size";
	}

	/**
	 * 浏览器类型
	 */
	interface BrowserType {
		/**
		 * IE浏览器
		 */
		String MSIE = "MSIE";

		/**
		 * 火狐浏览器
		 */
		String FIREFOX = "Firefox";

		/**
		 * 谷歌浏览器
		 */
		String CHROME = "Chrome";
	}

	/**
	 * 日期格式
	 */
	class DatePattern extends cn.hutool.core.date.DatePattern {

		public static final String yyyyMM = "yyyy-MM";

		public static final String SLASH_DATE = "yyyy/MM/dd";
	}

	/**
	 * 用户相关常亮
	 */
	interface User {
		/**
		 * 平台内系统用户的唯一标志
		 */
		String SYS_USER = "SYS_USER";

		/**
		 * 正常状态
		 */
		String NORMAL = "0";

		/**
		 * 异常状态
		 */
		String EXCEPTION = "1";

		/**
		 * 用户封禁状态
		 */
		String USER_BLOCKED = "1";

		/**
		 * 角色封禁状态
		 */
		String ROLE_BLOCKED = "1";

		/**
		 * 部门正常状态
		 */
		String DEPT_NORMAL = "0";

		/**
		 * 字典正常状态
		 */
		String DICT_NORMAL = "0";

		/**
		 * 是否为系统默认（是）
		 */
		String YES = "Y";

		/**
		 * 校验返回结果码
		 */
		String UNIQUE = "0";

		/**
		 * 不唯一
		 */
		String NOT_UNIQUE = "1";
	}

	/**
	 * 用户相关常亮
	 */
	interface GenCode {
		/**
		 * 猫圈用户编码
		 */
		String CAT_USER = "CAT_USER";

		/**
		 * 猫咪编码
		 */
		String CAT_CAT = "CAT_CAT";

		/**
		 * 请求任务标志
		 */
		String REQ_MARK = "REQ_MARK";
	}

	/**
	 * api接口
	 */
	interface Api {

		/**
		 * 产品编码
		 */
		String API = "/api";

		/**
		 * 产品编码
		 */
		String PRODUCT = "/bird";

		/**
		 * 版本号
		 */
		String VERSION = "/0.0.1";

		/**
		 * mapping的base路径
		 */
		String MAP = API + VERSION + PRODUCT;
	}

	/**
	 * 浏览器类型
	 */
	interface TS {
		/**
		 * phone变量
		 */
		String phone = "${phone}";

		/**
		 * 超时标志
		 */
		String TIMEOUT = "TIMEOUT";

		/**
		 * 代理无
		 */
		String PROXY_NONE = "无";

		/**
		 * 自定义号码数据
		 */
		String custom = "custom:num:";
	}
}
