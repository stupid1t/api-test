package com.bird.common.constant;

/**
 * bean的验证规则
 *
 * @author: 625
 * @version: 2020年06月12日 13:59
 */
public interface ValidRule {

	/**
	 * 新增
	 */
	interface Add {
	}

	/**
	 * 修改
	 */
	interface Update {
	}

}
