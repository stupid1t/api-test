package com.bird.common.constant;

import com.bird.common.utils.StringUtil;

/**
 * 错误信息码定义
 *
 * @author: 625
 * @version: 2020年06月12日 13:59
 */
public interface Coder {

	/**
	 * 获取code
	 *
	 * @return
	 */
	int getCode();

	/**
	 * 获取错误描述信息
	 *
	 * @return
	 */
	String getMsg();


	/**
	 * 参数构建
	 *
	 * @param params
	 * @return
	 */
	default Coder build(String... params) {
		int code = getCode();
		String msg = StringUtil.format(getMsg(), params);
		Coder coder = new Coder() {
			@Override
			public int getCode() {
				return code;
			}

			@Override
			public String getMsg() {
				return msg;
			}
		};
		return coder;
	}

	/**
	 * 系统错误码
	 */
	enum System implements Coder {

		SUCCESS(200, "操作成功"),

		CREATED(201, "对象创建成功"),

		ACCEPTED(202, "请求已经被接受"),

		NO_CONTENT(204, "操作已经执行成功，但是没有返回数据"),

		MOVED_PERM(301, "资源已被移除"),

		SEE_OTHER(303, "重定向"),

		NOT_MODIFIED(304, "资源没有被修改"),

		BAD_REQUEST(400, "参数列表错误: {}"),

		UNAUTHORIZED(401, "请求访问：{}，认证失败，无法访问系统资源"),

		FORBIDDEN(403, "访问受限，请联系管理员"),

		NOT_FOUND(404, "资源，服务未找到"),

		BAD_METHOD(405, "不允许的http方法"),

		CONFLICT(409, "资源冲突，或者资源被锁"),

		UNSUPPORTED_TYPE(415, " 不支持的数据，媒体类型"),

		ERROR(500, "  操作失败"),

		NOT_IMPLEMENTED(501, "接口未实现"),

		BUSINESS_ERROR(502, "业务异常"),

		CODE_REPETITION(503, "编码重复：{}"),
		;

		/**
		 * 错误码
		 */
		private int code;

		/**
		 * 错误信息
		 */
		private String msg;

		System(int code, String msg) {
			this.code = code;
			this.msg = msg;
		}

		@Override
		public int getCode() {
			return code;
		}

		@Override
		public String getMsg() {
			return msg;
		}
	}


	/**
	 * 用户相关错误码
	 */
	enum SysUser implements Coder {

		ABNORMAL_ACCESS_TO_USER_ACCOUNT(4001, "获取用户账户异常"),

		USER_DOES_NOT_EXIST(4002, "登录用户：{} 不存在"),

		ACCOUNT_EXPIRED(4003, "账号过期：{}，请重新登录"),

		ACCOUNT_DELETION(4004, "对不起，您的账号：{} 已被删除"),

		ACCOUNT_DEACTIVATION(4005, "对不起，您的账号：{} 已停用"),

		THE_ROLE_NAME_ALREADY_EXISTS(4006, "{}角色'{}'失败，角色名称已存在"),
		;

		/**
		 * 错误码
		 */
		private int code;

		/**
		 * 错误信息
		 */
		private String msg;

		SysUser(int code, String msg) {
			this.code = code;
			this.msg = msg;
		}

		@Override
		public int getCode() {
			return code;
		}

		@Override
		public String getMsg() {
			return msg;
		}
	}
}
