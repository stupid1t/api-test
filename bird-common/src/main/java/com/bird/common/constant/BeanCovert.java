package com.bird.common.constant;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.lang.reflect.ParameterizedType;

/**
 * 类型转换
 *
 * @author: 625
 * @version: 2019年12月30日 20:09
 */
public abstract class BeanCovert<T> {

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseParams.class);

	/**
	 * 转换为数据库对象
	 *
	 * @param ignoreProperties 不转换的字段
	 * @return
	 */
	public T covert(String... ignoreProperties) {
		T entity = null;
		try {

			Class<T> entityClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
			entity = entityClass.newInstance();
			BeanUtils.copyProperties(this, entity, ignoreProperties);
		} catch (Exception e) {
			LOGGER.error(ExceptionUtils.getMessage(e));
		}
		return entity;
	}
}
