package com.bird.common.constant;

import com.bird.common.utils.SqlUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 参数基类，和entity交互使用，
 *
 * @author bird
 */
@ApiModel("参数基类")
public abstract class BaseParams {

	/**
	 * 要查询的列
	 */
	@ApiModelProperty(hidden = true)
	private String includeColumn;

	/**
	 * 搜索值
	 */
	@ApiModelProperty("关键词")
	private String searchValue;

	/**
	 * 数据权限
	 */
	@ApiModelProperty(hidden = true)
	private String dataScope;

	public String getSearchValue() {
		return searchValue;
	}

	public void setSearchValue(String searchValue) {
		this.searchValue = searchValue;
	}

	public String getDataScope() {
		return dataScope;
	}

	public void setDataScope(String dataScope) {
		this.dataScope = dataScope;
	}

	public String getIncludeColumn() {
		return includeColumn;
	}

	public void setIncludeColumn(String includeColumn) {
		boolean validOrderBySql = SqlUtil.isValidOrderBySql(includeColumn);
		if (validOrderBySql) {
			this.includeColumn = includeColumn;
		}
	}
}
