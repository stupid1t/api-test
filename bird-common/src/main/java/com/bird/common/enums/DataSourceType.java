package com.bird.common.enums;

/**
 * 数据源
 *
 * @author bird
 */
public enum DataSourceType {
	/**
	 * 主库
	 */
	MASTER,

	/**
	 * 从库
	 */
	SLAVE
}
