package com.bird.common.enums;

/**
 * 操作状态
 *
 * @author bird
 */
public enum BusinessStatus {
    /**
     * 成功
     */
    SUCCESS,

    /**
     * 失败
     */
    FAIL,
}
