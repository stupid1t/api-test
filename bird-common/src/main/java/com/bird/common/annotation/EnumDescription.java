package com.bird.common.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 枚举类的描述
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface EnumDescription {

    /**
     * 备注
     */
    String name();


    /**
     * 默认选中的KEY
     */
    String select() default "";


    /**
     * 0正常1停用
     */
    String status() default "0";

    /**
     * 表格回显样式
     */
    String listClass() default "";

    /**
     * 样式属性（其他样式扩展）
     */
    String cssClass() default "";


}
