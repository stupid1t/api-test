package com.bird.common.annotation;

import com.bird.common.utils.redis.RedisKeyEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 锁注解
 *
 * @author 625
 */
@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Locker {

	/**
	 * 要锁定的Key
	 *
	 * @return
	 */
	RedisKeyEnum key();

	/**
	 * 要锁定的参数
	 * 格式:Array对象,里边放jsonp表达式,
	 * 如['$.name','','$.num'] 则表示第一个字段根节点的name+第三个字段根节点的num
	 *
	 * @return
	 */
	String paramExp() default "[]";

	/**
	 * 业务超时自动释放锁的时间,应该大于正常业务执行时间
	 *
	 * @return
	 */
	long expireTime() default 10000;

	/**
	 * 最小持有锁的时间
	 *
	 * @return
	 */
	long limitTime() default 0;

	/**
	 * 是否持续竞争锁
	 *
	 * @return
	 */
	boolean continueGet() default false;

	/**
	 * 最大竞争次数。默认0不限次
	 *
	 * @return
	 */
	int maxGetNum() default 0;

	/**
	 * 拿不到锁，异常返回信息
	 *
	 * @return
	 */
	String noGetMsg() default "未获取锁";
}
