package com.bird.common.exception;

import com.bird.common.constant.Coder;

/**
 * 自定义异常
 *
 * @author bird
 */
public class CustomException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private Integer code;

	private String message;


	public CustomException(Coder coder) {
		this.code = coder.getCode();
		this.message = coder.getMsg();
	}


	public CustomException(String msg) {
		this.code = Coder.System.BUSINESS_ERROR.getCode();
		this.message = msg;
	}

	@Override
	public String getMessage() {
		return message;
	}

	public Integer getCode() {
		return code;
	}
}
