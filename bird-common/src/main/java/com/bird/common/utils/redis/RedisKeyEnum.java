package com.bird.common.utils.redis;

import com.bird.common.constant.Constants;

/**
 * Redis的Key
 *
 * @author: 625
 * @version: 2019年05月07日 15:21
 */
public enum RedisKeyEnum {

	//------------------------------锁开始----------------------------------

	/**
	 * 全局唯一锁KEY
	 */
	COMMON_LOCK(Constants.System.CACHE_KEY_PREFIX, "Global", "Global", "全局的一个锁"),

	/**
	 * 频繁操作Key
	 */
	FREQUENT_OPERATION_KEY(Constants.System.CACHE_KEY_PREFIX, "FREQUENT_OPERATION_KEY", "FREQUENT_OPERATION_KEY", "操作太频繁"),
	;


	/**
	 * 系统标识
	 */
	private String keyPrefix;
	/**
	 * 模块名称
	 */
	private String module;
	/**
	 * 方法名称
	 */
	private String func;

	/**
	 * 描述
	 */
	private String remark;

	RedisKeyEnum() {
	}

	RedisKeyEnum(String keyPrefix, String module, String func, String remark) {
		this.keyPrefix = keyPrefix;
		this.module = module;
		this.func = func;
		this.remark = remark;
	}

	public String getKeyPrefix() {
		return keyPrefix;
	}

	public String getModule() {
		return module;
	}

	public String getFunc() {
		return func;
	}

	public String getRemark() {
		return remark;
	}


}
