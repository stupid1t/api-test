package com.bird.common.utils;

public class FileTypeUtil extends cn.hutool.core.io.FileTypeUtil {

	private FileTypeUtil() {
	}

	static {
		//初始化文件类型信息
		//aac语音
		putFileType("FFF15C4013", "aac");
		//mp3
		putFileType("FFE368", "mp3");
		//webm
		putFileType("1A45DFA39F42868101", "webm");
		//webm
		putFileType("0000001C667479704D344120000000004D3441206D70", "m4a");
	}

}

