package com.bird.common.utils;

import com.alibaba.fastjson.JSON;

import java.io.InputStream;
import java.util.Map;

public interface SpiderResponse {

	/**
	 * 获取返回状态码
	 *
	 * @return
	 */
	int getStatusCode();

	/**
	 * 获取网页标题
	 *
	 * @return
	 */
	String getTitle();

	/**
	 * 获取网页html
	 *
	 * @return
	 */
	String getHtml();

	/**
	 * 获取json
	 *
	 * @return
	 */
	default Object getJson() {
		return JSON.parse(getHtml());
	}

	/**
	 * 获取cookies
	 *
	 * @return
	 */
	Map<String, String> getCookies();

	/**
	 * 获取headers
	 *
	 * @return
	 */
	Map<String, String> getHeaders();

	/**
	 * 获取byte[]
	 *
	 * @return
	 */
	byte[] getBytes();

	/**
	 * 获取ContentType
	 *
	 * @return
	 */
	String getContentType();

	/**
	 * 获取当前url
	 *
	 * @return
	 */
	String getUrl();


	default void setCharset(String charset) {

	}

	default InputStream getStream() {
		return null;
	}
}
