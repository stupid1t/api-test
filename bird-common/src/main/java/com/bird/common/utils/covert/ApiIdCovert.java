package com.bird.common.utils.covert;

import cn.hutool.cache.CacheUtil;
import cn.hutool.cache.impl.TimedCache;
import cn.hutool.core.util.ReflectUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.fasterxml.jackson.databind.util.StdConverter;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * 系统用户转换
 *
 * @author: 李涛
 * @version: 2020年02月14日 17:46
 */
@Slf4j
public class ApiIdCovert extends StdConverter<Long, ApiIdCovert.ApiBean> {

	/**
	 * 字典翻译的bean引用
	 */
	public static final Object SPRING_BEAN = SpringUtil.getBean("tsGroupApiServiceImpl");

	/**
	 * 已存在的用户
	 */
	public static Map<Long, ApiBean> existUser = new ConcurrentHashMap();

	/**
	 * 错误查找的用户，30分钟失效
	 */
	private static TimedCache<Long, String> errorUser = CacheUtil.newTimedCache(TimeUnit.MINUTES.toMillis(30));

	@Override
	public ApiBean convert(Long id) {
		if (id == null || id < 0) {
			return null;
		}
		if (errorUser.containsKey(id)) {
			log.error("Cache-Api查找失败:{}!", id);
			return null;
		}
		ApiBean theBean = existUser.get(id);
		if (theBean == null) {
			theBean = new ApiBean();
			theBean.setId(id);
			String name = ReflectUtil.invoke(SPRING_BEAN, "selectNameById", id);
			if (name != null) {
				theBean.setName(name);
				existUser.put(id, theBean);
			} else {
				errorUser.put(id, id.toString());
				log.error("Api查找失败:{}!", id);
			}

		}
		return theBean;
	}

	/**
	 * 系统用户
	 */
	@Data
	public static class ApiBean {

		private Long id;

		private String name;

	}
}
