package com.bird.common.utils;

/**
 * 处理并记录日志文件
 *
 * @author bird
 */
public class LogUtil {
	public static String getBlock(Object msg) {
		if (msg == null) {
			msg = "";
		}
		return "[" + msg.toString() + "]";
	}
}
