package com.bird.common.utils.function;

import cn.hutool.core.date.DateUtil;
import org.springframework.stereotype.Component;

/**
 * 时间表达式
 *
 * @author Administrator
 */
@Component
public class DateFunctionExecutor extends FunctionExecutor {

	@Override
	String[] getFunctionPattern() {
		return new String[]{"\\$\\{date\\.now\\(\\)}", "\\$\\{date\\.now\\(([^}]+)\\)}", "\\$\\{date\\.nowMillis\\(\\)}", "\\$\\{date\\.nowSeconds\\(\\)}"};
	}

	/**
	 * 当前时间，格式 yyyy-MM-dd HH:mm:ss
	 * <p>
	 * ${date.now()}
	 *
	 * @return
	 */
	public String now() {
		return DateUtil.now();
	}

	/**
	 * 格式化当前格式
	 * <p>
	 * ${date.now(format)}
	 *
	 * @return
	 */
	public String now(String format) {
		return DateUtil.date().toString(format);
	}

	/**
	 * 当前时间毫秒
	 * <p>
	 * ${date.nowMillis()}
	 *
	 * @return
	 */
	public long nowMillis() {
		return System.currentTimeMillis();
	}

	/**
	 * 当前时间秒
	 * <p>
	 * ${date.nowSeconds()}
	 *
	 * @return
	 */
	public long nowSeconds() {
		return System.currentTimeMillis() / 1000;
	}
}
