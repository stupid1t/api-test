package com.bird.common.utils.function;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import org.springframework.stereotype.Component;

/**
 * 随机数/字符串 生成方法
 *
 * @author Administrator
 */
@Component
public class RandomFunctionExecutor extends FunctionExecutor {

	@Override
	String[] getFunctionPattern() {
		return new String[]{"\\$\\{random\\.randomLong\\((\\d+),(\\d+)\\)}", "\\$\\{random\\.randomDouble\\((\\d+),(\\d+)\\)}", "\\$\\{random\\.string\\((\\d+)\\)}", "\\$\\{random\\.uuid\\(\\)}",};
	}

	/**
	 * 随机获取数字
	 * ${random.randomLong(1,10)}
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	public long randomLong(String min, String max) {
		return RandomUtil.randomLong(Long.valueOf(min), Long.valueOf(max));
	}

	/**
	 * 随机获取double
	 * ${random.randomDouble(1,10)}
	 *
	 * @param min
	 * @param max
	 * @return
	 */
	public double randomDouble(String min, String max) {
		return RandomUtil.randomDouble(Long.valueOf(min), Long.valueOf(max));
	}

	/**
	 * 随机获取字符串
	 * ${random.string(length)}
	 *
	 * @param length 字符范围
	 * @return String 随机字符串
	 */
	public String string(String length) {
		return RandomUtil.randomString(Integer.valueOf(length));
	}


	/**
	 * 获取UUID
	 * ${random.uuid()}
	 */
	public String uuid() {
		return IdUtil.fastSimpleUUID();
	}
}
