package com.bird.common.utils;

import org.springframework.stereotype.Component;

/**
 * spring工具类 方便在非spring管理环境中获取bean
 *
 * @author bird
 */
@Component
public final class SpringUtil extends cn.hutool.extra.spring.SpringUtil {

}
