package com.bird.common.utils;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.exception.CustomException;

/**
 * 业务异常断言
 */
public class Assert {

	/**
	 * 为空
	 *
	 * @param obj 对象
	 * @param msg 描述信息,变量用{}代替
	 */
	public static void isEmpty(Object obj, String msg, Object... params) {
		if (ObjectUtil.isEmpty(obj)) {
			throw new CustomException(StringUtil.format(msg, params));
		}
	}

	/**
	 * 不为空
	 *
	 * @param obj 对象
	 * @param msg 描述信息,变量用{}代替
	 */
	public static void isNotEmpty(Object obj, String msg, Object... params) {
		if (ObjectUtil.isNotEmpty(obj)) {
			throw new CustomException(StringUtil.format(msg, params));
		}
	}

	/**
	 * 如果为真
	 *
	 * @param flag 对象
	 * @param msg  描述信息,变量用{}代替
	 */
	public static void isTrue(boolean flag, String msg, Object... params) {
		if (flag) {
			throw new CustomException(StringUtil.format(msg, params));
		}
	}

	/**
	 * 如果不为真
	 *
	 * @param flag 对象
	 * @param msg  描述信息,变量用{}代替
	 */
	public static void isFalse(boolean flag, String msg, Object... params) {
		if (!flag) {
			throw new CustomException(StringUtil.format(msg, params));
		}
	}
}
