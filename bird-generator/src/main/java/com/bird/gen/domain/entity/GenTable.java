package com.bird.gen.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.common.constant.Constants;
import com.bird.common.utils.StringUtil;
import com.bird.gen.config.GenConstants;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * 业务表 gen_table
 *
 * @author ruoyi
 */
public class GenTable extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 编号
	 */
	private Long tableId;

	/**
	 * 表名称
	 */
	@NotBlank(message = "表名称不能为空")
	private String tableName;

	/**
	 * 表描述
	 */
	@NotBlank(message = "表描述不能为空")
	private String tableComment;

	/**
	 * 实体类名称(首字母大写)
	 */
	@NotBlank(message = "实体类名称不能为空")
	private String className;

	/**
	 * 使用的模板（crud单表操作 tree树表操作）
	 */
	private String tplCategory;

	/**
	 * 生成包路径
	 */
	@NotBlank(message = "生成包路径不能为空")
	private String packageName;

	/**
	 * 生成模块名
	 */
	@NotBlank(message = "生成模块名不能为空")
	private String moduleName;

	/**
	 * 生成业务名
	 */
	@NotBlank(message = "生成业务名不能为空")
	private String businessName;

	/**
	 * 生成功能名
	 */
	@NotBlank(message = "生成功能名不能为空")
	private String functionName;

	/**
	 * 生成作者
	 */
	@NotBlank(message = "作者不能为空")
	private String functionAuthor;

	/**
	 * 主键信息
	 */
	private GenTableColumn pkColumn;

	/**
	 * 表列信息
	 */
	@Valid
	private List<GenTableColumn> columns;

	/**
	 * 其它生成选项
	 */
	private String options;

	/**
	 * 树编码字段
	 */
	private String treeCode;

	/**
	 * 树父编码字段
	 */
	private String treeParentCode;

	/**
	 * 树名称字段
	 */
	private String treeName;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 额外参数
	 */
	private Map<String, Object> params;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间 推广
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date updateTime;

	public Long getTableId() {
		return tableId;
	}

	public void setTableId(Long tableId) {
		this.tableId = tableId;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getTableComment() {
		return tableComment;
	}

	public void setTableComment(String tableComment) {
		this.tableComment = tableComment;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getTplCategory() {
		return tplCategory;
	}

	public void setTplCategory(String tplCategory) {
		this.tplCategory = tplCategory;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}

	public String getBusinessName() {
		return businessName;
	}

	public void setBusinessName(String businessName) {
		this.businessName = businessName;
	}

	public String getFunctionName() {
		return functionName;
	}

	public void setFunctionName(String functionName) {
		this.functionName = functionName;
	}

	public String getFunctionAuthor() {
		return functionAuthor;
	}

	public void setFunctionAuthor(String functionAuthor) {
		this.functionAuthor = functionAuthor;
	}

	public GenTableColumn getPkColumn() {
		return pkColumn;
	}

	public void setPkColumn(GenTableColumn pkColumn) {
		this.pkColumn = pkColumn;
	}

	public List<GenTableColumn> getColumns() {
		return columns;
	}

	public void setColumns(List<GenTableColumn> columns) {
		this.columns = columns;
	}

	public String getOptions() {
		return options;
	}

	public void setOptions(String options) {
		this.options = options;
	}

	public String getTreeCode() {
		return treeCode;
	}

	public void setTreeCode(String treeCode) {
		this.treeCode = treeCode;
	}

	public String getTreeParentCode() {
		return treeParentCode;
	}

	public void setTreeParentCode(String treeParentCode) {
		this.treeParentCode = treeParentCode;
	}

	public String getTreeName() {
		return treeName;
	}

	public void setTreeName(String treeName) {
		this.treeName = treeName;
	}

	public boolean isTree() {
		return isTree(this.tplCategory);
	}

	public static boolean isTree(String tplCategory) {
		return tplCategory != null && StringUtil.equals(GenConstants.TPL_TREE, tplCategory);
	}

	public boolean isCrud() {
		return isCrud(this.tplCategory);
	}

	public static boolean isCrud(String tplCategory) {
		return tplCategory != null && StringUtil.equals(GenConstants.TPL_CRUD, tplCategory);
	}

	public boolean isSuperColumn(String javaField) {
		return isSuperColumn(this.tplCategory, javaField);
	}

	public static boolean isSuperColumn(String tplCategory, String javaField) {
		if (isTree(tplCategory)) {
			StringUtil.equalsAnyIgnoreCase(javaField, GenConstants.TREE_ENTITY);
		}
		return StringUtil.equalsAnyIgnoreCase(javaField, GenConstants.BASE_ENTITY);
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getCreateBy() {
		return createBy;
	}

	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateBy() {
		return updateBy;
	}

	public void setUpdateBy(String updateBy) {
		this.updateBy = updateBy;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}
}