<template>
    <div class="app-container">
        <el-collapse-transition>
            <el-form v-if="openCondition" :model="queryParams" ref="queryForm" :inline="true" label-width="68px">
                #foreach($column in $columns)
                    #if($column.query)
                        #set($dictType=$column.dictType)
                        #set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
                        #set($parentheseIndex=$column.columnComment.indexOf("（"))
                        #if($parentheseIndex == -1)
                            #set($parentheseIndex=$column.columnComment.indexOf("("))
                        #end
                        #if($parentheseIndex != -1)
                            #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                        #else
                            #set($comment=$column.columnComment)
                        #end
                        #if($column.htmlType == "input")
                            <el-form-item label="${comment}" prop="${column.javaField}">
                                <el-input
                                        v-model="queryParams.${column.javaField}"
                                        placeholder="请输入${comment}"
                                        clearable
                                    #if($column.javaType=="Long"||$column.javaType=="Integer"||$column.javaType==
                                        "Double"||$column.javaType=="BigDeciaml")
                                        type="number"
                                    #end
                                        size="small"
                                        @keyup.enter.native="handleQuery"
                                />
                            </el-form-item>
                        #elseif(($column.htmlType == "select" || $column.htmlType == "radio") && "" != $dictType)
                            <el-form-item label="${comment}" prop="${column.javaField}">
                                <el-select v-model="queryParams.${column.javaField}" placeholder="请选择${comment}"
                                           clearable size="small">
                                    <el-option
                                            v-for="dict in ${column.javaField}Options"
                                            :key="dict.dictValue"
                                            :label="dict.dictLabel"
                                            :value="dict.dictValue"
                                    />
                                </el-select>
                            </el-form-item>
                        #elseif(($column.htmlType == "select" || $column.htmlType == "radio") && $dictType)
                            <el-form-item label="${comment}" prop="${column.javaField}">
                                <el-select v-model="queryParams.${column.javaField}" placeholder="请选择${comment}"
                                           clearable size="small">
                                    <el-option label="请选择字典生成" value=""/>
                                </el-select>
                            </el-form-item>
                        #elseif($column.htmlType == "datetime")
                            #if($column.htmlType == "datetime" && $column.queryType=="BETWEEN")
                                #set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
                                <el-form-item label="${comment}" prop="${column.javaField}">
                                    <el-date-picker
                                            v-model="dateRange$AttrName"
                                            size="small"
                                            style="width:472px"
                                            value-format="yyyy-MM-dd"
                                            type="daterange"
                                            range-separator="-"
                                            start-placeholder="开始日期"
                                            end-placeholder="结束日期"
                                    ></el-date-picker>
                                </el-form-item>
                            #else
                                <el-form-item label="${comment}" prop="${column.javaField}">
                                    <el-date-picker clearable size="small"
                                                    style="width: 195px"
                                                    v-model="queryParams.${column.javaField}"
                                                    type="date"
                                                    value-format="yyyy-MM-dd"
                                                    placeholder="选择${comment}">
                                    </el-date-picker>
                                </el-form-item>
                            #end
                        #end
                    #end
                #end
            </el-form>
        </el-collapse-transition>
        <el-row :gutter="10" class="mb8">
            <el-col :span="1.5">
                <el-button
                        type="primary"
                        icon="el-icon-plus"
                        size="mini"
                        @click="handleAdd"
                        v-hasPermi="['${moduleName}:${businessName}:add']"
                >新增
                </el-button>
            </el-col>
            <el-col :span="1.5">
                <el-button
                        type="warning"
                        icon="el-icon-edit"
                        size="mini"
                        :disabled="single"
                        @click="handleUpdate"
                        v-hasPermi="['${moduleName}:${businessName}:edit']"
                >修改
                </el-button>
            </el-col>
            <el-col :span="1.5">
                <el-button
                        type="danger"
                        icon="el-icon-delete"
                        size="mini"
                        :disabled="multiple"
                        @click="handleDelete"
                        v-hasPermi="['${moduleName}:${businessName}:remove']"
                >删除
                </el-button>
            </el-col>
            <el-col :span="1.5" style="float: right">
                <el-button type="primary" icon="el-icon-search" size="mini" @click="handleQuery">搜索</el-button>
                <el-button icon="el-icon-refresh" size="mini" @click="resetQuery">重置</el-button>
                <el-button :type="!openCondition?'success':'info'"
                           :icon="!openCondition?'el-icon-caret-bottom':'el-icon-caret-top'" size="mini"
                           @click="openCondition=!openCondition">{{!openCondition?'展开条件':'收起条件'}}
                </el-button>
            </el-col>
        </el-row>

        <el-table :data="${businessName}List" height="580" @sort-change="handleSortChange"
                  @selection-change="handleSelectionChange">
            <el-table-column type="selection" width="55" align="center"/>
            <el-table-column label="序号" width="50">
                <template scope="scope">
                    <span>{{(queryParams.pageNum - 1) * queryParams.pageSize + scope.$index + 1}}</span>
                </template>
            </el-table-column>
            #foreach($column in $columns)
                #set($javaType=$column.javaType)
                #set($columnType=$column.columnType)
                #if($column.javaType == 'String' && ($column.columnType == 'char(2)'|| $column.columnType ==
                    'varchar(2)' )  && ($column.htmlType == 'select'||$column.htmlType == 'radio') )##是否显示字典
                    #set($dictName=".value")
                #else
                    #set($dictName="")
                #end
                #set($javaField=$column.javaField)
                #set($parentheseIndex=$column.columnComment.indexOf("（"))
                #if($parentheseIndex == -1)
                    #set($parentheseIndex=$column.columnComment.indexOf("("))
                #end
                #if($parentheseIndex != -1)
                    #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                #else
                    #set($comment=$column.columnComment)
                #end
                #if($column.list && $column.htmlType == "datetime")
                    <el-table-column label="${comment}" align="center" sortable="custom" prop="${javaField}"
                                     width="180">
                        <template slot-scope="scope">
                            <span>{{ parseTime(scope.row.${javaField}) }}</span>
                        </template>
                    </el-table-column>
                #elseif($column.list && "" == $column.dictType && $dictName =="" )
                    <el-table-column label="${comment}" align="center" sortable="custom" prop="${javaField}"/>
                #elseif($column.list && "" != $column.dictType && $dictName !="" )
                    <el-table-column label="${comment}" align="center" sortable="custom" prop="${javaField}">
                        <template slot-scope="scope">
                            <el-tag size="medium">{{ scope.row.${javaField}${dictName} }}</el-tag>
                        </template>
                    </el-table-column>
                #end
            #end
            <el-table-column label="操作" align="center" fixed="right" width="180" class-name="small-padding fixed-width">
                <template slot-scope="scope">
                    <el-button
                            size="mini"
                            type="text"
                            icon="el-icon-info"
                            @click="handleView(scope.row)"
                    >详情
                    </el-button>
                    <el-button
                            size="mini"
                            type="text"
                            icon="el-icon-edit"
                            @click="handleUpdate(scope.row)"
                            v-hasPermi="['${moduleName}:${businessName}:edit']"
                    >修改
                    </el-button>
                    <el-button
                            size="mini"
                            type="text"
                            icon="el-icon-delete"
                            @click="handleDelete(scope.row)"
                            v-hasPermi="['${moduleName}:${businessName}:remove']"
                    >删除
                    </el-button>
                </template>
            </el-table-column>
        </el-table>

        <pagination
                v-show="total>0"
                :total="total"
                :page.sync="queryParams.pageNum"
                :limit.sync="queryParams.pageSize"
                @pagination="getList"
        />

        <!-- 添加或修改${functionName}对话框 -->
        <el-dialog :title="title" :visible.sync="open" width="750px">
            <!-- 修改 -->
            <el-form v-if="this.edit" ref="form" :model="form" :rules="rules" label-width="140px">
                #foreach($column in $columns)
                    #set($field=$column.javaField)
                    #if($column.edit && !$column.pk)
                        #if(($column.usableColumn) || (!$column.superColumn))
                            #set($parentheseIndex=$column.columnComment.indexOf("（"))
                            #if($parentheseIndex == -1)
                                #set($parentheseIndex=$column.columnComment.indexOf("("))
                            #end
                            #if($parentheseIndex != -1)
                                #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                            #else
                                #set($comment=$column.columnComment)
                            #end
                            #set($dictType=$column.dictType)
                            #if($column.htmlType == "input")
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-input v-model="form.${field}" placeholder="请输入${comment}"/>
                                </el-form-item>
                            #elseif($column.htmlType == "select" && "" != $dictType)
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-select v-model="form.${field}" placeholder="请选择${comment}">
                                        <el-option
                                                v-for="dict in ${column.javaField}Options"
                                                :key="dict.dictValue"
                                                :label="dict.dictLabel"
                                                :value="dict.dictValue"
                                        />
                                    </el-select>
                                </el-form-item>
                            #elseif($column.htmlType == "select" && $dictType)
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-select v-model="form.${field}" placeholder="请选择${comment}">
                                        <el-option label="请选择字典生成" value=""/>
                                    </el-select>
                                </el-form-item>
                            #elseif($column.htmlType == "radio" && "" != $dictType)
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-radio-group v-model="form.${field}">
                                        <el-radio
                                                v-for="dict in ${field}Options"
                                                :key="dict.dictValue"
                                                :label="dict.dictValue"
                                        >{{dict.dictLabel}}
                                        </el-radio>
                                    </el-radio-group>
                                </el-form-item>
                            #elseif($column.htmlType == "radio" && $dictType)
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-radio-group v-model="form.${field}">
                                        <el-radio label="1">请选择字典生成</el-radio>
                                    </el-radio-group>
                                </el-form-item>
                            #elseif($column.htmlType == "datetime")
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-date-picker clearable size="small"
                                                    v-model="form.${field}"
                                                    type="date"
                                                    format="yyyy-MM-dd HH:mm:ss"
                                                    value-format="yyyy-MM-dd HH:mm:ss"
                                                    placeholder="选择${comment}">
                                    </el-date-picker>
                                </el-form-item>
                            #elseif($column.htmlType == "textarea")
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-input v-model="form.${field}" type="textarea" placeholder="请输入内容"/>
                                </el-form-item>
                            #end
                        #end
                    #end
                #end
            </el-form>
            <!-- 添加 -->
            <el-form v-if="!this.edit" ref="form" :model="form" :rules="rules" label-width="140px">
                #foreach($column in $columns)
                    #set($field=$column.javaField)
                    #if($column.insert && !$column.pk)
                        #if(($column.usableColumn) || (!$column.superColumn))
                            #set($parentheseIndex=$column.columnComment.indexOf("（"))
                            #if($parentheseIndex == -1)
                                #set($parentheseIndex=$column.columnComment.indexOf("("))
                            #end
                            #if($parentheseIndex != -1)
                                #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                            #else
                                #set($comment=$column.columnComment)
                            #end
                            #set($dictType=$column.dictType)
                            #if($column.htmlType == "input")
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-input v-model="form.${field}" placeholder="请输入${comment}"/>
                                </el-form-item>
                            #elseif($column.htmlType == "select" && "" != $dictType)
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-select v-model="form.${field}" placeholder="请选择${comment}">
                                        <el-option
                                                v-for="dict in ${column.javaField}Options"
                                                :key="dict.dictValue"
                                                :label="dict.dictLabel"
                                                :value="dict.dictValue"
                                        />
                                    </el-select>
                                </el-form-item>
                            #elseif($column.htmlType == "select" && $dictType)
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-select v-model="form.${field}" placeholder="请选择${comment}">
                                        <el-option label="请选择字典生成" value=""/>
                                    </el-select>
                                </el-form-item>
                            #elseif($column.htmlType == "radio" && "" != $dictType)
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-radio-group v-model="form.${field}">
                                        <el-radio
                                                v-for="dict in ${field}Options"
                                                :key="dict.dictValue"
                                                :label="dict.dictValue"
                                        >{{dict.dictLabel}}
                                        </el-radio>
                                    </el-radio-group>
                                </el-form-item>
                            #elseif($column.htmlType == "radio" && $dictType)
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-radio-group v-model="form.${field}">
                                        <el-radio label="1">请选择字典生成</el-radio>
                                    </el-radio-group>
                                </el-form-item>
                            #elseif($column.htmlType == "datetime")
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-date-picker clearable size="small"
                                                    v-model="form.${field}"
                                                    type="date"
                                                    format="yyyy-MM-dd HH:mm:ss"
                                                    value-format="yyyy-MM-dd HH:mm:ss"
                                                    placeholder="选择${comment}">
                                    </el-date-picker>
                                </el-form-item>
                            #elseif($column.htmlType == "textarea")
                                <el-form-item label="${comment}" prop="${field}">
                                    <el-input v-model="form.${field}" type="textarea" placeholder="请输入内容"/>
                                </el-form-item>
                            #end
                        #end
                    #end
                #end
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button type="primary" @click="submitForm" size="mini">确 定</el-button>
                <el-button @click="cancel" size="mini">取 消</el-button>
            </div>
        </el-dialog>

        <!-- ${functionName}详情 -->
        <el-dialog title="${functionName}详情" :visible.sync="viewOpen" width="750px">
            <el-form :inline="true" class="detail-inline" label-width="140px">
                #foreach($column in $columns)
                    #set($field=$column.javaField)
                    #if(!$column.pk)
                        #if(($column.usableColumn) || (!$column.superColumn))
                            #set($parentheseIndex=$column.columnComment.indexOf("（"))
                            #if($parentheseIndex == -1)
                                #set($parentheseIndex=$column.columnComment.indexOf("("))
                            #end
                            #if($parentheseIndex != -1)
                                #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                            #else
                                #set($comment=$column.columnComment)
                            #end
                            #set($dictType=$column.dictType)
                            #if($column.htmlType == "input")
                                <el-form-item label="${comment}：">
                                    <input class="bird-view-info-input" disabled :title="viewData.${field}"
                                           :value="viewData.${field}"/>
                                </el-form-item>
                            #elseif($column.htmlType == "select")
                                <el-form-item label="${comment}：">
                                    <input class="bird-view-info-input" disabled :title="viewData.${field}"
                                           :value="viewData.${field}"/>
                                </el-form-item>
                            #elseif($column.htmlType == "radio")
                                <el-form-item label="${comment}：">
                                    <input class="bird-view-info-input" disabled :title="viewData.${field}"
                                           :value="viewData.${field}"/>
                                </el-form-item>
                            #elseif($column.htmlType == "datetime")
                                <el-form-item label="${comment}：">
                                    <input class="bird-view-info-input" disabled :title="viewData.${field}"
                                           :value="viewData.${field}"/>
                                </el-form-item>
                            #elseif($column.htmlType == "textarea")
                                <el-form-item style="width: 100%;" label="${comment}：">
                                    <div style="color: #606266;width:700px;" v-html="viewData.${field}"></div>
                                </el-form-item>
                            #end
                        #end
                    #end
                #end
            </el-form>
            <div slot="footer" class="dialog-footer">
                <el-button @click="viewOpen = false" size="mini">关 闭</el-button>
            </div>
        </el-dialog>
    </div>
</template>

<script>
    import {add, del, get, list, update} from "@/api/";

    export default {
        data() {
            return {
                // 条件展开
                openCondition: true,
                // 是否修改
                edit: false,
                // 选中数组
                ids: [],
                // 非单个禁用
                single: true,
                // 非多个禁用
                multiple: true,
                // 总条数
                total: 0,
                // ${functionName}表格数据
                    ${businessName}List: [],
                // 弹出层标题
                title: "",
                // 是否显示详情弹出层
                viewOpen: false,
                // 是否显示弹出层
                open: false,
                #foreach ($column in $columns)
                    #set($parentheseIndex=$column.columnComment.indexOf("（"))
                    #if($parentheseIndex == -1)
                        #set($parentheseIndex=$column.columnComment.indexOf("("))
                    #end
                    #if($parentheseIndex != -1)
                        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                    #else
                        #set($comment=$column.columnComment)
                    #end
                    #if(${column.dictType} != '')
                        // $comment字典
                            ${column.javaField}Options: [],

                    #end
                #end
                #foreach ($column in $columns)
                    #set($parentheseIndex=$column.columnComment.indexOf("（"))
                    #if($parentheseIndex == -1)
                        #set($parentheseIndex=$column.columnComment.indexOf("("))
                    #end
                    #if($parentheseIndex != -1)
                        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                    #else
                        #set($comment=$column.columnComment)
                    #end
                    #if($column.htmlType == "datetime" && $column.queryType=="BETWEEN")
                        #set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
                        // $comment范围查询
                        dateRange$AttrName: [],
                    #end
                #end
                // 查询参数
                queryParams: {
                    pageNum: 1,
                    pageSize: 10,
                    orderByColumn: 'id',
                    isAsc: 'desc'
                },
                // 详情参数
                viewData: {},
                // 表单参数
                form: {},
                // 表单校验
                rules: {
            #foreach ($column in $columns)
                #if($column.required && ($column.edit || $column.insert))
                    #set($parentheseIndex=$column.columnComment.indexOf("（"))
                    #if($parentheseIndex == -1)
                        #set($parentheseIndex=$column.columnComment.indexOf("("))
                    #end
                    #if($parentheseIndex != -1)
                        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                    #else
                        #set($comment=$column.columnComment)
                    #end
                    $column.javaField: [
                    {required: true, message: "$comment不能为空", trigger: "blur"}
                ]#if($velocityCount != $columns.size()),
                #end
                #end
            #end
        }
        }
            ;
        },
        created() {
            this.getList();
            #foreach ($column in $columns)
                #if(${column.dictType} != '')
                    this.getDicts("${column.dictType}").then(response => {
                        this.${column.javaField}Options = response.data;
                    });
                #end
            #end
        },
        methods: {
            /** 查询${functionName}列表 */
            getList() {
                #foreach ($column in $columns)
                    #set($parentheseIndex=$column.columnComment.indexOf("（"))
                    #if($parentheseIndex == -1)
                        #set($parentheseIndex=$column.columnComment.indexOf("("))
                    #end
                    #if($parentheseIndex != -1)
                        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                    #else
                        #set($comment=$column.columnComment)
                    #end
                    #if($column.htmlType == "datetime" && $column.queryType=="BETWEEN")
                        #set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
                        // $comment范围设置
                        this.addDateRangeField(this.queryParams, this.dateRange$AttrName, '$AttrName')
                    #end
                #end
                list${BusinessName}(this.queryParams).then(response => {
                    this.${businessName}List = response.data.list;
                    this.total = response.data.total;
                });
            },
            #foreach ($column in $columns)
                #if(${column.dictType} != '' && $column.columnType != 'char(2)'&& $column.columnType != 'varchar(2)' )
                    #set($parentheseIndex=$column.columnComment.indexOf("（"))
                    #if($parentheseIndex == -1)
                        #set($parentheseIndex=$column.columnComment.indexOf("("))
                    #end
                    #if($parentheseIndex != -1)
                        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                    #else
                        #set($comment=$column.columnComment)
                    #end
                    // $comment字典翻译
                        ${column.javaField}Format(row, column) {
                        return this.selectDictLabel(this.${column.javaField}Options, row.${column.javaField});
                    },
                #end
            #end
            // 取消按钮
            cancel() {
                this.open = false;
                this.reset();
            },
            // 表单重置
            reset() {
                this.resetPropField(this.form);
                this.clearForm('form');
            },
            /** 搜索按钮操作 */
            handleQuery() {
                this.queryParams.pageNum = 1;
                this.getList();
            },
            /** 重置按钮操作 */
            resetQuery() {
                #foreach ($column in $columns)
                    #set($parentheseIndex=$column.columnComment.indexOf("（"))
                    #if($parentheseIndex == -1)
                        #set($parentheseIndex=$column.columnComment.indexOf("("))
                    #end
                    #if($parentheseIndex != -1)
                        #set($comment=$column.columnComment.substring(0, $parentheseIndex))
                    #else
                        #set($comment=$column.columnComment)
                    #end
                    #if($column.htmlType == "datetime" && $column.queryType=="BETWEEN")
                        #set($AttrName=$column.javaField.substring(0,1).toUpperCase() + ${column.javaField.substring(1)})
                        this.dateRange$AttrName = [];
                    #end
                #end
                this.resetPropField(this.queryParams, {pageNum: 1, pageSize: 10, orderByColumn: 'id', isAsc: 'desc'});
                this.handleQuery();
            },
            // 排序改变事件
            handleSortChange(sorter) {
                this.handleSort(this.queryParams, sorter);
                this.getList();
            },
            // 多选框选中数据
            handleSelectionChange(selection) {
                this.ids = selection.map(item => item.${pkColumn.javaField})
                this.single = selection.length != 1
                this.multiple = !selection.length
            },
            /** 详细按钮操作 */
            handleView(row) {
                const ${pkColumn.javaField} =
                row.${pkColumn.javaField}
                get${BusinessName}(${pkColumn.javaField}).then(response => {
                    const {data} = response;
                    this.viewData = {
                        ...data,
                    #foreach ($column in $columns)
                        #set($javaType=$column.javaType)
                        #set($columnType=$column.columnType)
                        #if($column.javaType == 'String' && ($column.htmlType == 'select'||$column.htmlType ==
                            'radio') )##是否显示字典
                            ${column.javaField}:
                            data.${column.javaField} && data.${column.javaField}.value,
                        #else
                            #set($dictName="")
                        #end
                    #end
                }
                    this.viewOpen = true;
                });
            },
            /** 新增按钮操作 */
            handleAdd() {
                this.reset();
                this.edit = false;
                this.open = true;
                this.title = "添加${functionName}";
            },
            /** 修改按钮操作 */
            handleUpdate(row) {
                this.edit = true;
                this.reset();
                const ${pkColumn.javaField} =
                row.${pkColumn.javaField} || this.ids
                get${BusinessName}(${pkColumn.javaField}).then(response => {
                    const {data} = response;
                    this.form = {
                        ...data,
                    #foreach ($column in $columns)
                        #set($javaType=$column.javaType)
                        #set($columnType=$column.columnType)
                        #if($column.javaType == 'String' && ($column.htmlType == 'select'||$column.htmlType ==
                            'radio') )##是否显示字典
                            ${column.javaField}:
                            data.${column.javaField} && data.${column.javaField}.key,
                        #else
                            #set($dictName="")
                        #end
                    #end
                }
                    this.open = true;
                    this.title = "修改${functionName}";
                });
            },
            /** 提交按钮 */
            submitForm: function () {
                this.#[[$]]
                #refs["form"].validate(valid => {
                    if (valid) {
                        if (this.form.${pkColumn.javaField} != undefined) {
                            update${BusinessName}(this.form).then(response => {
                                this.msgSuccess("修改成功");
                                this.open = false;
                                this.getList();
                            });
                        } else {
                            add${BusinessName}(this.form).then(response => {
                                this.msgSuccess("新增成功");
                                this.open = false;
                                this.getList();
                            });
                        }
                    }
                });
            },
            /** 删除按钮操作 */
            handleDelete(row) {
                const ${pkColumn.javaField}s = row.${pkColumn.javaField} || this.ids;
                this.$confirm('是否确认删除${functionName}数据项?', "警告", {
                    confirmButtonText: "确定",
                    cancelButtonText: "取消",
                    type: "warning"
                }).then(function () {
                    return del${BusinessName}(${pkColumn.javaField}s);
                }).then(() => {
                    this.getList();
                    this.msgSuccess("删除成功");
                });
            }
        }
    };
</script>
