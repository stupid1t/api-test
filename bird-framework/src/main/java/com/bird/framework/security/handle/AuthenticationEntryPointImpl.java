package com.bird.framework.security.handle;

import com.alibaba.fastjson.JSON;
import com.bird.common.constant.Coder;
import com.bird.common.utils.ServletUtil;
import com.bird.framework.domain.Result;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 认证失败处理类 返回未授权
 *
 * @author bird
 */
@Component
public class AuthenticationEntryPointImpl implements AuthenticationEntryPoint, Serializable {
	private static final long serialVersionUID = -8970718410437077606L;

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException e)
			throws IOException {
		Coder coder = Coder.System.UNAUTHORIZED.build(request.getRequestURI());
		ServletUtil.write(response, JSON.toJSONString(Result.no(coder)), MediaType.APPLICATION_JSON_UTF8_VALUE);
	}
}
