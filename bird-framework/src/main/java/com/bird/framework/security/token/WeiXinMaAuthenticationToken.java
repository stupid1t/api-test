package com.bird.framework.security.token;

import org.springframework.security.authentication.AbstractAuthenticationToken;

/**
 * 小程序认证toekn
 *
 * @author: 625
 * @version: 2020年07月23日 15:35
 */
public class WeiXinMaAuthenticationToken extends AbstractAuthenticationToken {

	/**
	 * 微信code
	 */
	private String code;


	public WeiXinMaAuthenticationToken(String code) {
		super(null);
		this.code = code;
		setAuthenticated(false);
	}

	@Override
	public Object getCredentials() {
		return code;
	}

	@Override
	public Object getPrincipal() {
		return code;
	}
}
