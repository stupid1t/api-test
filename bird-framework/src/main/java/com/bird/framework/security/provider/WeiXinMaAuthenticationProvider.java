package com.bird.framework.security.provider;

import com.bird.framework.security.token.WeiXinMaAuthenticationToken;
import com.bird.system.domain.bo.LoginUser;
import com.bird.system.domain.bo.Subject;
import com.bird.system.enums.LoginTypeEnum;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

/**
 * 微信登录认证处理
 *
 * @author: 625
 * @version: 2020年07月23日 15:40
 */
@Component
public class WeiXinMaAuthenticationProvider implements AuthenticationProvider {

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {
		WeiXinMaAuthenticationToken token = (WeiXinMaAuthenticationToken) authentication;
		// 执行微信登录,模拟登录成功
		LoginUser loginUser = new LoginUser();
		loginUser.setUser(new Subject() {
			@Override
			public Long getId() {
				return 1L;
			}
		});
		loginUser.setLoginType(LoginTypeEnum.CAT_MEMBER);
		token.setDetails(loginUser);
		return token;
	}

	@Override
	public boolean supports(Class<?> support) {
		return (WeiXinMaAuthenticationToken.class.isAssignableFrom(support));
	}
}
