package com.bird.framework.security.service;

import cn.hutool.core.exceptions.ExceptionUtil;
import com.bird.common.constant.Constants;
import com.bird.common.exception.CustomException;
import com.bird.common.exception.user.CaptchaException;
import com.bird.common.exception.user.CaptchaExpireException;
import com.bird.common.exception.user.UserPasswordNotMatchException;
import com.bird.common.utils.MessageUtil;
import com.bird.framework.thread.AsyncManager;
import com.bird.framework.thread.factory.AsyncFactory;
import com.bird.framework.utils.redis.RedisCache;
import com.bird.system.domain.bo.LoginUser;
import com.bird.system.enums.LoginTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 登录校验方法
 *
 * @author bird
 */
@Component
public class SysLoginService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SysLoginService.class);

	@Autowired
	private TokenService tokenService;

	@Resource
	private AuthenticationManager authenticationManager;

	@Autowired
	private RedisCache redisCache;

	/**
	 * 登录验证
	 *
	 * @param username 用户名
	 * @param password 密码
	 * @param captcha  验证码
	 * @param uuid     唯一标识
	 * @return 结果
	 */
	public String login(String username, String password, String code, String uuid) {
		String verifyKey = Constants.System.CACHE_KEY_PREFIX + Constants.System.CACHE_CAPTCHA_CODE_KEY + uuid;
		String captcha = redisCache.getCacheObject(verifyKey);
		redisCache.deleteObject(verifyKey);
		if (captcha == null) {
			AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, LoginTypeEnum.SYSTEM, Constants.System.LOGIN_FAIL, MessageUtil.message("user.jcaptcha.error")));
			throw new CaptchaExpireException();
		}
		if (!code.equalsIgnoreCase(captcha)) {
			AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, LoginTypeEnum.SYSTEM, Constants.System.LOGIN_FAIL, MessageUtil.message("user.jcaptcha.expire")));
			throw new CaptchaException();
		}
		// 用户验证
		Authentication authentication = null;
		try {
			// 该方法会去调用UserDetailsServiceImpl.loadUserByUsername
			authentication = authenticationManager
					.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (Exception e) {
			LOGGER.error(ExceptionUtil.stacktraceToString(e));
			if (e instanceof BadCredentialsException) {
				AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, LoginTypeEnum.SYSTEM, Constants.System.LOGIN_FAIL, MessageUtil.message("user.password.not.match")));
				throw new UserPasswordNotMatchException();
			} else {
				AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, LoginTypeEnum.SYSTEM, Constants.System.LOGIN_FAIL, e.getMessage()));
				throw new CustomException(e.getMessage());
			}
		}
		AsyncManager.me().execute(AsyncFactory.recordLogininfor(username, LoginTypeEnum.SYSTEM, Constants.System.LOGIN_SUCCESS, MessageUtil.message("user.login.success")));
		LoginUser loginUser = (LoginUser) authentication.getPrincipal();
		// 生成token
		return tokenService.createToken(loginUser);
	}
}
