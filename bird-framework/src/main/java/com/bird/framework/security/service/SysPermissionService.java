package com.bird.framework.security.service;

import com.bird.system.domain.entity.SysUserEntity;
import com.bird.system.service.ISysMenuService;
import com.bird.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

/**
 * 用户权限处理
 *
 * @author bird
 */
@Component
public class SysPermissionService {
	@Autowired
	private ISysRoleService roleService;

	@Autowired
	private ISysMenuService menuService;

	/**
	 * 获取角色数据权限
	 *
	 * @param user 用户信息
	 * @return 角色权限信息
	 */
	public Set<String> getRolePermission(SysUserEntity user) {
		Set<String> roles = new HashSet<String>();
		// 管理员拥有所有权限
		if (user.isweb()) {
			roles.add("web");
		} else {
			roles.addAll(roleService.selectRolePermissionByUserId(user.getId()));
		}
		return roles;
	}

	/**
	 * 获取菜单数据权限
	 *
	 * @param user 用户信息
	 * @return 菜单权限信息
	 */
	public Set<String> getMenuPermission(SysUserEntity user) {
		Set<String> roles = new HashSet<String>();
		// 管理员拥有所有权限
		if (user.isweb()) {
			roles.add("*:*:*");
		} else {
			roles.addAll(menuService.selectMenuPermsByUserId(user.getId()));
		}
		return roles;
	}
}
