package com.bird.framework.domain.server;

import cn.hutool.core.date.DateUtil;
import com.bird.common.utils.CalcUtil;

import java.lang.management.ManagementFactory;
import java.util.Date;

/**
 * JVM相关信息
 *
 * @author bird
 */
public class Jvm {
	/**
	 * 当前JVM占用的内存总数(M)
	 */
	private double total;

	/**
	 * JVM最大可用内存总数(M)
	 */
	private double max;

	/**
	 * JVM空闲内存(M)
	 */
	private double free;

	/**
	 * JDK版本
	 */
	private String version;

	/**
	 * JDK路径
	 */
	private String home;

	public double getTotal() {
		return CalcUtil.div(total, (1024 * 1024)).doubleValue(2);
	}

	public void setTotal(double total) {
		this.total = total;
	}

	public double getMax() {
		return CalcUtil.div(max, (1024 * 1024)).doubleValue(2);
	}

	public void setMax(double max) {
		this.max = max;
	}

	public double getFree() {
		return CalcUtil.div(free, (1024 * 1024)).doubleValue(2);
	}

	public void setFree(double free) {
		this.free = free;
	}

	public double getUsed() {
		return CalcUtil.div(total - free, (1024 * 1024)).doubleValue(2);
	}

	public double getUsage() {
		return CalcUtil.mul(CalcUtil.div(total - free, total).doubleValue(4), 100).doubleValue();
	}

	/**
	 * 获取JDK名称
	 */
	public String getName() {
		return ManagementFactory.getRuntimeMXBean().getVmName();
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getHome() {
		return home;
	}

	public void setHome(String home) {
		this.home = home;
	}

	/**
	 * JDK启动时间
	 */
	public String getStartTime() {
		return DateUtil.date(new Date(ManagementFactory.getRuntimeMXBean().getStartTime())).toString();
	}

	/**
	 * JDK运行时间
	 */
	public String getRunTime() {
		return DateUtil.formatBetween(System.currentTimeMillis() - ManagementFactory.getRuntimeMXBean().getStartTime());
	}
}
