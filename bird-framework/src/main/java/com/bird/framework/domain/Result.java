package com.bird.framework.domain;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.constant.Coder;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 操作消息提醒
 *
 * @author ruoyi
 */
public class Result<T> implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final Coder.System SUCCESS = Coder.System.SUCCESS;

	private static final Coder.System ERROR = Coder.System.ERROR;

	/**
	 * 状态码
	 */
	private int code;

	/**
	 * 返回内容
	 */
	private String msg;

	/**
	 * 数据对象
	 */
	private Object data;

	/**
	 * 初始化一个新创建的 AjaxResult 对象
	 *
	 * @param msg  消息
	 * @param data 数据对象
	 */
	public <T> Result(int code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		if (ObjectUtil.isNotNull(data)) {
			this.data = data;
		}
	}

	/**
	 * 自定义put数据
	 *
	 * @param key
	 * @param obj
	 * @return
	 */
	public <T> Result<T> put(String key, Object obj) {
		if (this.data == null) {
			this.data = new HashMap<String, Object>();
		} else if (!(this.data instanceof Map)) {
			throw new UnsupportedOperationException("不支持设置data后put数据");
		}
		((Map<String, Object>) data).put(key, obj);
		return (Result<T>) this;
	}

	/**
	 * 返回成功消息
	 *
	 * @return 成功消息
	 */
	public static <T> Result<T> ok() {
		return new Result<T>(SUCCESS.getCode(), SUCCESS.getMsg(), null);
	}

	/**
	 * 返回成功数据
	 *
	 * @param data
	 * @return 成功消息
	 */
	public static <T> Result<T> ok(Object data) {
		return new Result<T>(SUCCESS.getCode(), SUCCESS.getMsg(), data);
	}

	/**
	 * 返回成功数据
	 *
	 * @return 成功消息
	 */
	public static <T> Result<T> ok(String msg, T data) {
		return new Result<T>(SUCCESS.getCode(), msg, data);
	}

	/**
	 * 返回错误消息
	 *
	 * @return
	 */
	public static <T> Result<T> no() {
		return new Result<T>(ERROR.getCode(), ERROR.getMsg(), null);
	}

	/**
	 * 返回错误消息
	 *
	 * @return
	 */
	public static <T> Result<T> no(String msg) {
		return new Result<T>(ERROR.getCode(), msg, null);
	}

	/**
	 * 返回错误消息
	 *
	 * @param data 返回内容
	 * @return 警告消息
	 */
	public static <T> Result<T> no(Object data) {
		return new Result<T>(ERROR.getCode(), ERROR.getMsg(), data);
	}

	/**
	 * 返回错误消息
	 *
	 * @param coder 状态码
	 * @return 警告消息
	 */
	public static <T> Result<T> no(Coder coder) {
		return new Result<T>(coder.getCode(), coder.getMsg(), null);
	}


	/**
	 * 返回错误消息
	 *
	 * @param coder code
	 * @param data  数据对象
	 * @return 警告消息
	 */
	public static <T> Result<T> no(Coder coder, Object data) {
		return new Result<T>(coder.getCode(), coder.getMsg(), data);
	}

	/**
	 * 返回错误消息
	 *
	 * @param msg  消息
	 * @param data 数据对象
	 * @return 警告消息
	 */
	public static <T> Result<T> no(String msg, Object data) {
		return new Result<T>(ERROR.getCode(), msg, data);
	}

	public int getCode() {
		return code;
	}

	public String getMsg() {
		return msg;
	}

	public Object getData() {
		return data;
	}

}
