package com.bird.framework.domain.server;

import com.bird.common.utils.CalcUtil;

/**
 * 內存相关信息
 *
 * @author bird
 */
public class Mem {
	/**
	 * 内存总量
	 */
	private double total;

	/**
	 * 已用内存
	 */
	private double used;

	/**
	 * 剩余内存
	 */
	private double free;

	public double getTotal() {
		return CalcUtil.div(total, (1024 * 1024 * 1024)).doubleValue(2);
	}

	public void setTotal(long total) {
		this.total = total;
	}

	public double getUsed() {
		return CalcUtil.div(used, (1024 * 1024 * 1024)).doubleValue(2);
	}

	public void setUsed(long used) {
		this.used = used;
	}

	public double getFree() {
		return CalcUtil.div(free, (1024 * 1024 * 1024)).doubleValue(2);
	}

	public void setFree(long free) {
		this.free = free;
	}

	public double getUsage() {
		return CalcUtil.mul(CalcUtil.div(used, total).doubleValue(4), 100).doubleValue();
	}
}
