package com.bird.framework.domain.page;


import com.bird.common.utils.SqlUtil;
import com.bird.common.utils.StringUtil;
import com.github.pagehelper.PageHelper;

/**
 * 分页数据
 *
 * @author ruoyi
 */
public class PageDomain {
	/**
	 * 当前记录起始索引
	 */
	private int pageNum = 1;
	/**
	 * 每页显示记录数
	 */
	private int pageSize = 10;
	/**
	 * 排序列
	 */
	private String orderByColumn;
	/**
	 * 排序的方向 "desc" 或者 "asc".
	 */
	private String isAsc;

	/**
	 * 清理分页信息
	 */
	public static void clearPage() {
		PageHelper.clearPage();
	}

	public String getOrderBy() {
		StringBuffer orderByStr = new StringBuffer();
		if (StringUtil.isEmpty(orderByColumn)) {
			// 如果不传递排序列，默认按id倒叙
			return "";
		}
		if (isAsc == null) {
			isAsc = "";
		}
		// 检测是否多字段
		if (orderByColumn.contains(",")) {
			String[] orderByArray = orderByColumn.split(",");
			String[] isAscArray = isAsc.split(",");
			for (int i = 0; i < orderByArray.length; i++) {
				String sortField = orderByArray[i];
				if (StringUtil.isNotEmpty(sortField)) {
					String asc = "";
					if (i < isAscArray.length && isAscArray[i].matches("desc|asc")) {
						asc = isAscArray[i];
					}
					orderByStr.append(StringUtil.toUnderScoreCase(sortField.trim())).append(" ").append(asc.trim()).append(",");
				}
			}
			orderByStr.deleteCharAt(orderByStr.length() - 1);

		} else {
			orderByStr.append(StringUtil.toUnderScoreCase(orderByColumn.trim()) + " " + isAsc.trim());
		}
		return orderByStr.toString();
	}

	/**
	 * 开启分页.默认拉取request请求中的分页数据
	 */
	public static void startPage() {
		PageHelper.clearPage();
		PageDomain pageDomain = PageSupport.buildPageRequest();
		int pageNum = pageDomain.getPageNum();
		int pageSize = pageDomain.getPageSize();
		String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
		PageHelper.startPage(pageNum, pageSize, orderBy);
	}

	/**
	 * 设置排序
	 */
	public static void orderBy(String orderBy) {
		PageHelper.orderBy(orderBy);
	}

	/**
	 * 开启分页，不排序
	 *
	 * @param pageNum  页码
	 * @param pageSize 数量
	 */
	public static void startPage(int pageNum, int pageSize) {
		PageHelper.clearPage();
		PageHelper.startPage(pageNum, pageSize);
	}

	/**
	 * 开启分页，全部自定义
	 *
	 * @param pageNum       页码
	 * @param pageSize      数量
	 * @param orderByColumn 排序的列,非下划线字段
	 * @param asc           升降序
	 */
	public static void startPage(int pageNum, int pageSize, String orderByColumn, String asc) {
		PageHelper.clearPage();
		PageDomain pageDomain = PageSupport.buildPageRequest();
		pageDomain.setOrderByColumn(orderByColumn);
		pageDomain.setIsAsc(asc);
		String orderBy = SqlUtil.escapeOrderBySql(pageDomain.getOrderBy());
		PageHelper.startPage(pageNum, pageSize, orderBy);
	}

	public int getPageNum() {
		return pageNum;
	}

	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public String getOrderByColumn() {
		return orderByColumn;
	}

	public void setOrderByColumn(String orderByColumn) {
		this.orderByColumn = orderByColumn;
	}

	public String getIsAsc() {
		return isAsc;
	}

	public void setIsAsc(String isAsc) {
		this.isAsc = isAsc;
	}
}
