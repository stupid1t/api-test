package com.bird.framework.interceptor;

import com.bird.common.exception.DemoModeException;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 演示环境拦截器，拦截删除、修改操作
 *
 * @author bird
 */
@Component
@ConditionalOnProperty(prefix = "bird", name = "demoEnabled", havingValue = "true")
public class DemoShowInterceptor extends HandlerInterceptorAdapter {

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		if (handler instanceof HandlerMethod) {
			String method = request.getMethod();
			if (HttpMethod.PUT.name().equals(method) || HttpMethod.DELETE.name().equals(method)) {
				throw new DemoModeException();
			}
			return true;
		} else {
			return super.preHandle(request, response, handler);
		}
	}
}
