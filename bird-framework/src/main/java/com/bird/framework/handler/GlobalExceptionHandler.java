package com.bird.framework.handler;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.constant.Coder;
import com.bird.common.exception.BaseException;
import com.bird.common.exception.CustomException;
import com.bird.common.exception.DemoModeException;
import com.bird.framework.domain.Result;
import com.bird.framework.thread.AsyncManager;
import com.bird.framework.thread.factory.AsyncFactory;
import com.bird.system.service.ISysErrorLogService;
import com.bird.system.service.TaskStatusManageService;
import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AccountExpiredException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.Set;

/**
 * 全局异常处理器
 *
 * @author bird
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

	private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

	@Autowired
	private ISysErrorLogService sysErrorLogService;

	@Autowired
	private TaskStatusManageService taskStatusManageService;

	/**
	 * 基础异常
	 */
	@ExceptionHandler(BaseException.class)
	public Result baseException(BaseException e) {
		return Result.no(e.getMessage());
	}

	/**
	 * 业务异常
	 */
	@ExceptionHandler(CustomException.class)
	public Result businessException(CustomException e) {
		if (ObjectUtil.isNull(e.getCode())) {
			return Result.no(e.getMessage());
		}
		return new Result(e.getCode(), e.getMessage(), null);
	}

	@ExceptionHandler(NoHandlerFoundException.class)
	public Result handlerNoFoundException(Exception e) {
		log.error(e.getMessage(), e);
		return Result.no(Coder.System.NOT_FOUND);
	}

	@ExceptionHandler(AccessDeniedException.class)
	public Result handleAuthorizationException(AccessDeniedException e) {
		log.error(e.getMessage());
		return Result.no(Coder.System.FORBIDDEN);
	}

	@ExceptionHandler(AccountExpiredException.class)
	public Result handleAccountExpiredException(AccountExpiredException e) {
		log.error(e.getMessage(), e);
		return Result.no(Coder.SysUser.ACCOUNT_EXPIRED.build(e.getMessage()));
	}

	@ExceptionHandler(UsernameNotFoundException.class)
	public Result handleUsernameNotFoundException(UsernameNotFoundException e) {
		log.error(e.getMessage(), e);
		return Result.no(Coder.SysUser.USER_DOES_NOT_EXIST.build(e.getMessage()));
	}

	/**
	 * 自定义验证异常
	 */
	@ExceptionHandler(BindException.class)
	public Result validatedBindException(BindException e) {
		log.error(e.getMessage(), e);
		String message = e.getAllErrors().get(0).getDefaultMessage();
		return Result.no(Coder.System.BAD_REQUEST.build(message));
	}

	@ExceptionHandler(value = ConstraintViolationException.class)
	public Result handleException(ConstraintViolationException e) {
		String message = e.getMessage();
		Set<ConstraintViolation<?>> constraintViolations = e.getConstraintViolations();
		StringBuilder builder = new StringBuilder();
		constraintViolations.forEach(n -> {
			ConstraintViolationImpl info = (ConstraintViolationImpl) n;
			String propertyPath = info.getPropertyPath().toString();
			builder.append(info.getMessage());
			// builder.append(propertyPath.substring(propertyPath.lastIndexOf(".") + 1, propertyPath.length()) + info.getMessage() + ",");
		});
		String msg = builder.toString().substring(0, builder.length() - 1);
		log.error("参数校验异常 Error:e={}", msg);
		return Result.no(msg);
	}

	/**
	 * 自定义验证异常
	 */
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Object validExceptionHandler(MethodArgumentNotValidException e) {
		log.error(e.getMessage(), e);
		String message = e.getBindingResult().getFieldError().getDefaultMessage();
		return Result.no(Coder.System.BAD_REQUEST.build(message));
	}

	/**
	 * 演示模式异常
	 */
	@ExceptionHandler(DemoModeException.class)
	public Result demoModeException(DemoModeException e) {
		return Result.no("演示模式，不允许该类型操作");
	}

	@ExceptionHandler(Exception.class)
	public Result handleException(Exception e) {
		log.error(e.getMessage(), e);
		AsyncManager.me().execute(AsyncFactory.recordError(e));
		return Result.no(e.getMessage());
	}
}
