package com.bird.framework.utils;

import com.bird.common.constant.Coder;
import com.bird.common.exception.CustomException;
import com.bird.system.domain.bo.LoginUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * 安全服务工具类
 *
 * @author bird
 */
public class SecurityUtils {
	/**
	 * 获取用户账户
	 **/
	public static String getUsername() {
		try {
			return getLoginUser().getUsername();
		} catch (Exception e) {
			throw new CustomException(Coder.SysUser.ABNORMAL_ACCESS_TO_USER_ACCOUNT);
		}
	}

	/**
	 * 获取用户
	 **/
	public static LoginUser getLoginUser(boolean... catchException) {
		try {
			return (LoginUser) getAuthentication().getPrincipal();
		} catch (Exception e) {
			if (catchException != null && catchException.length > 0 && catchException[0]) {
				return null;
			}
			throw new CustomException(Coder.SysUser.ABNORMAL_ACCESS_TO_USER_ACCOUNT);
		}
	}

	/**
	 * 获取Authentication
	 */
	public static Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	/**
	 * 生成BCryptPasswordEncoder密码
	 *
	 * @param password 密码
	 * @return 加密字符串
	 */
	public static String encryptPassword(String password) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.encode(password);
	}

	/**
	 * 判断密码是否相同
	 *
	 * @param rawPassword     真实密码
	 * @param encodedPassword 加密后字符
	 * @return 结果
	 */
	public static boolean matchesPassword(String rawPassword, String encodedPassword) {
		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
		return passwordEncoder.matches(rawPassword, encodedPassword);
	}

}
