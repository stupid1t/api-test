package com.bird.framework.config;

import com.bird.common.utils.StringUtil;
import com.bird.framework.filter.ChannelFilter;
import com.bird.framework.filter.XssFilter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.DispatcherType;
import java.util.HashMap;
import java.util.Map;

/**
 * Filter配置
 *
 * @author bird
 */
@Configuration
public class FilterConfig {

	@Value("${xss.enabled}")
	private String enabled;

	@Value("${xss.excludes}")
	private String excludes;

	@Value("${xss.urlPatterns}")
	private String urlPatterns;


	@Bean
	public FilterRegistrationBean channelFilter() {
		String includeUri = "/*";
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setDispatcherTypes(DispatcherType.REQUEST);
		registration.setFilter(new ChannelFilter());
		registration.addUrlPatterns(StringUtils.split(includeUri, ","));
		registration.setName("channelFilter");
		registration.setOrder(1);
		return registration;
	}

	@Bean
	public FilterRegistrationBean xssFilterRegistration() {
		FilterRegistrationBean registration = new FilterRegistrationBean();
		registration.setDispatcherTypes(DispatcherType.REQUEST);
		registration.setFilter(new XssFilter());
		registration.addUrlPatterns(StringUtil.split(urlPatterns, ","));
		registration.setName("xssFilter");
		registration.setOrder(2);
		Map<String, String> initParameters = new HashMap<String, String>();
		initParameters.put("excludes", excludes);
		initParameters.put("enabled", enabled);
		registration.setInitParameters(initParameters);
		return registration;
	}
}
