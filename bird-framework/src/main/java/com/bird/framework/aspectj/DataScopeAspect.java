package com.bird.framework.aspectj;

import com.bird.common.annotation.DataScope;
import com.bird.common.constant.BaseParams;
import com.bird.common.utils.ServletUtil;
import com.bird.common.utils.SpringUtil;
import com.bird.common.utils.StringUtil;
import com.bird.framework.security.service.TokenService;
import com.bird.system.domain.bo.LoginUser;
import com.bird.system.domain.entity.SysRoleEntity;
import com.bird.system.domain.entity.SysUserEntity;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * 数据过滤处理
 *
 * @author bird
 */
@Aspect
@Component
public class DataScopeAspect {
	/**
	 * 全部数据权限
	 */
	public static final String DATA_SCOPE_ALL = "1";

	/**
	 * 自定数据权限
	 */
	public static final String DATA_SCOPE_CUSTOM = "2";

	/**
	 * 部门数据权限
	 */
	public static final String DATA_SCOPE_DEPT = "3";

	/**
	 * 部门及以下数据权限
	 */
	public static final String DATA_SCOPE_DEPT_AND_CHILD = "4";

	/**
	 * 仅本人数据权限
	 */
	public static final String DATA_SCOPE_SELF = "5";

	// 配置织入点
	@Pointcut("@annotation(com.bird.common.annotation.DataScope)")
	public void dataScopePointCut() {
	}

	@Before("dataScopePointCut()")
	public void doBefore(JoinPoint point) throws Throwable {
		handleDataScope(point);
	}

	protected void handleDataScope(final JoinPoint joinPoint) {
		// 获得注解
		DataScope controllerDataScope = getAnnotationLog(joinPoint);
		if (controllerDataScope == null) {
			return;
		}
		// 获取当前的用户
		LoginUser loginUser = SpringUtil.getBean(TokenService.class).getLoginUser(ServletUtil.getRequest());
		SysUserEntity currentUser = loginUser.getUser(SysUserEntity.class);
		if (currentUser != null) {
			// 如果是超级管理员，则不过滤数据
			if (!currentUser.isweb()) {
				dataScopeFilter(joinPoint, currentUser, controllerDataScope.deptAlias(),
						controllerDataScope.userAlias());
			}
		}
	}

	/**
	 * 数据范围过滤
	 *
	 * @param joinPoint 切点
	 * @param user      用户
	 * @param userAlias 别名
	 */
	public static void dataScopeFilter(JoinPoint joinPoint, SysUserEntity user, String deptAlias, String userAlias) {
		StringBuilder sqlString = new StringBuilder();

		for (SysRoleEntity role : user.getRoles()) {
			String dataScope = role.getDataScope();
			if (DATA_SCOPE_ALL.equals(dataScope)) {
				sqlString = new StringBuilder();
				break;
			} else if (DATA_SCOPE_CUSTOM.equals(dataScope)) {
				sqlString.append(StringUtil.format(
						" OR {}.dept_id IN ( SELECT dept_id FROM sys_role_dept WHERE role_id = {} ) ", deptAlias,
						role.getId()));
			} else if (DATA_SCOPE_DEPT.equals(dataScope)) {
				sqlString.append(StringUtil.format(" OR {}.dept_id = {} ", deptAlias, user.getDeptId()));
			} else if (DATA_SCOPE_DEPT_AND_CHILD.equals(dataScope)) {
				sqlString.append(StringUtil.format(
						" OR {}.dept_id IN ( SELECT dept_id FROM sys_dept WHERE dept_id = {} or find_in_set( {} , ancestors ) )",
						deptAlias, user.getDeptId(), user.getDeptId()));
			} else if (DATA_SCOPE_SELF.equals(dataScope)) {
				if (StringUtil.isNotBlank(userAlias)) {
					sqlString.append(StringUtil.format(" OR {}.user_id = {} ", userAlias, user.getId()));
				} else {
					// 数据权限为仅本人且没有userAlias别名不查询任何数据
					sqlString.append(" OR 1=0 ");
				}
			}
		}

		if (StringUtil.isNotBlank(sqlString.toString())) {
			BaseParams baseEntity = (BaseParams) joinPoint.getArgs()[0];
			baseEntity.setDataScope(" AND (" + sqlString.substring(4) + ")");
		}
	}

	/**
	 * 是否存在注解，如果存在就获取
	 */
	private DataScope getAnnotationLog(JoinPoint joinPoint) {
		Signature signature = joinPoint.getSignature();
		MethodSignature methodSignature = (MethodSignature) signature;
		Method method = methodSignature.getMethod();

		if (method != null) {
			return method.getAnnotation(DataScope.class);
		}
		return null;
	}
}
