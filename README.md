## 开发
1. 将bird-framework中的pom如下注释掉tomcat依赖
    ```xml
    <!-- SpringBoot Web容器 -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <!--<exclusions>
            <exclusion>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-tomcat</artifactId>
            </exclusion>
        </exclusions>-->
    </dependency>
    ```
2. 将bird-web的pom的打包方式由war改为jar，以便于springboot启动
```
<packaging>jar</packaging>
```
3. 刷新maven依赖，编译代码开发

## 发布

1. 将bird-framework中的pom的tomcat依赖取消掉注释
    ```xml
    <!-- SpringBoot Web容器 -->
    <dependency>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-web</artifactId>
        <exclusions>
            <exclusion>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-starter-tomcat</artifactId>
            </exclusion>
        </exclusions>
    </dependency>
    ```
2. 将bird-web的pom的打包方式由jar改为war，以便于maven打包为war包，放入tomcat容器启动
    ```
    <packaging>jar</packaging>
    ```
3. 在api-test目录下，打开cmd，输入 mvn clean install -Dmaven.test.skip=true -Pprod
4. 打包完成后，将src\main\docker\api-test-server.war 文件丢入tomcat的webapps目录下
5. 运行tomcat的bin目录下的startup，服务发布成功，接口请求的路径就是war的名字