package com.bird.quartz.domain.entity;

import com.bird.common.constant.Constants;
import com.bird.quartz.common.constant.ScheduleConstants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 定时任务调度表 sys_job
 *
 * @author ruoyi
 */
@Data
public class SysJobEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long jobId;

	/**
	 * 任务名称
	 */
	private String jobName;

	/**
	 * 任务组名
	 */
	private String jobGroup;

	/**
	 * 调用目标字符串
	 */
	private String invokeTarget;

	/**
	 * cron执行表达式
	 */
	private String cronExpression;

	/**
	 * cron计划策略
	 */
	private String misfirePolicy = ScheduleConstants.MISFIRE_DEFAULT;

	/**
	 * 是否并发执行（0允许 1禁止）
	 */
	private String concurrent;

	/**
	 * 任务状态（0正常 1暂停）
	 */
	private String status;

	private String remark;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间 推广
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date updateTime;
}
