package com.bird.quartz.common.utils;

import com.bird.quartz.domain.entity.SysJobEntity;
import org.quartz.JobExecutionContext;

/**
 * 定时任务处理（允许并发执行）
 *
 * @author ruoyi
 */
public class QuartzJobExecution extends AbstractQuartzJob {
	@Override
	protected void doExecute(JobExecutionContext context, SysJobEntity sysJob) throws Exception {
		JobInvokeUtil.invokeMethod(sysJob);
	}
}
