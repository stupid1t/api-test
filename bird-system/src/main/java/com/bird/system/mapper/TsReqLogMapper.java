package com.bird.system.mapper;

import com.bird.system.domain.entity.TsReqLogEntity;
import com.bird.system.domain.vo.TsReqLogListPcVO;
import com.bird.system.domain.params.TsReqLogQueryParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import java.util.List;

/**
 * 请求日志Dao接口
 *
 * @author bird
 * @date 2020-09-23
 */
@Repository
public interface TsReqLogMapper extends Mapper<TsReqLogEntity>{

    /**
     * 查询请求日志列表,PC专用不要轻易混用更改
     *
     * @param queryParam  请求日志查询参数
     * @return 请求日志集合
     */
    List<TsReqLogListPcVO> selectListPC(TsReqLogQueryParam queryParam);

}
