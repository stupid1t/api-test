package com.bird.system.mapper;

import com.bird.system.domain.entity.SysCodeRuleEntity;
import com.bird.system.domain.params.SysCodeRuleQueryParam;
import com.bird.system.domain.vo.SysCodeRuleListPcVO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 系统编码规则配置Dao接口
 *
 * @author bird
 * @date 2020-07-22
 */
@Repository
public interface SysCodeRuleMapper extends Mapper<SysCodeRuleEntity> {

	/**
	 * 查询系统编码规则配置列表,PC专用不要轻易混用更改
	 *
	 * @param queryParam 系统编码规则配置查询参数
	 * @return 系统编码规则配置集合
	 */
	List<SysCodeRuleListPcVO> selectListPC(SysCodeRuleQueryParam queryParam);

}
