package com.bird.system.mapper;

import com.bird.system.domain.entity.TsGroupApiConfigEntity;
import com.bird.system.domain.entity.TsReqLogEntity;
import com.bird.system.domain.params.TsGroupApiConfigQueryParam;
import com.bird.system.domain.vo.TsGroupApiConfigInfoPcVo;
import com.bird.system.domain.vo.TsGroupApiConfigListPcVO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 请求任务配置Dao接口
 *
 * @author bird
 * @date 2020-09-23
 */
@Repository
public interface TsGroupApiConfigMapper extends Mapper<TsGroupApiConfigEntity> {

	/**
	 * 查询请求任务配置列表,PC专用不要轻易混用更改
	 *
	 * @param queryParam 请求任务配置查询参数
	 * @return 请求任务配置集合
	 */
	List<TsGroupApiConfigListPcVO> selectListPC(TsGroupApiConfigQueryParam queryParam);

	/**
	 * 根据ID查找
	 *
	 * @param id
	 * @return
	 */
	TsGroupApiConfigInfoPcVo selectByIdPc(Long id);

	/**
	 * 批量插入
	 *
	 * @param insertList
	 */
	void insertBatch(List<TsReqLogEntity> insertList);
}
