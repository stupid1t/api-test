package com.bird.system.mapper;

import com.bird.system.domain.entity.SysDeptEntity;
import com.bird.system.domain.params.SysDeptQueryParams;
import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 部门管理 数据层
 *
 * @author bird
 */
public interface SysDeptMapper extends Mapper<SysDeptEntity> {
	/**
	 * 查询部门管理数据
	 *
	 * @param dept 部门信息
	 * @return 部门信息集合
	 */
	public List<SysDeptEntity> selectDeptList(SysDeptQueryParams dept);

	/**
	 * 根据角色ID查询部门树信息
	 *
	 * @param roleId 角色ID
	 * @return 选中部门列表
	 */
	public List<Integer> selectDeptListByRoleId(Long roleId);

	/**
	 * 根据ID查询所有子部门
	 *
	 * @param deptId 部门ID
	 * @return 部门列表
	 */
	public List<SysDeptEntity> selectChildrenDeptById(Long deptId);

	/**
	 * 修改子元素关系
	 *
	 * @param depts 子元素
	 * @return 结果
	 */
	public int updateDeptChildren(@Param("depts") List<SysDeptEntity> depts);

	/**
	 * 检查部门是否存在用户
	 *
	 * @param deptId
	 * @return
	 */
	int checkDeptExistUser(Long deptId);
}
