package com.bird.system.mapper;

import com.bird.system.domain.entity.SysErrorLogEntity;
import com.bird.system.domain.vo.SysErrorLogListPcVO;
import com.bird.system.domain.params.SysErrorLogQueryParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import java.util.List;

/**
 * 异常日志Dao接口
 *
 * @author bird
 * @date 2020-07-15
 */
@Repository
public interface SysErrorLogMapper extends Mapper<SysErrorLogEntity>{

    /**
     * 查询异常日志列表,PC专用不要轻易混用更改
     *
     * @param queryParam  异常日志查询参数
     * @return 异常日志集合
     */
    List<SysErrorLogListPcVO> selectListPC(SysErrorLogQueryParam queryParam);

}
