package com.bird.system.mapper;

import com.bird.system.domain.entity.SysLogininforEntity;
import com.bird.system.domain.params.SysLogininforQueryParams;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 系统访问日志情况信息 数据层
 *
 * @author bird
 */
public interface SysLogininforMapper extends Mapper<SysLogininforEntity> {

	/**
	 * 查询系统登录日志集合
	 *
	 * @param logininfor 访问日志对象
	 * @return 登录记录集合
	 */
	public List<SysLogininforEntity> selectLogininforList(SysLogininforQueryParams logininfor);

	/**
	 * 清空系统登录日志
	 *
	 * @return 结果
	 */
	public int cleanLogininfor();
}
