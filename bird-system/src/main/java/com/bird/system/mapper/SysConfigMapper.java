package com.bird.system.mapper;

import com.bird.system.domain.entity.SysConfigEntity;
import com.bird.system.domain.params.SysConfigQueryParams;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 参数配置 数据层
 *
 * @author bird
 */
public interface SysConfigMapper extends Mapper<SysConfigEntity> {

	/**
	 * 查询配置列表
	 *
	 * @param config
	 * @return
	 */
	List<SysConfigEntity> selectConfigList(SysConfigQueryParams config);
}