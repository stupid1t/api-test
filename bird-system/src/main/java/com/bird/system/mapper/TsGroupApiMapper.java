package com.bird.system.mapper;

import com.bird.system.domain.entity.TsGroupApiEntity;
import com.bird.system.domain.params.TsGroupApiQueryParam;
import com.bird.system.domain.vo.TsGroupApiListPcVO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 组接口Dao接口
 *
 * @author bird
 * @date 2020-09-23
 */
@Repository
public interface TsGroupApiMapper extends Mapper<TsGroupApiEntity> {

	/**
	 * 查询组接口列表,PC专用不要轻易混用更改
	 *
	 * @param queryParam 组接口查询参数
	 * @return 组接口集合
	 */
	List<TsGroupApiListPcVO> selectListPC(TsGroupApiQueryParam queryParam);

}
