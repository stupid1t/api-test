package com.bird.system.mapper;

import com.bird.system.domain.entity.TsPhoneDataEntity;
import com.bird.system.domain.params.TsPhoneDataQueryParam;
import com.bird.system.domain.vo.TsPhoneDataListPcVO;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 数据Dao接口
 *
 * @author bird
 * @date 2020-09-23
 */
@Repository
public interface TsPhoneDataMapper extends Mapper<TsPhoneDataEntity> {

	/**
	 * 查询数据列表,PC专用不要轻易混用更改
	 *
	 * @param queryParam 数据查询参数
	 * @return 数据集合
	 */
	List<TsPhoneDataListPcVO> selectListPC(TsPhoneDataQueryParam queryParam);

	/**
	 * 批量插入
	 *
	 * @param list
	 */
	void insertBatch(List<TsPhoneDataEntity> list);

}
