package com.bird.system.mapper;

import com.bird.system.domain.entity.SysOperLogEntity;
import com.bird.system.domain.params.SysOperLogQueryParams;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 操作日志 数据层
 *
 * @author bird
 */
public interface SysOperLogMapper extends Mapper<SysOperLogEntity> {


	/**
	 * 查询系统操作日志集合
	 *
	 * @param operLog 操作日志对象
	 * @return 操作日志集合
	 */
	public List<SysOperLogEntity> selectOperLogList(SysOperLogQueryParams operLog);

	/**
	 * 清空操作日志
	 */
	public void cleanOperLog();
}
