package com.bird.system.mapper;

import com.bird.system.domain.entity.SysUserEntity;
import com.bird.system.domain.params.SysUserQueryParams;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 用户表 数据层
 *
 * @author bird
 */
public interface SysUserMapper extends Mapper<SysUserEntity> {
	/**
	 * 根据条件分页查询用户列表
	 *
	 * @param sysUser 用户信息
	 * @return 用户信息集合信息
	 */
	public List<SysUserEntity> selectUserList(SysUserQueryParams sysUser);

	/**
	 * 通过用户名查询用户
	 *
	 * @param userName 用户名
	 * @return 用户对象信息
	 */
	public SysUserEntity selectUserByUserName(String userName);

	/**
	 * 通过用户ID查询用户
	 *
	 * @param userId 用户ID
	 * @return 用户对象信息
	 */
	public SysUserEntity selectUserById(Long userId);
}
