package com.bird.system.mapper;

import com.bird.system.domain.entity.SysDictTypeEntity;
import com.bird.system.domain.params.SysDictTypeQueryParams;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 字典表 数据层
 *
 * @author bird
 */
public interface SysDictTypeMapper extends Mapper<SysDictTypeEntity> {
	/**
	 * 根据条件分页查询字典类型
	 *
	 * @param dictType 字典类型信息
	 * @return 字典类型集合信息
	 */
	public List<SysDictTypeEntity> selectDictTypeList(SysDictTypeQueryParams dictType);
}
