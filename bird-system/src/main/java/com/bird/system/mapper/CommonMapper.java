package com.bird.system.mapper;

import com.alibaba.fastjson.JSONObject;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;


public interface CommonMapper {

    /**
     * 新增
     *
     * @param sql
     * @param params
     * @param <T>
     * @return
     */
    <T> int insert(@Param("sql") String sql, @Param("params") T params);

    /**
     * 删除
     *
     * @param sql
     */
    void delete(@Param("sql") String sql);

    /**
     * 修改
     *
     * @param sql
     * @param params
     */
    void update(@Param("sql") String sql, @Param("params") Map<String, Object> params);

    /**
     * 查询
     *
     * @param sql
     * @param params
     * @return
     */
    List<JSONObject> selectList(@Param("sql") String sql, @Param("params") Map<String, Object> params);

    /**
     * 查询某个字段
     *
     * @param sql
     * @param params
     * @return
     */
    Object queryField(@Param("sql") String sql, @Param("params") Map<String, Object> params);


}
