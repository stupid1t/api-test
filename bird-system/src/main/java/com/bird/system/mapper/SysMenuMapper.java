package com.bird.system.mapper;

import com.bird.system.domain.entity.SysMenuEntity;
import com.bird.system.domain.params.SysMenuQueryParams;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 菜单表 数据层
 *
 * @author bird
 */
public interface SysMenuMapper extends Mapper<SysMenuEntity> {
	/**
	 * 查询系统菜单列表
	 *
	 * @param menu 菜单信息
	 * @return 菜单列表
	 */
	public List<SysMenuEntity> selectMenuList(SysMenuQueryParams menu);

	/**
	 * 根据用户所有权限
	 *
	 * @return 权限列表
	 */
	public List<String> selectMenuPerms();

	/**
	 * 根据用户ID查询权限
	 *
	 * @param userId 用户ID
	 * @return 权限列表
	 */
	public List<String> selectMenuPermsByUserId(Long userId);

	/**
	 * 根据用户ID查询菜单
	 *
	 * @return 菜单列表
	 */
	public List<SysMenuEntity> selectMenuTreeAll();

	/**
	 * 根据用户ID查询菜单
	 *
	 * @param userId 用户ID
	 * @return 菜单列表
	 */
	public List<SysMenuEntity> selectMenuTreeByUserId(Long userId);

	/**
	 * 根据角色ID查询菜单树信息
	 *
	 * @param roleId 角色ID
	 * @return 选中菜单列表
	 */
	public List<Integer> selectMenuListByRoleId(Long roleId);
}
