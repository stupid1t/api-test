package com.bird.system.mapper;

import com.bird.system.domain.entity.TsGroupEntity;
import com.bird.system.domain.vo.TsGroupListPcVO;
import com.bird.system.domain.params.TsGroupQueryParam;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;
import tk.mybatis.mapper.common.Mapper;
import java.util.List;

/**
 * 组Dao接口
 *
 * @author bird
 * @date 2020-09-23
 */
@Repository
public interface TsGroupMapper extends Mapper<TsGroupEntity>{

    /**
     * 查询组列表,PC专用不要轻易混用更改
     *
     * @param queryParam  组查询参数
     * @return 组集合
     */
    List<TsGroupListPcVO> selectListPC(TsGroupQueryParam queryParam);

}
