package com.bird.system.mapper;

import com.bird.system.domain.entity.SysPostEntity;
import com.bird.system.domain.params.SysPostQueryParams;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 岗位信息 数据层
 *
 * @author bird
 */
public interface SysPostMapper extends Mapper<SysPostEntity> {
	/**
	 * 查询岗位数据集合
	 *
	 * @param post 岗位信息
	 * @return 岗位数据集合
	 */
	public List<SysPostEntity> selectPostList(SysPostQueryParams post);

	/**
	 * 根据用户ID获取岗位选择框列表
	 *
	 * @param userId 用户ID
	 * @return 选中岗位ID列表
	 */
	public List<Integer> selectPostListByUserId(Long userId);

	/**
	 * 查询用户所属岗位组
	 *
	 * @param userName 用户名
	 * @return 结果
	 */
	public List<SysPostEntity> selectPostsByUserName(String userName);
	
}
