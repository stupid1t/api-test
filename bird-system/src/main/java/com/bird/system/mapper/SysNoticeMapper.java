package com.bird.system.mapper;

import com.bird.system.domain.entity.SysNoticeEntity;
import com.bird.system.domain.params.SysNoticeQueryParams;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 通知公告表 数据层
 *
 * @author bird
 */
public interface SysNoticeMapper extends Mapper<SysNoticeEntity> {

	/**
	 * 查询公告列表
	 *
	 * @param notice 公告信息
	 * @return 公告集合
	 */
	public List<SysNoticeEntity> selectNoticeList(SysNoticeQueryParams notice);
}