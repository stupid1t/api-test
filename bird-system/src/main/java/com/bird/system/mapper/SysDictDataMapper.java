package com.bird.system.mapper;

import com.bird.system.domain.entity.SysDictDataEntity;
import com.bird.system.domain.params.SysDictDataQueryParams;
import tk.mybatis.mapper.common.Mapper;

import java.util.List;

/**
 * 字典表 数据层
 *
 * @author bird
 */
public interface SysDictDataMapper extends Mapper<SysDictDataEntity> {
	/**
	 * 根据条件分页查询字典数据
	 *
	 * @param dictData 字典数据信息
	 * @return 字典数据集合信息
	 */
	public List<SysDictDataEntity> selectDictDataList(SysDictDataQueryParams dictData);
}
