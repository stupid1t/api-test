package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;


@EnumDescription(name = "请求状态")
public enum ReqStatusEnum implements BaseEnum {

	HAS_NOT_STARTED("1", "未开始"),

	PAUSE("2", "暂停"),

	REQUESTING("3", "请求中"),

	EDN("4", "已结束"),
	;

	private String key;

	private String value;

	ReqStatusEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(ReqStatusEnum.class, value);
		}
	}
}
