package com.bird.system.enums;


import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;

/**
 * 性别
 *
 * @author bird
 */
@EnumDescription(name = "用户性别")
public enum SexEnum implements BaseEnum {

	MALE("1", "男"),

	FEMALE("2", "女"),

	UNKNOWN("3", "未知"),

	;
	private String key;

	private String value;

	SexEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(SexEnum.class, value);
		}
	}


}
