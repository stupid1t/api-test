package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;

/**
 * 用户状态
 *
 * @author bird
 */
@EnumDescription(name = "用户状态")
public enum UserStatus implements BaseEnum {

	OK("0", "正常"),
	DISABLE("1", "停用"),
	DELETED("2", "删除"),
	;
	private final String key;
	private final String value;

	UserStatus(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(UserStatus.class, value);
		}
	}
}
