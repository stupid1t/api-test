package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;


@EnumDescription(name = "组类型")
public enum GroupTypeEnum implements BaseEnum {

	PRIVATELY_OWNED("1", "私有"),

	OVERALL_SITUATION("2", "全局"),
	;

	private String key;

	private String value;

	GroupTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(GroupTypeEnum.class, value);
		}
	}
}
