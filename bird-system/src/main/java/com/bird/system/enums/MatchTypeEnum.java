package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;


@EnumDescription(name = "结果匹配模式")
public enum MatchTypeEnum implements BaseEnum {

	FULL_TEXT("1", "全文匹配"),

	REGXP("2", "正则匹配"),

	JSON_PATH("3", "jsonPath匹配"),
	;

	private String key;

	private String value;

	MatchTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(MatchTypeEnum.class, value);
		}
	}
}
