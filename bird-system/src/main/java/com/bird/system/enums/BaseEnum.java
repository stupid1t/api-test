package com.bird.system.enums;

import cn.hutool.extra.spring.SpringUtil;
import com.alibaba.fastjson.JSONObject;
import com.bird.system.service.ISysDictDataService;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 字典类
 *
 * @author: 625
 * @version: 2019年04月26日 15:15
 */
public interface BaseEnum<T> {

	Map<String, BaseEnum> dictCache = new ConcurrentHashMap<>();

	String getKey();

	String getValue();

	/***
	 * 根据枚举类型和值获取枚举
	 * @param enumClass
	 * @param value
	 * @return
	 */
	static <T extends BaseEnum<T>> BaseEnum valueOfEnum(Class<? extends BaseEnum> enumClass, String value) {
		String cacheKey = enumClass.getSimpleName() + "-" + value;
		if (value == null) {
			return null;
		}
		// 1.缓存中拿到直接返回
		BaseEnum cacheDict = dictCache.get(cacheKey);
		if (cacheDict != null) {
			return cacheDict;
		}
		// 2.遍历返回
		BaseEnum[] enums = enumClass.getEnumConstants();
		if (enums == null) {
			return null;
		}
		Optional<BaseEnum> optional = Arrays.asList(enums).stream().filter(baseEnum -> baseEnum.getKey().equals(value)).findAny();
		if (optional.isPresent()) {
			BaseEnum dictEnum = optional.get();
			dictCache.put(cacheKey, dictEnum);
			return dictEnum;
		}
		return null;
	}


	/**
	 * 查找是否存在
	 *
	 * @param enumClass
	 * @param value
	 * @return
	 */
	static boolean exist(Class<? extends BaseEnum> enumClass, String value) {
		// 1.缓存中查找
		String dictValue = SpringUtil.getBean(ISysDictDataService.class).selectDictLabel(enumClass.getSimpleName(), value);
		return StringUtils.isNotBlank(dictValue);
	}

	/***
	 * 根据枚举类型和值获取枚举
	 * @param enumClass
	 * @param value
	 * @return
	 */
	static JSONObject valueOfJson(Class<? extends BaseEnum> enumClass, String value) {
		if (value == null) {
			return null;
		}
		JSONObject json = new JSONObject();
		json.put("key", value);
		json.put("value", value);
		// 1.缓存中查找
		String dictValue = SpringUtil.getBean(ISysDictDataService.class).selectDictLabel(enumClass.getSimpleName(), value);
		if (StringUtils.isNotBlank(dictValue)) {
			json.put("value", dictValue);
		}
		return json;
	}


}

