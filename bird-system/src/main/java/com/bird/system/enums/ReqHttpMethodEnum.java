package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;


@EnumDescription(name = "请求方式")
public enum ReqHttpMethodEnum implements BaseEnum {

	GET("GET", "GET"),

	POST("POST", "POST"),

	PUT("PUT", "PUT"),

	DELETE("DELETE", "DELETE"),
	;

	private String key;

	private String value;

	ReqHttpMethodEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(ReqHttpMethodEnum.class, value);
		}
	}
}
