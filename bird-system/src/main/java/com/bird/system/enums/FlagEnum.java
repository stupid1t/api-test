package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;

/**
 * 是否
 *
 * @author: 625
 * @version: 2019年11月27日 22:38
 */
@EnumDescription(name = "逻辑判断")
public enum FlagEnum implements BaseEnum {

	YES("1", "是"),

	NO("0", "否"),
	;

	private String key;

	private String value;

	FlagEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(FlagEnum.class, value);
		}
	}
}
