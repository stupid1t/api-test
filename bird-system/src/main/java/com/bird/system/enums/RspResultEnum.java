package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;


@EnumDescription(name = "响应结果")
public enum RspResultEnum implements BaseEnum {

	SUCCESSFUL("1", "成功"),

	FAIL("2", "失败"),

	;

	private String key;

	private String value;

	RspResultEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(RspResultEnum.class, value);
		}
	}
}
