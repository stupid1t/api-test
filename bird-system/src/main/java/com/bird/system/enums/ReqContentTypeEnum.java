package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;


@EnumDescription(name = "请求协议类型")
public enum ReqContentTypeEnum implements BaseEnum {

	NONE("NONE", "NONE"),

	JSON("application/json", "application/json"),

	FORM("application/x-www-form-urlencoded", "application/x-www-form-urlencoded"),

	FORM_DATA("multipart/form-data; boundary=FormBoundary", "multipart/form-data; boundary=FormBoundary"),

	XML("application/xml", "application/xml"),
	;

	private String key;

	private String value;

	ReqContentTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(ReqContentTypeEnum.class, value);
		}
	}
}
