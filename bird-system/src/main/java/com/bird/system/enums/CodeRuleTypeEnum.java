package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;

/**
 * 用户状态
 *
 * @author bird
 */
@EnumDescription(name = "编码生成类型")
public enum CodeRuleTypeEnum implements BaseEnum {

	TIME_RANDOM("1", "日期+随机数"),

	TIME_SERIAL("2", "日期+递增序列"),

	SERIAL("3", "递增序列"),
	;
	private String key;

	private String value;

	CodeRuleTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(CodeRuleTypeEnum.class, value);
		}
	}
}
