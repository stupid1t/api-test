package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;


@EnumDescription(name = "代理地址")
public enum ProxyHostEnum implements BaseEnum {

	NONE("无", "无"),

	LOCALHOST("本机80", "proxy://root:password@127.0.0.1:80/authorization"),
	;

	private String key;

	private String value;

	ProxyHostEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(ProxyHostEnum.class, value);
		}
	}
}
