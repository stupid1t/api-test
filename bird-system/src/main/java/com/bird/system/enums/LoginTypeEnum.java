package com.bird.system.enums;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.annotation.EnumDescription;
import com.fasterxml.jackson.databind.util.StdConverter;

/**
 * 登录用户类型
 *
 * @author: 625
 * @version: 2019年11月27日 22:38
 */
@EnumDescription(name = "登录用户类型")
public enum LoginTypeEnum implements BaseEnum {

	SYSTEM("SYSTEM", "系统用户"),
	CAT_MEMBER("CAT_USER", "小程序用户"),
	;

	private String key;

	private String value;

	LoginTypeEnum(String key, String value) {
		this.key = key;
		this.value = value;
	}

	@Override
	public String getKey() {
		return this.key;
	}

	@Override
	public String getValue() {
		return value;
	}

	/**
	 * 序列化使用
	 */
	public static class Covert extends StdConverter<String, JSONObject> {
		@Override
		public JSONObject convert(String value) {
			return BaseEnum.valueOfJson(LoginTypeEnum.class, value);
		}
	}
}
