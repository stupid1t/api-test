package com.bird.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.constant.Constants;
import com.bird.common.exception.CustomException;
import com.bird.system.domain.entity.SysPostEntity;
import com.bird.system.domain.params.SysPostQueryParams;
import com.bird.system.domain.params.SysPostUpdateParams;
import com.bird.system.mapper.SysPostMapper;
import com.bird.system.mapper.SysUserPostMapper;
import com.bird.system.service.ISysPostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 岗位信息 服务层处理
 *
 * @author bird
 */
@Service
public class SysPostServiceImpl extends IBaseServiceImpl<SysPostEntity, SysPostMapper> implements ISysPostService {

	@Autowired
	private SysUserPostMapper userPostMapper;

	/**
	 * 查询岗位信息集合
	 *
	 * @param post 岗位信息
	 * @return 岗位信息集合
	 */
	@Override
	public List<SysPostEntity> selectPostList(SysPostQueryParams post) {
		return mapper.selectPostList(post);
	}

	/**
	 * 查询所有岗位
	 *
	 * @return 岗位列表
	 */
	@Override
	public List<SysPostEntity> selectPostAll() {
		return mapper.selectAll();
	}

	/**
	 * 通过岗位ID查询岗位信息
	 *
	 * @param postId 岗位ID
	 * @return 角色对象信息
	 */
	@Override
	public SysPostEntity selectPostById(Long postId) {
		return mapper.selectByPrimaryKey(postId);
	}

	/**
	 * 根据用户ID获取岗位选择框列表
	 *
	 * @param userId 用户ID
	 * @return 选中岗位ID列表
	 */
	@Override
	public List<Integer> selectPostListByUserId(Long userId) {
		return mapper.selectPostListByUserId(userId);
	}

	/**
	 * 校验岗位名称是否唯一
	 *
	 * @param post 岗位信息
	 * @return 结果
	 */
	@Override
	public String checkPostNameUnique(SysPostUpdateParams post) {
		Long postId = ObjectUtil.isNull(post.getId()) ? -1L : post.getId();
		SysPostEntity info = mapper.selectOneByExample(SelectWhere().andEqualTo(SysPostEntity::getPostName, post.getPostName()).end());
		if (ObjectUtil.isNotNull(info) && info.getId().longValue() != postId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	/**
	 * 校验岗位编码是否唯一
	 *
	 * @param post 岗位信息
	 * @return 结果
	 */
	@Override
	public String checkPostCodeUnique(SysPostUpdateParams post) {
		Long postId = ObjectUtil.isNull(post.getId()) ? -1L : post.getId();
		SysPostEntity info = mapper.selectOneByExample(SelectWhere().andEqualTo(SysPostEntity::getPostCode, post.getPostCode()).end());
		if (ObjectUtil.isNotNull(info) && info.getId().longValue() != postId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	/**
	 * 通过岗位ID查询岗位使用数量
	 *
	 * @param postId 岗位ID
	 * @return 结果
	 */
	@Override
	public int countUserPostById(Long postId) {
		return userPostMapper.countUserPostById(postId);
	}

	/**
	 * 删除岗位信息
	 *
	 * @param postId 岗位ID
	 * @return 结果
	 */
	@Override
	public int deletePostById(Long postId) {
		return mapper.deleteByPrimaryKey(postId);
	}

	/**
	 * 批量删除岗位信息
	 *
	 * @param postIds 需要删除的岗位ID
	 * @return 结果
	 * @throws Exception 异常
	 */
	@Override
	public int deletePostByIds(Long[] postIds) {
		for (Long postId : postIds) {
			SysPostEntity post = selectPostById(postId);
			if (countUserPostById(postId) > 0) {
				throw new CustomException(String.format("%1$s已分配,不能删除", post.getPostName()));
			}
		}
		return this.deleteByIds(postIds);
	}

	/**
	 * 新增保存岗位信息
	 *
	 * @param post 岗位信息
	 * @return 结果
	 */
	@Override
	public int insertPost(SysPostEntity post) {
		return mapper.insertSelective(post);
	}

	/**
	 * 修改保存岗位信息
	 *
	 * @param post 岗位信息
	 * @return 结果
	 */
	@Override
	public int updatePost(SysPostEntity post) {
		return this.updateById(post);
	}
}
