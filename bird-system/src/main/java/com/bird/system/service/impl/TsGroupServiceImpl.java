package com.bird.system.service.impl;

import com.bird.common.utils.covert.GroupIdCovert;
import com.bird.system.domain.entity.TsGroupEntity;
import com.bird.system.domain.params.TsGroupQueryParam;
import com.bird.system.domain.params.TsGroupUpdateParam;
import com.bird.system.domain.vo.TsGroupListPcVO;
import com.bird.system.mapper.TsGroupMapper;
import com.bird.system.service.ITsGroupService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 组Service业务层处理
 *
 * @author bird
 * @date 2020-09-23
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TsGroupServiceImpl extends IBaseServiceImpl<TsGroupEntity, TsGroupMapper> implements ITsGroupService {

	/**
	 * 查询组列表
	 *
	 * @param queryParam 组 查询参数
	 * @return 组
	 */
	@Override
	public List<TsGroupListPcVO> selectListPC(TsGroupQueryParam queryParam) {
		List<TsGroupListPcVO> tsGroups = mapper.selectListPC(queryParam);
		return tsGroups;
	}

	/**
	 * 新增组
	 *
	 * @param updateParam 组 新增参数
	 * @return 结果
	 */
	@Override
	public Long insertPC(TsGroupUpdateParam updateParam, Long userId) {
		TsGroupEntity entity = updateParam.covert();
		entity.setCreSb(userId);
		this.insert(entity);
		return entity.getId();
	}

	/**
	 * 修改组
	 *
	 * @param updateParam 组 修改参数
	 * @return 结果
	 */
	@Override
	public int updateByIdPC(TsGroupUpdateParam updateParam) {
		TsGroupEntity entity = updateParam.covert();
		int result = this.updateById(entity);
		// 删除本地缓存
		GroupIdCovert.existUser.remove(updateParam.getId());
		return result;
	}

	@Override
	public String selectNameById(Long id) {
		return this.selectField(TsGroupEntity::getTheName, SelectWhere().andEqualTo(TsGroupEntity::getId, id).end());
	}
}
