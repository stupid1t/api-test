package com.bird.system.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.constant.Coder;
import com.bird.common.exception.CustomException;
import com.bird.common.utils.StringUtil;
import com.bird.system.mapper.CommonMapper;
import com.bird.system.service.ICommonService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class CommonServiceImpl implements ICommonService {

	private static final Logger logger = LoggerFactory.getLogger(CommonServiceImpl.class);

	@Autowired
	private CommonMapper commonDao;


	@Override
	public <T> List<T> queryListEntity(String sql, Map<String, Object> params, Class<T> clz) {
		List<JSONObject> result = commonDao.selectList(sql, params);
		return result.stream().map(n -> n.toJavaObject(clz)).collect(Collectors.toList());
	}

	@Override
	public <T> T queryEntity(String sql, Map<String, Object> params, Class<T> clz) {
		List<T> result = queryListEntity(sql, params, clz);
		return result.isEmpty() ? null : result.get(0);
	}

	@Override
	public <T> void insertEntity(String sql, T entity) {
		commonDao.insert(sql, entity);
	}

	@Override
	public List<JSONObject> queryListJson(String sql, Map<String, Object> params) {
		return commonDao.selectList(sql, params);
	}

	@Override
	public JSONObject queryJson(String sql, Map<String, Object> params) {
		List<JSONObject> result = queryListJson(sql, params);
		return result.isEmpty() ? null : result.get(0);
	}

	@Override
	public void delete(String sql) {
		commonDao.delete(sql);
	}

	@Override
	public <T> T queryField(String sql, Map<String, Object> params, Class<T> clz) {
		return (T) commonDao.queryField(sql, params);
	}


	@Override
	public void update(String sql, Map<String, Object> params) {
		commonDao.update(sql, params);
	}


	@Override
	public void checkRepeat(Long id, Class<?> tableCls, Map<String, Object> fieldInfo, String message) {
		if (fieldInfo == null) {
			throw new UnsupportedOperationException("fieldInfo 不能为空！必须设置检测重复的条件！");
		}
		boolean repeat = false;
		// 判定是更新还是新增
		boolean update = id != null && id > 0;
		StringBuilder sql = new StringBuilder(" select id from  " + StringUtil.toUnderScoreCase(tableCls.getSimpleName()) + "  where  1 = 1 ");
		fieldInfo.forEach((filedName, fieldValue) -> {
			sql.append(" and " + StringUtil.toUnderScoreCase(filedName) + " = '" + fieldValue + "' ");
		});
		JSONObject object = this.queryJson(sql.toString(), null);
		if (update) {
			repeat = object != null && !object.getLong("id").equals(id);
		} else {
			repeat = object != null;
		}
		if (repeat) {
			throw new CustomException(Coder.System.CODE_REPETITION.build(message));
		}
	}

	@Override
	public Long findMaxSort(Class<?> tableCls, String sortField, Map<String, Object> fieldInfo) {
		Long maxId = 0L;
		sortField = StringUtil.toUnderScoreCase(sortField);
		StringBuilder sql = new StringBuilder(" select " + sortField + " from  " + StringUtil.toUnderScoreCase(tableCls.getSimpleName()) + "  where  1 = 1 ");
		if (fieldInfo != null) {
			fieldInfo.forEach((filedName, fieldValue) -> {
				sql.append(" and " + StringUtil.toUnderScoreCase(filedName) + " = '" + fieldValue + "' ");
			});
		}
		sql.append(" order by ").append(sortField).append(" desc limit 1");
		JSONObject object = this.queryJson(sql.toString(), null);
		if (object != null) {
			maxId = object.getLong(sortField);
		}
		return maxId;
	}
}