package com.bird.system.service;

import com.bird.system.domain.entity.TsGroupApiConfigEntity;
import com.bird.system.domain.params.TsGroupApiConfigQueryParam;
import com.bird.system.domain.params.TsGroupApiConfigUpdateParam;
import com.bird.system.domain.vo.TsGroupApiConfigInfoPcVo;
import com.bird.system.domain.vo.TsGroupApiConfigListPcVO;

import java.util.List;

/**
 * 请求任务配置Service接口
 *
 * @author bird
 * @date 2020-09-23
 */
public interface ITsGroupApiConfigService extends IBaseService<TsGroupApiConfigEntity> {

	/**
	 * 查询请求任务配置列表
	 *
	 * @param queryParam 请求任务配置 查询对象
	 * @return 请求任务配置集合
	 */
	List<TsGroupApiConfigListPcVO> selectListPC(TsGroupApiConfigQueryParam queryParam);


	/**
	 * 新增请求任务配置
	 *
	 * @param updateParam 请求任务配置 新增对象
	 * @return 结果
	 */
	Long insertPC(TsGroupApiConfigUpdateParam updateParam, Long userId);

	/**
	 * 修改请求任务配置
	 *
	 * @param updateParam 请求任务配置 修改参数
	 * @return 结果
	 */
	int updateByIdPC(TsGroupApiConfigUpdateParam updateParam, Long userId);

	/**
	 * 根据ID查找
	 *
	 * @param id
	 * @return
	 */
	TsGroupApiConfigInfoPcVo selectByIdPc(Long id);

	/**
	 * 开始执行任务配置
	 *
	 * @param id
	 */
	void start(Long id);

	/**
	 * 暂停执行
	 *
	 * @param id
	 */
	void pause(Long id);

	/**
	 * 继续执行
	 *
	 * @param id
	 */
	void continueExcute(Long id);

	/**
	 * 终止执行
	 *
	 * @param id
	 */
	void stop(Long id);

	/**
	 * 根据ID查询名字
	 *
	 * @param id
	 * @return
	 */
	String selectNameById(Long id);

}
