package com.bird.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.constant.Constants;
import com.bird.system.domain.entity.SysConfigEntity;
import com.bird.system.domain.params.SysConfigQueryParams;
import com.bird.system.domain.params.SysConfigUpdateParams;
import com.bird.system.mapper.SysConfigMapper;
import com.bird.system.service.ISysConfigService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 参数配置 服务层实现
 *
 * @author bird
 */
@Service
public class SysConfigServiceImpl extends IBaseServiceImpl<SysConfigEntity, SysConfigMapper> implements ISysConfigService {

	/**
	 * 校验参数键名是否唯一
	 *
	 * @param config 参数配置信息
	 * @return 结果
	 */
	@Override
	public String checkConfigKeyUnique(SysConfigUpdateParams config) {
		Long configId = ObjectUtil.isNull(config.getId()) ? -1L : config.getId();
		SysConfigEntity info = mapper.selectOneByExample(SelectWhere().andEqualTo(SysConfigEntity::getConfigKey, config.getConfigKey()).end());
		if (ObjectUtil.isNotNull(info) && info.getId().longValue() != configId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	@Override
	public List<SysConfigEntity> selectConfigList(SysConfigQueryParams config) {
		return mapper.selectConfigList(config);
	}

	@Override
	public String selectConfigByKey(String configKey) {
		SysConfigEntity info = mapper.selectOneByExample(
				Select().column("configValue")
						.where()
						.andEqualTo(SysConfigEntity::getConfigKey, configKey)
						.end()
		);
		return info.getConfigValue();
	}
}
