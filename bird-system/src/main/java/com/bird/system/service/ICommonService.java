package com.bird.system.service;

import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

/**
 * 通用自定义sql查询
 */
public interface ICommonService {


	/**
	 * 查询实体列表
	 *
	 * @param sql
	 * @param params
	 * @param clz
	 * @param <T>
	 * @return
	 */
	<T> List<T> queryListEntity(String sql, Map<String, Object> params, Class<T> clz);

	/**
	 * 查询一个实体
	 *
	 * @param sql
	 * @param params
	 * @param clz
	 * @param <T>
	 * @return
	 */
	<T> T queryEntity(String sql, Map<String, Object> params, Class<T> clz);

	/**
	 * 插入
	 *
	 * @param sql
	 * @param entity
	 * @param <T>
	 */
	<T> void insertEntity(String sql, T entity);

	/**
	 * 查询列表
	 *
	 * @param sql
	 * @param params
	 * @return
	 */
	List<JSONObject> queryListJson(String sql, Map<String, Object> params);

	/**
	 * 查询一个对象
	 *
	 * @param sql
	 * @param params
	 * @return
	 */
	JSONObject queryJson(String sql, Map<String, Object> params);

	/**
	 * 查询单个字段
	 *
	 * @param sql
	 * @param params
	 * @param clz
	 * @param <T>
	 * @return
	 */
	<T> T queryField(String sql, Map<String, Object> params, Class<T> clz);

	/**
	 * 删除
	 *
	 * @param sql
	 */
	void delete(String sql);

	/**
	 * 修改
	 *
	 * @param sql
	 * @param params
	 */
	void update(String sql, Map<String, Object> params);

	/**
	 * 检测表字段是否重复
	 *
	 * @param id        主键
	 * @param tableCls  表名
	 * @param fieldInfo 检测重复的字段和值
	 * @param message   如果重复，提示消息
	 */
	void checkRepeat(Long id, Class<?> tableCls, Map<String, Object> fieldInfo, String message);

	/**
	 * 查询最大序号
	 *
	 * @param tableCls  表名
	 * @param sortField 排序字段
	 * @param fieldInfo 查询条件
	 */
	Long findMaxSort(Class<?> tableCls, String sortField, Map<String, Object> fieldInfo);
}
