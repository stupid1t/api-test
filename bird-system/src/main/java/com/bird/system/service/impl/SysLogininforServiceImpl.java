package com.bird.system.service.impl;

import com.bird.system.domain.entity.SysLogininforEntity;
import com.bird.system.domain.params.SysLogininforQueryParams;
import com.bird.system.mapper.SysLogininforMapper;
import com.bird.system.service.ISysLogininforService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 系统访问日志情况信息 服务层处理
 *
 * @author bird
 */
@Service
public class SysLogininforServiceImpl extends IBaseServiceImpl<SysLogininforEntity, SysLogininforMapper> implements ISysLogininforService {


	/**
	 * 新增系统登录日志
	 *
	 * @param logininfor 访问日志对象
	 */
	@Override
	public void insertLogininfor(SysLogininforEntity logininfor) {
		mapper.insertSelective(logininfor);
	}

	/**
	 * 查询系统登录日志集合
	 *
	 * @param logininfor 访问日志对象
	 * @return 登录记录集合
	 */
	@Override
	public List<SysLogininforEntity> selectLogininforList(SysLogininforQueryParams logininfor) {
		return mapper.selectLogininforList(logininfor);
	}

	/**
	 * 批量删除系统登录日志
	 *
	 * @param infoIds 需要删除的登录日志ID
	 * @return
	 */
	@Override
	public int deleteLogininforByIds(Long[] infoIds) {
		return this.deleteByIds(infoIds);
	}

	/**
	 * 清空系统登录日志
	 */
	@Override
	public void cleanLogininfor() {
		mapper.cleanLogininfor();
	}
}
