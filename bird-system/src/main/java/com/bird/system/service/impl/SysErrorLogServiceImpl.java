package com.bird.system.service.impl;

import com.bird.system.domain.entity.SysErrorLogEntity;
import com.bird.system.domain.params.SysErrorLogQueryParam;
import com.bird.system.domain.params.SysErrorLogUpdateParam;
import com.bird.system.domain.vo.SysErrorLogListPcVO;
import com.bird.system.mapper.SysErrorLogMapper;
import com.bird.system.service.ISysErrorLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 异常日志Service业务层处理
 *
 * @author bird
 * @date 2020-07-15
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class SysErrorLogServiceImpl extends IBaseServiceImpl<SysErrorLogEntity, SysErrorLogMapper> implements ISysErrorLogService {

	/**
	 * 查询异常日志列表
	 *
	 * @param queryParam 异常日志 查询参数
	 * @return 异常日志
	 */
	@Override
	public List<SysErrorLogListPcVO> selectListPC(SysErrorLogQueryParam queryParam) {
		List<SysErrorLogListPcVO> sysErrorLogs = mapper.selectListPC(queryParam);
		return sysErrorLogs;
	}

	/**
	 * 新增异常日志
	 *
	 * @param updateParam 异常日志 新增参数
	 * @return 结果
	 */
	@Override
	public Long insertPC(SysErrorLogUpdateParam updateParam) {
		SysErrorLogEntity entity = updateParam.covert();
		this.insert(entity);
		return entity.getId();
	}

	/**
	 * 修改异常日志
	 *
	 * @param updateParam 异常日志 修改参数
	 * @return 结果
	 */
	@Override
	public int updateByIdPC(SysErrorLogUpdateParam updateParam) {
		SysErrorLogEntity entity = updateParam.covert();
		int result = this.updateById(entity);
		return result;
	}

}
