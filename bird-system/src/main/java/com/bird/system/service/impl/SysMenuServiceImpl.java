package com.bird.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.constant.Constants;
import com.bird.common.utils.StringUtil;
import com.bird.system.domain.bo.TreeSelect;
import com.bird.system.domain.entity.SysMenuEntity;
import com.bird.system.domain.params.SysMenuQueryParams;
import com.bird.system.domain.params.SysMenuUpdateParams;
import com.bird.system.domain.vo.MetaVo;
import com.bird.system.domain.vo.RouterVo;
import com.bird.system.mapper.SysMenuMapper;
import com.bird.system.mapper.SysRoleMenuMapper;
import com.bird.system.service.ISysMenuService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * 菜单 业务层处理
 *
 * @author bird
 */
@Service
public class SysMenuServiceImpl extends IBaseServiceImpl<SysMenuEntity, SysMenuMapper> implements ISysMenuService {
	public static final String PREMISSION_STRING = "perms[\"{0}\"]";

	@Autowired
	private SysRoleMenuMapper roleMenuMapper;

	/**
	 * 查询系统菜单列表
	 *
	 * @param menu 菜单信息
	 * @return 菜单列表
	 */
	@Override
	public List<SysMenuEntity> selectMenuList(SysMenuQueryParams menu) {
		List<SysMenuEntity> menuList = mapper.selectMenuList(menu);
		return menuList;
	}

	/**
	 * 根据用户ID查询权限
	 *
	 * @param userId 用户ID
	 * @return 权限列表
	 */
	@Override
	public Set<String> selectMenuPermsByUserId(Long userId) {
		List<String> perms = mapper.selectMenuPermsByUserId(userId);
		Set<String> permsSet = new HashSet<>();
		for (String perm : perms) {
			if (StringUtil.isNotEmpty(perm)) {
				permsSet.addAll(Arrays.asList(perm.trim().split(",")));
			}
		}
		return permsSet;
	}

	/**
	 * 根据用户名称查询菜单
	 *
	 * @param userId 用户名称
	 * @return 菜单列表
	 */
	@Override
	public List<SysMenuEntity> selectMenuTreeByUserId(Long userId) {
		List<SysMenuEntity> menus = null;
		if (userId != null && 1L == userId) {
			//管理员
			menus = mapper.selectMenuTreeAll();
		} else {
			menus = mapper.selectMenuTreeByUserId(userId);
		}
		return getChildPerms(menus, 0);
	}

	/**
	 * 根据角色ID查询菜单树信息
	 *
	 * @param roleId 角色ID
	 * @return 选中菜单列表
	 */
	@Override
	public List<Integer> selectMenuListByRoleId(Long roleId) {
		return mapper.selectMenuListByRoleId(roleId);
	}

	/**
	 * 构建前端路由所需要的菜单
	 *
	 * @param menus 菜单列表
	 * @return 路由列表
	 */
	@Override
	public List<RouterVo> buildMenus(List<SysMenuEntity> menus) {
		List<RouterVo> routers = new LinkedList<RouterVo>();
		for (SysMenuEntity menu : menus) {
			RouterVo router = new RouterVo();
			router.setName(StringUtils.capitalize(menu.getPath()));
			router.setPath(getRouterPath(menu));
			router.setComponent(StringUtil.isEmpty(menu.getComponent()) ? "Layout" : menu.getComponent());
			router.setMeta(new MetaVo(menu.getMenuName(), menu.getIcon()));
			List<SysMenuEntity> cMenus = menu.getChildren();
			if (!cMenus.isEmpty() && cMenus.size() > 0 && "M".equals(menu.getMenuType())) {
				router.setAlwaysShow(true);
				router.setRedirect("noRedirect");
				router.setChildren(buildMenus(cMenus));
			}
			routers.add(router);
		}
		return routers;
	}

	/**
	 * 构建前端所需要树结构
	 *
	 * @param menus 菜单列表
	 * @return 树结构列表
	 */
	@Override
	public List<SysMenuEntity> buildMenuTree(List<SysMenuEntity> menus) {
		List<SysMenuEntity> returnList = new ArrayList<SysMenuEntity>();
		for (Iterator<SysMenuEntity> iterator = menus.iterator(); iterator.hasNext(); ) {
			SysMenuEntity t = (SysMenuEntity) iterator.next();
			// 根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (t.getParentId() == 0) {
				recursionFn(menus, t);
				returnList.add(t);
			}
		}
		if (returnList.isEmpty()) {
			returnList = menus;
		}
		return returnList;
	}

	/**
	 * 构建前端所需要下拉树结构
	 *
	 * @param menus 菜单列表
	 * @return 下拉树结构列表
	 */
	@Override
	public List<TreeSelect> buildMenuTreeSelect(List<SysMenuEntity> menus) {
		List<SysMenuEntity> menuTrees = buildMenuTree(menus);
		return menuTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
	}

	/**
	 * 根据菜单ID查询信息
	 *
	 * @param menuId 菜单ID
	 * @return 菜单信息
	 */
	@Override
	public SysMenuEntity selectMenuById(Long menuId) {
		return mapper.selectByPrimaryKey(menuId);
	}

	/**
	 * 是否存在菜单子节点
	 *
	 * @param menuId 菜单ID
	 * @return 结果
	 */
	@Override
	public boolean hasChildByMenuId(Long menuId) {
		int result = mapper.selectCountByExample(SelectWhere().andEqualTo(SysMenuEntity::getParentId, menuId).end());
		return result > 0 ? true : false;
	}

	/**
	 * 查询菜单使用数量
	 *
	 * @param menuId 菜单ID
	 * @return 结果
	 */
	@Override
	public boolean checkMenuExistRole(Long menuId) {
		int result = roleMenuMapper.checkMenuExistRole(menuId);
		return result > 0 ? true : false;
	}

	/**
	 * 新增保存菜单信息
	 *
	 * @param menu 菜单信息
	 * @return 结果
	 */
	@Override
	public int insertMenu(SysMenuEntity menu) {
		return mapper.insertSelective(menu);
	}

	/**
	 * 修改保存菜单信息
	 *
	 * @param menu 菜单信息
	 * @return 结果
	 */
	@Override
	public int updateMenu(SysMenuEntity menu) {
		return mapper.updateByPrimaryKeySelective(menu);
	}

	/**
	 * 删除菜单管理信息
	 *
	 * @param menuId 菜单ID
	 * @return 结果
	 */
	@Override
	public int deleteMenuById(Long menuId) {
		return this.deleteById(menuId);
	}

	/**
	 * 校验菜单名称是否唯一
	 *
	 * @param menu 菜单信息
	 * @return 结果
	 */
	@Override
	public String checkMenuNameUnique(SysMenuUpdateParams menu) {
		Long menuId = ObjectUtil.isNull(menu.getId()) ? -1L : menu.getId();
		SysMenuEntity info = mapper.selectOneByExample(
				SelectWhere()
						.andEqualTo(SysMenuEntity::getMenuName, menu.getMenuName())
						.andEqualTo(SysMenuEntity::getParentId, menu.getParentId())
						.end()
		);
		if (ObjectUtil.isNotNull(info) && info.getId().longValue() != menuId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	/**
	 * 获取路由地址
	 *
	 * @param menu 菜单信息
	 * @return 路由地址
	 */
	public String getRouterPath(SysMenuEntity menu) {
		String routerPath = menu.getPath();
		// 非外链并且是一级目录
		if (0 == menu.getParentId() && "1".equals(menu.getIsFrame())) {
			routerPath = "/" + menu.getPath();
		}
		return routerPath;
	}

	/**
	 * 根据父节点的ID获取所有子节点
	 *
	 * @param list     分类表
	 * @param parentId 传入的父节点ID
	 * @return String
	 */
	public List<SysMenuEntity> getChildPerms(List<SysMenuEntity> list, int parentId) {
		List<SysMenuEntity> returnList = new ArrayList<SysMenuEntity>();
		for (Iterator<SysMenuEntity> iterator = list.iterator(); iterator.hasNext(); ) {
			SysMenuEntity t = (SysMenuEntity) iterator.next();
			// 一、根据传入的某个父节点ID,遍历该父节点的所有子节点
			if (t.getParentId() == parentId) {
				recursionFn(list, t);
				returnList.add(t);
			}
		}
		return returnList;
	}

	/**
	 * 递归列表
	 *
	 * @param list
	 * @param t
	 */
	private void recursionFn(List<SysMenuEntity> list, SysMenuEntity t) {
		// 得到子节点列表
		List<SysMenuEntity> childList = getChildList(list, t);
		t.setChildren(childList);
		for (SysMenuEntity tChild : childList) {
			if (hasChild(list, tChild)) {
				// 判断是否有子节点
				Iterator<SysMenuEntity> it = childList.iterator();
				while (it.hasNext()) {
					SysMenuEntity n = (SysMenuEntity) it.next();
					recursionFn(list, n);
				}
			}
		}
	}

	/**
	 * 得到子节点列表
	 */
	private List<SysMenuEntity> getChildList(List<SysMenuEntity> list, SysMenuEntity t) {
		List<SysMenuEntity> tlist = new ArrayList<SysMenuEntity>();
		Iterator<SysMenuEntity> it = list.iterator();
		while (it.hasNext()) {
			SysMenuEntity n = (SysMenuEntity) it.next();
			if (n.getParentId().longValue() == t.getId().longValue()) {
				tlist.add(n);
			}
		}
		return tlist;
	}

	/**
	 * 判断是否有子节点
	 */
	private boolean hasChild(List<SysMenuEntity> list, SysMenuEntity t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}
}
