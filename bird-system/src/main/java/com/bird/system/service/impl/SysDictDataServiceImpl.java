package com.bird.system.service.impl;

import com.bird.common.constant.Constants;
import com.bird.system.domain.entity.SysDictDataEntity;
import com.bird.system.domain.params.SysDictDataQueryParams;
import com.bird.system.mapper.SysDictDataMapper;
import com.bird.system.service.ISysDictDataService;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 字典 业务层处理
 *
 * @author bird
 */
@Service
public class SysDictDataServiceImpl extends IBaseServiceImpl<SysDictDataEntity, SysDictDataMapper> implements ISysDictDataService {

	/**
	 * 根据条件分页查询字典数据
	 *
	 * @param dictData 字典数据信息
	 * @return 字典数据集合信息
	 */
	@Override
	public List<SysDictDataEntity> selectDictDataList(SysDictDataQueryParams dictData) {
		return mapper.selectDictDataList(dictData);
	}

	/**
	 * 根据字典类型查询字典数据
	 *
	 * @param dictType 字典类型
	 * @return 字典数据集合信息
	 */
	@Override
	@Cacheable(value = Constants.System.CACHE_KEY_PREFIX + Constants.System.CACHE_DICT, key = "#dictType+':Double:All'", unless = "#result == null ")
	public List<SysDictDataEntity> selectDictDataByType(String dictType) {
		List<SysDictDataEntity> sysDictDataEntities = this.selectList(Select().orderBy(SysDictDataEntity::getDictSort).where()
				.andEqualTo(SysDictDataEntity::getStatus, "0")
				.andEqualTo(SysDictDataEntity::getDictType, dictType)
				.end()
		);
		return sysDictDataEntities;
	}

	/**
	 * 根据字典类型和字典键值查询字典数据信息
	 *
	 * @param dictType  字典类型
	 * @param dictValue 字典键值
	 * @return 字典标签
	 */
	@Override
	@Cacheable(value = Constants.System.CACHE_KEY_PREFIX + Constants.System.CACHE_DICT, key = "#dictType+':Signle:'+#dictValue", unless = "#result == null ")
	public String selectDictLabel(String dictType, String dictValue) {
		SysDictDataEntity sysDictDataEntities = this.selectOne(Select().column("dictLabel").where()
				.andEqualTo(SysDictDataEntity::getDictType, dictType)
				.andEqualTo(SysDictDataEntity::getDictValue, dictValue)
				.end()
		);
		return sysDictDataEntities == null ? null : sysDictDataEntities.getDictLabel();
	}

	/**
	 * 根据字典数据ID查询信息
	 *
	 * @param dictCode 字典数据ID
	 * @return 字典数据
	 */
	@Override
	public SysDictDataEntity selectDictDataById(Long dictCode) {
		return mapper.selectByPrimaryKey(dictCode);
	}

	/**
	 * 通过字典ID删除字典数据信息
	 *
	 * @param dictCode 字典数据ID
	 * @return 结果
	 */
	@Override
	public int deleteDictDataById(Long dictCode) {
		return mapper.deleteByPrimaryKey(dictCode);
	}

	/**
	 * 批量删除字典数据信息
	 *
	 * @param dictCodes 需要删除的字典数据ID
	 * @return 结果
	 */
	@Override
	public int deleteDictDataByIds(Long[] dictCodes) {
		return this.deleteByIds(dictCodes);
	}

	/**
	 * 新增保存字典数据信息
	 *
	 * @param dictData 字典数据信息
	 * @return 结果
	 */
	@Override
	public int insertDictData(SysDictDataEntity dictData) {
		return mapper.insertSelective(dictData);
	}

	/**
	 * 修改保存字典数据信息
	 *
	 * @param dictData 字典数据信息
	 * @return 结果
	 */
	@Override
	public int updateDictData(SysDictDataEntity dictData) {
		return mapper.updateByPrimaryKeySelective(dictData);
	}

}
