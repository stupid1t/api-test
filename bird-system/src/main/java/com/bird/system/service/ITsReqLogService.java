package com.bird.system.service;

import com.bird.system.domain.entity.TsReqLogEntity;
import com.bird.system.domain.params.TsReqLogQueryParam;
import com.bird.system.domain.params.TsReqLogUpdateParam;
import com.bird.system.domain.vo.TsReqLogListPcVO;

import java.util.List;

/**
 * 请求日志Service接口
 *
 * @author bird
 * @date 2020-09-23
 */
public interface ITsReqLogService extends IBaseService<TsReqLogEntity> {

	/**
	 * 查询请求日志列表
	 *
	 * @param queryParam 请求日志 查询对象
	 * @return 请求日志集合
	 */
	List<TsReqLogListPcVO> selectListPC(TsReqLogQueryParam queryParam);


	/**
	 * 新增请求日志
	 *
	 * @param updateParam 请求日志 新增对象
	 * @return 结果
	 */
	Long insertPC(TsReqLogUpdateParam updateParam);

	/**
	 * 修改请求日志
	 *
	 * @param updateParam 请求日志 修改参数
	 * @return 结果
	 */
	int updateByIdPC(TsReqLogUpdateParam updateParam);

}
