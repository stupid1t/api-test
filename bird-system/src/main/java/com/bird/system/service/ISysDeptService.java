package com.bird.system.service;

import com.bird.system.domain.bo.TreeSelect;
import com.bird.system.domain.entity.SysDeptEntity;
import com.bird.system.domain.params.SysDeptQueryParams;
import com.bird.system.domain.params.SysDeptUpdateParams;

import java.util.List;

/**
 * 部门管理 服务层
 *
 * @author bird
 */
public interface ISysDeptService extends IBaseService<SysDeptEntity> {
	/**
	 * 查询部门管理数据
	 *
	 * @param dept 部门信息
	 * @return 部门信息集合
	 */
	public List<SysDeptEntity> selectDeptList(SysDeptQueryParams dept);

	/**
	 * 构建前端所需要树结构
	 *
	 * @param depts 部门列表
	 * @return 树结构列表
	 */
	public List<SysDeptEntity> buildDeptTree(List<SysDeptEntity> depts);

	/**
	 * 构建前端所需要下拉树结构
	 *
	 * @param depts 部门列表
	 * @return 下拉树结构列表
	 */
	public List<TreeSelect> buildDeptTreeSelect(List<SysDeptEntity> depts);

	/**
	 * 根据角色ID查询部门树信息
	 *
	 * @param roleId 角色ID
	 * @return 选中部门列表
	 */
	public List<Integer> selectDeptListByRoleId(Long roleId);

	/**
	 * 是否存在部门子节点
	 *
	 * @param deptId 部门ID
	 * @return 结果
	 */
	public boolean hasChildByDeptId(Long deptId);

	/**
	 * 查询部门是否存在用户
	 *
	 * @param deptId 部门ID
	 * @return 结果 true 存在 false 不存在
	 */
	public boolean checkDeptExistUser(Long deptId);

	/**
	 * 校验部门名称是否唯一
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	public String checkDeptNameUnique(SysDeptUpdateParams dept);

	/**
	 * 新增保存部门信息
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	public int insertDept(SysDeptEntity dept);

	/**
	 * 修改保存部门信息
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	public int updateDept(SysDeptEntity dept);

}
