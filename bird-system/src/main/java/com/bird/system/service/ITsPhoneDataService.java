package com.bird.system.service;

import com.bird.system.domain.entity.TsPhoneDataEntity;
import com.bird.system.domain.params.TsPhoneDataQueryParam;
import com.bird.system.domain.params.TsPhoneDataUpdateParam;
import com.bird.system.domain.vo.TsPhoneDataListPcVO;

import java.util.List;

/**
 * 数据Service接口
 *
 * @author bird
 * @date 2020-09-23
 */
public interface ITsPhoneDataService extends IBaseService<TsPhoneDataEntity> {

	/**
	 * 查询数据列表
	 *
	 * @param queryParam 数据 查询对象
	 * @return 数据集合
	 */
	List<TsPhoneDataListPcVO> selectListPC(TsPhoneDataQueryParam queryParam);


	/**
	 * 新增数据
	 *
	 * @param updateParam 数据 新增对象
	 * @return 结果
	 */
	Long insertPC(TsPhoneDataUpdateParam updateParam, Long userId);

	/**
	 * 修改数据
	 *
	 * @param updateParam 数据 修改参数
	 * @return 结果
	 */
	int updateByIdPC(TsPhoneDataUpdateParam updateParam);

	/**
	 * 批量插入
	 *
	 * @param list
	 */
	void insertBatch(List<TsPhoneDataEntity> list);

}
