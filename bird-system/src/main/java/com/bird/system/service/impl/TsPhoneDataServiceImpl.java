package com.bird.system.service.impl;

import com.bird.system.domain.entity.TsPhoneDataEntity;
import com.bird.system.domain.params.TsPhoneDataQueryParam;
import com.bird.system.domain.params.TsPhoneDataUpdateParam;
import com.bird.system.domain.vo.TsPhoneDataListPcVO;
import com.bird.system.mapper.TsPhoneDataMapper;
import com.bird.system.service.ITsPhoneDataService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * 数据Service业务层处理
 *
 * @author bird
 * @date 2020-09-23
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TsPhoneDataServiceImpl extends IBaseServiceImpl<TsPhoneDataEntity, TsPhoneDataMapper> implements ITsPhoneDataService {

	/**
	 * 查询数据列表
	 *
	 * @param queryParam 数据 查询参数
	 * @return 数据
	 */
	@Override
	public List<TsPhoneDataListPcVO> selectListPC(TsPhoneDataQueryParam queryParam) {
		List<TsPhoneDataListPcVO> tsPhoneDatas = mapper.selectListPC(queryParam);
		return tsPhoneDatas;
	}

	/**
	 * 新增数据
	 *
	 * @param updateParam 数据 新增参数
	 * @return 结果
	 */
	@Override
	public Long insertPC(TsPhoneDataUpdateParam updateParam, Long userId) {
		TsPhoneDataEntity entity = updateParam.covert();
		entity.setCreSb(userId);
		this.insert(entity);
		return entity.getId();
	}

	/**
	 * 修改数据
	 *
	 * @param updateParam 数据 修改参数
	 * @return 结果
	 */
	@Override
	public int updateByIdPC(TsPhoneDataUpdateParam updateParam) {
		TsPhoneDataEntity entity = updateParam.covert();
		int result = this.updateById(entity);
		return result;
	}

	@Override
	public void insertBatch(List<TsPhoneDataEntity> list) {
		this.mapper.insertBatch(list);
	}

}
