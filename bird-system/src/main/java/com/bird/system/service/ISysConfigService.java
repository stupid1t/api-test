package com.bird.system.service;

import com.bird.system.domain.entity.SysConfigEntity;
import com.bird.system.domain.params.SysConfigQueryParams;
import com.bird.system.domain.params.SysConfigUpdateParams;

import java.util.List;

/**
 * 参数配置 服务层
 *
 * @author bird
 */
public interface ISysConfigService extends IBaseService<SysConfigEntity> {

	/**
	 * 校验参数键名是否唯一
	 *
	 * @param config 参数信息
	 * @return 结果
	 */
	public String checkConfigKeyUnique(SysConfigUpdateParams config);

	/**
	 * 查询配置列表
	 *
	 * @param config
	 * @return
	 */
	List<SysConfigEntity> selectConfigList(SysConfigQueryParams config);

	/**
	 * 根据Key获取值
	 *
	 * @param configKey
	 * @return
	 */
	String selectConfigByKey(String configKey);
}
