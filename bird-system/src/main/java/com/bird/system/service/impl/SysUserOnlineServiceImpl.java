package com.bird.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.utils.StringUtil;
import com.bird.system.domain.bo.LoginUser;
import com.bird.system.domain.entity.SysUserEntity;
import com.bird.system.domain.entity.SysUserOnlineEntity;
import com.bird.system.service.ISysUserOnlineService;
import org.springframework.stereotype.Service;

/**
 * 在线用户 服务层处理
 *
 * @author bird
 */
@Service
public class SysUserOnlineServiceImpl implements ISysUserOnlineService {
	/**
	 * 通过登录地址查询信息
	 *
	 * @param ipaddr 登录地址
	 * @param user   用户信息
	 * @return 在线用户信息
	 */
	@Override
	public SysUserOnlineEntity selectOnlineByIpaddr(String ipaddr, LoginUser user) {
		if (StringUtil.equals(ipaddr, user.getIpaddr())) {
			return loginUserToUserOnline(user);
		}
		return null;
	}

	/**
	 * 通过用户名称查询信息
	 *
	 * @param userName 用户名称
	 * @param user     用户信息
	 * @return 在线用户信息
	 */
	@Override
	public SysUserOnlineEntity selectOnlineByUserName(String userName, LoginUser user) {
		if (StringUtil.equals(userName, user.getUsername())) {
			return loginUserToUserOnline(user);
		}
		return null;
	}

	/**
	 * 通过登录地址/用户名称查询信息
	 *
	 * @param ipaddr   登录地址
	 * @param userName 用户名称
	 * @param user     用户信息
	 * @return 在线用户信息
	 */
	@Override
	public SysUserOnlineEntity selectOnlineByInfo(String ipaddr, String userName, LoginUser user) {
		if (StringUtil.equals(ipaddr, user.getIpaddr()) && StringUtil.equals(userName, user.getUsername())) {
			return loginUserToUserOnline(user);
		}
		return null;
	}

	/**
	 * 设置在线用户信息
	 *
	 * @param user 用户信息
	 * @return 在线用户
	 */
	@Override
	public SysUserOnlineEntity loginUserToUserOnline(LoginUser user) {
		if (ObjectUtil.isNull(user) && ObjectUtil.isNull(user.getUser())) {
			return null;
		}
		SysUserOnlineEntity sysUserOnline = new SysUserOnlineEntity();
		sysUserOnline.setTokenId(user.getToken());
		sysUserOnline.setUserName(user.getUsername());
		sysUserOnline.setIpaddr(user.getIpaddr());
		sysUserOnline.setLoginLocation(user.getLoginLocation());
		sysUserOnline.setBrowser(user.getBrowser());
		sysUserOnline.setOs(user.getOs());
		sysUserOnline.setLoginTime(user.getLoginTime());
		if (ObjectUtil.isNotNull(user.getUser(SysUserEntity.class).getDept())) {
			sysUserOnline.setDeptName(user.getUser(SysUserEntity.class).getDept().getDeptName());
		}
		return sysUserOnline;
	}
}
