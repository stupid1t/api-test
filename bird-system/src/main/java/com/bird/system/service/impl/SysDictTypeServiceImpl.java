package com.bird.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.constant.Constants;
import com.bird.common.utils.tkmybatis.TK;
import com.bird.system.domain.entity.SysDictDataEntity;
import com.bird.system.domain.entity.SysDictTypeEntity;
import com.bird.system.domain.params.SysDictTypeQueryParams;
import com.bird.system.domain.params.SysDictTypeUpdateParams;
import com.bird.system.mapper.SysDictDataMapper;
import com.bird.system.mapper.SysDictTypeMapper;
import com.bird.system.service.ISysDictTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 字典 业务层处理
 *
 * @author bird
 */
@Service
public class SysDictTypeServiceImpl extends IBaseServiceImpl<SysDictTypeEntity, SysDictTypeMapper> implements ISysDictTypeService {

	@Autowired
	private SysDictDataMapper dictDataMapper;

	/**
	 * 根据条件分页查询字典类型
	 *
	 * @param dictType 字典类型信息
	 * @return 字典类型集合信息
	 */
	@Override
	public List<SysDictTypeEntity> selectDictTypeList(SysDictTypeQueryParams dictType) {
		return mapper.selectDictTypeList(dictType);
	}

	/**
	 * 根据所有字典类型
	 *
	 * @return 字典类型集合信息
	 */
	@Override
	public List<SysDictTypeEntity> selectDictTypeAll() {
		return mapper.selectAll();
	}

	/**
	 * 根据字典类型ID查询信息
	 *
	 * @param dictId 字典类型ID
	 * @return 字典类型
	 */
	@Override
	public SysDictTypeEntity selectDictTypeById(Long dictId) {
		return mapper.selectByPrimaryKey(dictId);
	}

	/**
	 * 根据字典类型查询信息
	 *
	 * @param dictType 字典类型
	 * @return 字典类型
	 */
	@Override
	public SysDictTypeEntity selectDictTypeByType(String dictType) {
		return mapper.selectOneByExample(SelectWhere().andEqualTo(SysDictTypeEntity::getDictType, dictType).end());
	}

	/**
	 * 通过字典ID删除字典信息
	 *
	 * @param dictId 字典ID
	 * @return 结果
	 */
	@Override
	public int deleteDictTypeById(Long dictId) {
		return mapper.deleteByPrimaryKey(dictId);
	}

	/**
	 * 批量删除字典类型信息
	 *
	 * @param dictIds 需要删除的字典ID
	 * @return 结果
	 */
	@Override
	public int deleteDictTypeByIds(Long[] dictIds) {
		return this.deleteByIds(dictIds);
	}

	/**
	 * 新增保存字典类型信息
	 *
	 * @param dictType 字典类型信息
	 * @return 结果
	 */
	@Override
	public int insertDictType(SysDictTypeEntity dictType) {
		return mapper.insertSelective(dictType);
	}

	/**
	 * 修改保存字典类型信息
	 *
	 * @param dictType 字典类型信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateDictType(SysDictTypeEntity dictType) {
		SysDictTypeEntity oldDict = mapper.selectByPrimaryKey(dictType.getId());
		dictDataMapper.updateByExampleSelective(
				SysDictDataEntity.builder().dictType(dictType.getDictType()).build(),
				TK.select(SysDictDataEntity.class).where().andEqualTo(SysDictDataEntity::getDictType, oldDict.getDictType()).end()
		);
		return mapper.updateByPrimaryKeySelective(dictType);
	}

	/**
	 * 校验字典类型称是否唯一
	 *
	 * @param dict 字典类型
	 * @return 结果
	 */
	@Override
	public String checkDictTypeUnique(SysDictTypeUpdateParams dict) {
		Long dictId = ObjectUtil.isNull(dict.getId()) ? -1L : dict.getId();
		SysDictTypeEntity dictType = this.selectDictTypeByType(dict.getDictType());
		if (ObjectUtil.isNotNull(dictType) && dictType.getId().longValue() != dictId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}
}
