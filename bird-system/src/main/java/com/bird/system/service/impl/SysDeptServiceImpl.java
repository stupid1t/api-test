package com.bird.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.annotation.DataScope;
import com.bird.common.constant.Constants;
import com.bird.common.exception.CustomException;
import com.bird.common.utils.Assert;
import com.bird.system.domain.bo.TreeSelect;
import com.bird.system.domain.entity.SysDeptEntity;
import com.bird.system.domain.params.SysDeptQueryParams;
import com.bird.system.domain.params.SysDeptUpdateParams;
import com.bird.system.mapper.SysDeptMapper;
import com.bird.system.service.ISysDeptService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 部门管理 服务实现
 *
 * @author ruoyi
 */
@Service
public class SysDeptServiceImpl extends IBaseServiceImpl<SysDeptEntity, SysDeptMapper> implements ISysDeptService {

	/**
	 * 查询部门管理数据
	 *
	 * @param dept 部门信息
	 * @return 部门信息集合
	 */
	@Override
	@DataScope(deptAlias = "d")
	public List<SysDeptEntity> selectDeptList(SysDeptQueryParams dept) {
		return mapper.selectDeptList(dept);
	}

	/**
	 * 构建前端所需要树结构
	 *
	 * @param depts 部门列表
	 * @return 树结构列表
	 */
	@Override
	public List<SysDeptEntity> buildDeptTree(List<SysDeptEntity> depts) {
		List<SysDeptEntity> returnList = new ArrayList<SysDeptEntity>();
		List<Long> tempList = new ArrayList<Long>();
		for (SysDeptEntity dept : depts) {
			tempList.add(dept.getId());
		}
		for (Iterator<SysDeptEntity> iterator = depts.iterator(); iterator.hasNext(); ) {
			SysDeptEntity dept = (SysDeptEntity) iterator.next();
			// 如果是顶级节点, 遍历该父节点的所有子节点
			if (!tempList.contains(dept.getParentId())) {
				recursionFn(depts, dept);
				returnList.add(dept);
			}
		}
		if (returnList.isEmpty()) {
			returnList = depts;
		}
		return returnList;
	}

	/**
	 * 构建前端所需要下拉树结构
	 *
	 * @param depts 部门列表
	 * @return 下拉树结构列表
	 */
	@Override
	public List<TreeSelect> buildDeptTreeSelect(List<SysDeptEntity> depts) {
		List<SysDeptEntity> deptTrees = buildDeptTree(depts);
		return deptTrees.stream().map(TreeSelect::new).collect(Collectors.toList());
	}

	/**
	 * 根据角色ID查询部门树信息
	 *
	 * @param roleId 角色ID
	 * @return 选中部门列表
	 */
	@Override
	public List<Integer> selectDeptListByRoleId(Long roleId) {
		return mapper.selectDeptListByRoleId(roleId);
	}


	/**
	 * 是否存在子节点
	 *
	 * @param deptId 部门ID
	 * @return 结果
	 */
	@Override
	public boolean hasChildByDeptId(Long deptId) {
		int result = mapper.selectCountByExample(SelectWhere().andEqualTo(SysDeptEntity::getParentId, deptId).end());
		return result > 0 ? true : false;
	}

	/**
	 * 查询部门是否存在用户
	 *
	 * @param deptId 部门ID
	 * @return 结果 true 存在 false 不存在
	 */
	@Override
	public boolean checkDeptExistUser(Long deptId) {
		int result = mapper.checkDeptExistUser(deptId);
		return result > 0 ? true : false;
	}

	/**
	 * 校验部门名称是否唯一
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public String checkDeptNameUnique(SysDeptUpdateParams dept) {
		Long deptId = ObjectUtil.isNull(dept.getId()) ? -1L : dept.getId();
		SysDeptEntity info = mapper.selectOneByExample(SelectWhere()
				.andEqualTo(SysDeptEntity::getParentId, dept.getParentId())
				.andEqualTo(SysDeptEntity::getDeptName, dept.getDeptName())
				.end()
		);
		if (ObjectUtil.isNotNull(info) && info.getId().longValue() != deptId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	/**
	 * 新增保存部门信息
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public int insertDept(SysDeptEntity dept) {
		SysDeptEntity info = mapper.selectByPrimaryKey(dept.getParentId());
		// 如果父节点不为正常状态,则不允许新增子节点
		Assert.isFalse(Constants.User.DEPT_NORMAL.equals(info.getStatus()), "部门停用，不允许新增");
		if (!Constants.User.DEPT_NORMAL.equals(info.getStatus())) {
			throw new CustomException("部门停用，不允许新增");
		}
		dept.setAncestors(info.getAncestors() + "," + dept.getParentId());
		return mapper.insertSelective(dept);
	}

	/**
	 * 修改保存部门信息
	 *
	 * @param dept 部门信息
	 * @return 结果
	 */
	@Override
	public int updateDept(SysDeptEntity dept) {
		SysDeptEntity newParentDept = mapper.selectByPrimaryKey(dept.getParentId());
		SysDeptEntity oldDept = mapper.selectByPrimaryKey(dept.getId());
		if (ObjectUtil.isNotNull(newParentDept) && ObjectUtil.isNotNull(oldDept)) {
			String newAncestors = newParentDept.getAncestors() + "," + newParentDept.getId();
			String oldAncestors = oldDept.getAncestors();
			dept.setAncestors(newAncestors);
			updateDeptChildren(dept.getId(), newAncestors, oldAncestors);
		}
		int result = mapper.updateByPrimaryKeySelective(dept);
		if (Constants.User.DEPT_NORMAL.equals(dept.getStatus())) {
			// 如果该部门是启用状态，则启用该部门的所有上级部门
			updateParentDeptStatus(dept);
		}
		return result;
	}

	/**
	 * 修改该部门的父级部门状态
	 *
	 * @param dept 当前部门
	 */
	private void updateParentDeptStatus(SysDeptEntity dept) {
		SysDeptEntity deptUpdate = new SysDeptEntity();
		deptUpdate.setUpdateBy(dept.getUpdateBy());
		deptUpdate.setStatus(dept.getStatus());
		updateByParams(deptUpdate, SelectWhere().andIn(SysDeptEntity::getId, dept.getAncestors().split(",")).end());
	}

	/**
	 * 修改子元素关系
	 *
	 * @param deptId       被修改的部门ID
	 * @param newAncestors 新的父ID集合
	 * @param oldAncestors 旧的父ID集合
	 */
	public void updateDeptChildren(Long deptId, String newAncestors, String oldAncestors) {
		List<SysDeptEntity> children = mapper.selectChildrenDeptById(deptId);
		for (SysDeptEntity child : children) {
			child.setAncestors(child.getAncestors().replace(oldAncestors, newAncestors));
		}
		if (children.size() > 0) {
			mapper.updateDeptChildren(children);
		}
	}

	/**
	 * 递归列表
	 */
	private void recursionFn(List<SysDeptEntity> list, SysDeptEntity t) {
		// 得到子节点列表
		List<SysDeptEntity> childList = getChildList(list, t);
		t.setChildren(childList);
		for (SysDeptEntity tChild : childList) {
			if (hasChild(list, tChild)) {
				// 判断是否有子节点
				Iterator<SysDeptEntity> it = childList.iterator();
				while (it.hasNext()) {
					SysDeptEntity n = (SysDeptEntity) it.next();
					recursionFn(list, n);
				}
			}
		}
	}

	/**
	 * 得到子节点列表
	 */
	private List<SysDeptEntity> getChildList(List<SysDeptEntity> list, SysDeptEntity t) {
		List<SysDeptEntity> tlist = new ArrayList<SysDeptEntity>();
		Iterator<SysDeptEntity> it = list.iterator();
		while (it.hasNext()) {
			SysDeptEntity n = (SysDeptEntity) it.next();
			if (ObjectUtil.isNotNull(n.getParentId()) && n.getParentId().longValue() == t.getId().longValue()) {
				tlist.add(n);
			}
		}
		return tlist;
	}

	/**
	 * 判断是否有子节点
	 */
	private boolean hasChild(List<SysDeptEntity> list, SysDeptEntity t) {
		return getChildList(list, t).size() > 0 ? true : false;
	}
}
