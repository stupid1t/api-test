package com.bird.system.service.impl;

import com.bird.system.domain.entity.TsReqLogEntity;
import com.bird.system.domain.params.TsReqLogQueryParam;
import com.bird.system.domain.params.TsReqLogUpdateParam;
import com.bird.system.domain.vo.TsReqLogListPcVO;
import com.bird.system.mapper.TsReqLogMapper;
import com.bird.system.service.ITsReqLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 请求日志Service业务层处理
 *
 * @author bird
 * @date 2020-09-23
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class TsReqLogServiceImpl extends IBaseServiceImpl<TsReqLogEntity, TsReqLogMapper> implements ITsReqLogService {

	/**
	 * 查询请求日志列表
	 *
	 * @param queryParam 请求日志 查询参数
	 * @return 请求日志
	 */
	@Override
	public List<TsReqLogListPcVO> selectListPC(TsReqLogQueryParam queryParam) {
		List<TsReqLogListPcVO> tsReqLogs = mapper.selectListPC(queryParam);
		return tsReqLogs;
	}

	/**
	 * 新增请求日志
	 *
	 * @param updateParam 请求日志 新增参数
	 * @return 结果
	 */
	@Override
	public Long insertPC(TsReqLogUpdateParam updateParam) {
		TsReqLogEntity entity = updateParam.covert();
		this.insert(entity);
		return entity.getId();
	}

	/**
	 * 修改请求日志
	 *
	 * @param updateParam 请求日志 修改参数
	 * @return 结果
	 */
	@Override
	public int updateByIdPC(TsReqLogUpdateParam updateParam) {
		TsReqLogEntity entity = updateParam.covert();
		int result = this.updateById(entity);
		return result;
	}

}
