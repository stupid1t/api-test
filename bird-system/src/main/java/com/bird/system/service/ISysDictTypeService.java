package com.bird.system.service;

import com.bird.system.domain.entity.SysDictTypeEntity;
import com.bird.system.domain.params.SysDictTypeQueryParams;
import com.bird.system.domain.params.SysDictTypeUpdateParams;

import java.util.List;

/**
 * 字典 业务层
 *
 * @author bird
 */
public interface ISysDictTypeService extends IBaseService<SysDictTypeEntity> {
	/**
	 * 根据条件分页查询字典类型
	 *
	 * @param dictType 字典类型信息
	 * @return 字典类型集合信息
	 */
	public List<SysDictTypeEntity> selectDictTypeList(SysDictTypeQueryParams dictType);

	/**
	 * 根据所有字典类型
	 *
	 * @return 字典类型集合信息
	 */
	public List<SysDictTypeEntity> selectDictTypeAll();

	/**
	 * 根据字典类型ID查询信息
	 *
	 * @param dictId 字典类型ID
	 * @return 字典类型
	 */
	public SysDictTypeEntity selectDictTypeById(Long dictId);

	/**
	 * 根据字典类型查询信息
	 *
	 * @param dictType 字典类型
	 * @return 字典类型
	 */
	public SysDictTypeEntity selectDictTypeByType(String dictType);

	/**
	 * 通过字典ID删除字典信息
	 *
	 * @param dictId 字典ID
	 * @return 结果
	 */
	public int deleteDictTypeById(Long dictId);

	/**
	 * 批量删除字典信息
	 *
	 * @param dictIds 需要删除的字典ID
	 * @return 结果
	 */
	public int deleteDictTypeByIds(Long[] dictIds);

	/**
	 * 新增保存字典类型信息
	 *
	 * @param dictType 字典类型信息
	 * @return 结果
	 */
	public int insertDictType(SysDictTypeEntity dictType);

	/**
	 * 修改保存字典类型信息
	 *
	 * @param dictType 字典类型信息
	 * @return 结果
	 */
	public int updateDictType(SysDictTypeEntity dictType);

	/**
	 * 校验字典类型称是否唯一
	 *
	 * @param dictType 字典类型
	 * @return 结果
	 */
	public String checkDictTypeUnique(SysDictTypeUpdateParams dictType);
}
