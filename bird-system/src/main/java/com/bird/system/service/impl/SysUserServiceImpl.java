package com.bird.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.annotation.DataScope;
import com.bird.common.constant.Constants;
import com.bird.common.exception.CustomException;
import com.bird.common.utils.StringUtil;
import com.bird.system.domain.entity.SysPostEntity;
import com.bird.system.domain.entity.SysRoleEntity;
import com.bird.system.domain.entity.SysUserEntity;
import com.bird.system.domain.entity.SysUserPostEntity;
import com.bird.system.domain.entity.SysUserRoleEntity;
import com.bird.system.domain.params.SysUserQueryParams;
import com.bird.system.domain.params.SysUserUpdateParams;
import com.bird.system.mapper.SysPostMapper;
import com.bird.system.mapper.SysRoleMapper;
import com.bird.system.mapper.SysUserMapper;
import com.bird.system.mapper.SysUserPostMapper;
import com.bird.system.mapper.SysUserRoleMapper;
import com.bird.system.service.ISysUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户 业务层处理
 *
 * @author bird
 */
@Service
public class SysUserServiceImpl extends IBaseServiceImpl<SysUserEntity, SysUserMapper> implements ISysUserService {

	@Resource
	private SysRoleMapper roleMapper;

	@Resource
	private SysPostMapper postMapper;

	@Resource
	private SysUserRoleMapper userRoleMapper;

	@Resource
	private SysUserPostMapper userPostMapper;

	/**
	 * 根据条件分页查询用户列表
	 *
	 * @param user 用户信息
	 * @return 用户信息集合信息
	 */
	@Override
	@DataScope(deptAlias = "d", userAlias = "u")
	public List<SysUserEntity> selectUserList(SysUserQueryParams user) {
		return mapper.selectUserList(user);
	}

	/**
	 * 通过用户名查询用户
	 *
	 * @param userName 用户名
	 * @return 用户对象信息
	 */
	@Override
	public SysUserEntity selectUserByUserName(String userName) {
		return mapper.selectUserByUserName(userName);
	}

	/**
	 * 通过用户ID查询用户
	 *
	 * @param userId 用户ID
	 * @return 用户对象信息
	 */
	@Override
	public SysUserEntity selectUserById(Long userId) {
		return mapper.selectUserById(userId);
	}

	@Override
	public String selectUserNameById(Long userId) {
		SysUserEntity sysUserEntity = mapper.selectUserById(userId);
		return sysUserEntity == null ? null : sysUserEntity.getUserName();
	}

	/**
	 * 查询用户所属角色组
	 *
	 * @param userName 用户名
	 * @return 结果
	 */
	@Override
	public String selectUserRoleGroup(String userName) {
		List<SysRoleEntity> list = roleMapper.selectRolesByUserName(userName);
		StringBuffer idsStr = new StringBuffer();
		for (SysRoleEntity role : list) {
			idsStr.append(role.getRoleName()).append(",");
		}
		if (StringUtil.isNotEmpty(idsStr.toString())) {
			return idsStr.substring(0, idsStr.length() - 1);
		}
		return idsStr.toString();
	}

	/**
	 * 查询用户所属岗位组
	 *
	 * @param userName 用户名
	 * @return 结果
	 */
	@Override
	public String selectUserPostGroup(String userName) {
		List<SysPostEntity> list = postMapper.selectPostsByUserName(userName);
		StringBuffer idsStr = new StringBuffer();
		for (SysPostEntity post : list) {
			idsStr.append(post.getPostName()).append(",");
		}
		if (StringUtil.isNotEmpty(idsStr.toString())) {
			return idsStr.substring(0, idsStr.length() - 1);
		}
		return idsStr.toString();
	}

	/**
	 * 校验用户名称是否唯一
	 *
	 * @param userName 用户名称
	 * @return 结果
	 */
	@Override
	public String checkUserNameUnique(String userName) {
		int count = this.countByParams(SelectWhere().andEqualTo(SysUserEntity::getUserName, userName).end());
		if (count > 0) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	/**
	 * 校验用户名称是否唯一
	 *
	 * @param user 用户信息
	 * @return
	 */
	@Override
	public String checkPhoneUnique(SysUserUpdateParams user) {
		Long userId = ObjectUtil.isNull(user.getId()) ? -1L : user.getId();
		SysUserEntity info = this.selectOne(SelectWhere().andEqualTo(SysUserEntity::getPhonenumber, user.getPhonenumber()).end());
		if (ObjectUtil.isNotNull(info) && info.getId().longValue() != userId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	/**
	 * 校验email是否唯一
	 *
	 * @param user 用户信息
	 * @return
	 */
	@Override
	public String checkEmailUnique(SysUserUpdateParams user) {
		Long userId = ObjectUtil.isNull(user.getId()) ? -1L : user.getId();
		SysUserEntity info = this.selectOne(SelectWhere().andEqualTo(SysUserEntity::getEmail, user.getEmail()).end());
		if (ObjectUtil.isNotNull(info) && info.getId().longValue() != userId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	/**
	 * 校验用户是否允许操作
	 *
	 * @param user 用户信息
	 */
	@Override
	public void checkUserAllowed(SysUserUpdateParams user) {
		if (ObjectUtil.isNotNull(user.getId()) && user.isweb()) {
			throw new CustomException("不允许操作超级管理员用户");
		}
	}

	/**
	 * 新增保存用户信息
	 *
	 * @param user 用户信息
	 * @return 结果
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int insertUser(SysUserEntity user) {
		// 新增用户信息
		int rows = this.insert(user).intValue();
		// 新增用户岗位关联
		insertUserPost(user);
		// 新增用户与角色管理
		insertUserRole(user);
		return rows;
	}

	/**
	 * 修改保存用户信息
	 *
	 * @param user 用户信息
	 * @return 结果
	 */
	@Override
	@Transactional(rollbackFor = Exception.class)
	public int updateUser(SysUserEntity user) {
		Long userId = user.getId();
		// 删除用户与角色关联
		userRoleMapper.deleteUserRoleByUserId(userId);
		// 新增用户与角色管理
		insertUserRole(user);
		// 删除用户与岗位关联
		userPostMapper.deleteUserPostByUserId(userId);
		// 新增用户与岗位管理
		insertUserPost(user);
		return this.updateById(user);
	}

	/**
	 * 修改用户状态
	 *
	 * @param user 用户信息
	 * @return 结果
	 */
	@Override
	public int updateUserStatus(SysUserEntity user) {
		return this.updateById(user);
	}

	/**
	 * 修改用户基本信息
	 *
	 * @param user 用户信息
	 * @return 结果
	 */
	@Override
	public int updateUserProfile(SysUserEntity user) {
		return this.updateById(user);
	}

	/**
	 * 修改用户头像
	 *
	 * @param userName 用户ID
	 * @param avatar   头像地址
	 * @return 结果
	 */
	@Override
	public boolean updateUserAvatar(String userName, String avatar) {
		int row = this.updateByParams(
				SysUserEntity.builder().avatar(avatar).build(),
				SelectWhere().andEqualTo(SysUserEntity::getUserName, userName).end()
		);
		return row > 0;
	}

	/**
	 * 重置用户密码
	 *
	 * @param user 用户信息
	 * @return 结果
	 */
	@Override
	public int resetPwd(SysUserEntity user) {
		return this.updateById(user);
	}

	/**
	 * 重置用户密码
	 *
	 * @param userName 用户名
	 * @param password 密码
	 * @return 结果
	 */
	@Override
	public int resetUserPwd(String userName, String password) {
		int row = this.updateByParams(
				SysUserEntity.builder().password(password).build(),
				SelectWhere().andEqualTo(SysUserEntity::getUserName, userName).end()
		);
		return row;
	}

	/**
	 * 新增用户角色信息
	 *
	 * @param user 用户对象
	 */
	public void insertUserRole(SysUserEntity user) {
		Long[] roles = user.getRoleIds();
		if (ObjectUtil.isNotNull(roles)) {
			// 新增用户与角色管理
			List<SysUserRoleEntity> list = new ArrayList<SysUserRoleEntity>();
			for (Long roleId : roles) {
				SysUserRoleEntity ur = new SysUserRoleEntity();
				ur.setUserId(user.getId());
				ur.setRoleId(roleId);
				list.add(ur);
			}
			if (list.size() > 0) {
				userRoleMapper.batchUserRole(list);
			}
		}
	}

	/**
	 * 新增用户岗位信息
	 *
	 * @param user 用户对象
	 */
	public void insertUserPost(SysUserEntity user) {
		Long[] posts = user.getPostIds();
		if (ObjectUtil.isNotNull(posts)) {
			// 新增用户与岗位管理
			List<SysUserPostEntity> list = new ArrayList<SysUserPostEntity>();
			for (Long postId : posts) {
				SysUserPostEntity up = new SysUserPostEntity();
				up.setUserId(user.getId());
				up.setPostId(postId);
				list.add(up);
			}
			if (list.size() > 0) {
				userPostMapper.batchUserPost(list);
			}
		}
	}

	/**
	 * 通过用户ID删除用户
	 *
	 * @param userId 用户ID
	 * @return 结果
	 */
	@Override
	public int deleteUserById(Long userId) {
		// 删除用户与角色关联
		userRoleMapper.deleteUserRoleByUserId(userId);
		// 删除用户与岗位表
		userPostMapper.deleteUserPostByUserId(userId);
		return this.deleteById(userId);
	}

	/**
	 * 批量删除用户信息
	 *
	 * @param userIds 需要删除的用户ID
	 * @return 结果
	 */
	@Override
	public int deleteUserByIds(Long[] userIds) {
		for (Long userId : userIds) {
			checkUserAllowed(new SysUserUpdateParams(userId));
		}
		return this.deleteByIds(userIds);
	}
}
