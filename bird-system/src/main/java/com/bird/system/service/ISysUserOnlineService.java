package com.bird.system.service;


import com.bird.system.domain.bo.LoginUser;
import com.bird.system.domain.entity.SysUserOnlineEntity;

/**
 * 在线用户 服务层
 *
 * @author bird
 */
public interface ISysUserOnlineService {
	/**
	 * 通过登录地址查询信息
	 *
	 * @param ipaddr 登录地址
	 * @param user   用户信息
	 * @return 在线用户信息
	 */
	public SysUserOnlineEntity selectOnlineByIpaddr(String ipaddr, LoginUser user);

	/**
	 * 通过用户名称查询信息
	 *
	 * @param userName 用户名称
	 * @param user     用户信息
	 * @return 在线用户信息
	 */
	public SysUserOnlineEntity selectOnlineByUserName(String userName, LoginUser user);

	/**
	 * 通过登录地址/用户名称查询信息
	 *
	 * @param ipaddr   登录地址
	 * @param userName 用户名称
	 * @param user     用户信息
	 * @return 在线用户信息
	 */
	public SysUserOnlineEntity selectOnlineByInfo(String ipaddr, String userName, LoginUser user);

	/**
	 * 设置在线用户信息
	 *
	 * @param user 用户信息
	 * @return 在线用户
	 */
	public SysUserOnlineEntity loginUserToUserOnline(LoginUser user);
}
