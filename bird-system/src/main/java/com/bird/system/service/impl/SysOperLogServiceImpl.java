package com.bird.system.service.impl;

import com.bird.system.domain.entity.SysOperLogEntity;
import com.bird.system.domain.params.SysOperLogQueryParams;
import com.bird.system.mapper.SysOperLogMapper;
import com.bird.system.service.ISysOperLogService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 操作日志 服务层处理
 *
 * @author bird
 */
@Service
public class SysOperLogServiceImpl extends IBaseServiceImpl<SysOperLogEntity, SysOperLogMapper> implements ISysOperLogService {

	/**
	 * 新增操作日志
	 *
	 * @param operLog 操作日志对象
	 */
	@Override
	public void insertOperlog(SysOperLogEntity operLog) {
		mapper.insertSelective(operLog);
	}

	/**
	 * 查询系统操作日志集合
	 *
	 * @param operLog 操作日志对象
	 * @return 操作日志集合
	 */
	@Override
	public List<SysOperLogEntity> selectOperLogList(SysOperLogQueryParams operLog) {
		return mapper.selectOperLogList(operLog);
	}

	/**
	 * 批量删除系统操作日志
	 *
	 * @param operIds 需要删除的操作日志ID
	 * @return 结果
	 */
	@Override
	public int deleteOperLogByIds(Long[] operIds) {
		return this.deleteByIds(operIds);
	}

	/**
	 * 查询操作日志详细
	 *
	 * @param operId 操作ID
	 * @return 操作日志对象
	 */
	@Override
	public SysOperLogEntity selectOperLogById(Long operId) {
		return mapper.selectByPrimaryKey(operId);
	}

	/**
	 * 清空操作日志
	 */
	@Override
	public void cleanOperLog() {
		mapper.cleanOperLog();
	}
}
