package com.bird.system.service;

import com.bird.common.utils.tkmybatis.TK;
import tk.mybatis.mapper.weekend.Fn;

import java.util.List;

/**
 * 基类service
 *
 * @author: 625
 * @version: 2020年07月01日 17:03
 */
public interface IBaseService<E> {

	/**
	 * 根据ID查询单个
	 *
	 * @param id 主键
	 * @return 结果
	 */
	E selectById(Long id);

	/**
	 * 查询一个字段
	 *
	 * @param queryParam 会员 查询对象
	 * @return 结果
	 */
	<R> R selectField(Fn<E, R> fn, TK<E> queryParam);

	/**
	 * 查询一个
	 *
	 * @param queryParam 会员 查询对象
	 * @return 结果
	 */
	E selectOne(TK<E> queryParam);

	/**
	 * 查询一个
	 *
	 * @param queryParam 会员 查询对象
	 * @return 结果
	 */
	E selectOne(E queryParam);

	/**
	 * 查询列表
	 *
	 * @param queryParam 条件
	 * @return 集合
	 */
	List<E> selectList(TK<E> queryParam);

	/**
	 * 查询列表
	 *
	 * @param queryParam 条件
	 * @return 集合
	 */
	List<E> selectList(E queryParam);

	/**
	 * 查询全部
	 *
	 * @return 集合
	 */
	List<E> selectAll();

	/**
	 * 总计
	 *
	 * @param queryParam 查询参数
	 * @return 数量
	 */
	int countByParams(TK<E> queryParam);

	/**
	 * 总计
	 *
	 * @param queryParam 查询参数
	 * @return 数量
	 */
	int countByParams(E queryParam);

	/**
	 * 存在
	 *
	 * @param queryParam 查询参数
	 * @return 数量
	 */
	boolean exist(TK<E> queryParam);

	/**
	 * 新增
	 *
	 * @param entity 新增对象
	 * @return 结果
	 */
	Long insert(E entity);

	/**
	 * 修改
	 *
	 * @param entity 修改参数
	 * @return 结果
	 */
	int updateById(E entity);

	/**
	 * 修改
	 *
	 * @param entity 修改参数
	 * @return 结果
	 */
	int updateByIds(E entity, Long[] ids);

	/**
	 * 修改
	 *
	 * @param entity     修改参数
	 * @param queryParam 查询对象
	 * @return 结果
	 */
	int updateByParams(E entity, TK<E> queryParam);

	/**
	 * 删除
	 *
	 * @param id ID
	 * @return 结果
	 */
	int deleteById(Long id);

	/**
	 * 删除
	 *
	 * @param queryParam 查询参数
	 * @return 结果
	 */
	int deleteByParams(TK<E> queryParam);

	/**
	 * 删除
	 *
	 * @param queryParam 查询参数
	 * @return 结果
	 */
	int deleteByParams(E queryParam);


	/**
	 * 批量删除会员
	 *
	 * @param ids 需要删除的会员ID
	 * @return 结果
	 */
	int deleteByIds(Long[] ids);
}
