package com.bird.system.service;

import com.bird.system.domain.entity.SysCodeRuleEntity;
import com.bird.system.domain.params.SysCodeRuleQueryParam;
import com.bird.system.domain.params.SysCodeRuleUpdateParam;
import com.bird.system.domain.vo.SysCodeRuleListPcVO;

import java.util.List;

/**
 * 系统编码规则配置Service接口
 *
 * @author bird
 * @date 2020-07-22
 */
public interface ISysCodeRuleService extends IBaseService<SysCodeRuleEntity> {

	/**
	 * 查询系统编码规则配置列表
	 *
	 * @param queryParam 系统编码规则配置 查询对象
	 * @return 系统编码规则配置集合
	 */
	List<SysCodeRuleListPcVO> selectListPC(SysCodeRuleQueryParam queryParam);


	/**
	 * 新增系统编码规则配置
	 *
	 * @param updateParam 系统编码规则配置 新增对象
	 * @return 结果
	 */
	Long insertPC(SysCodeRuleUpdateParam updateParam, Long loginId);

	/**
	 * 修改系统编码规则配置
	 *
	 * @param updateParam 系统编码规则配置 修改参数
	 * @return 结果
	 */
	int updateByIdPC(SysCodeRuleUpdateParam updateParam);

	/**
	 * 构建编码
	 *
	 * @param ruleCode 规则编码
	 * @return 结果
	 */
	String buildCode(String ruleCode);

}
