package com.bird.system.service;

import com.bird.system.domain.entity.SysErrorLogEntity;
import com.bird.system.domain.params.SysErrorLogQueryParam;
import com.bird.system.domain.params.SysErrorLogUpdateParam;
import com.bird.system.domain.vo.SysErrorLogListPcVO;

import java.util.List;

/**
 * 异常日志Service接口
 *
 * @author bird
 * @date 2020-07-15
 */
public interface ISysErrorLogService extends IBaseService<SysErrorLogEntity> {

	/**
	 * 查询异常日志列表
	 *
	 * @param queryParam 异常日志 查询对象
	 * @return 异常日志集合
	 */
	List<SysErrorLogListPcVO> selectListPC(SysErrorLogQueryParam queryParam);


	/**
	 * 新增异常日志
	 *
	 * @param updateParam 异常日志 新增对象
	 * @return 结果
	 */
	Long insertPC(SysErrorLogUpdateParam updateParam);

	/**
	 * 修改异常日志
	 *
	 * @param updateParam 异常日志 修改参数
	 * @return 结果
	 */
	int updateByIdPC(SysErrorLogUpdateParam updateParam);

}
