package com.bird.system.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.bird.common.annotation.DataScope;
import com.bird.common.constant.Constants;
import com.bird.common.exception.CustomException;
import com.bird.system.domain.entity.SysRoleDept;
import com.bird.system.domain.entity.SysRoleEntity;
import com.bird.system.domain.entity.SysRoleMenu;
import com.bird.system.domain.params.SysRoleQueryParams;
import com.bird.system.domain.params.SysRoleUpdateParams;
import com.bird.system.mapper.SysRoleDeptMapper;
import com.bird.system.mapper.SysRoleMapper;
import com.bird.system.mapper.SysRoleMenuMapper;
import com.bird.system.mapper.SysUserRoleMapper;
import com.bird.system.service.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 角色 业务层处理
 *
 * @author bird
 */
@Service
public class SysRoleServiceImpl extends IBaseServiceImpl<SysRoleEntity, SysRoleMapper> implements ISysRoleService {
	@Autowired
	private SysRoleMenuMapper roleMenuMapper;

	@Autowired
	private SysUserRoleMapper userRoleMapper;

	@Autowired
	private SysRoleDeptMapper roleDeptMapper;

	/**
	 * 根据条件分页查询角色数据
	 *
	 * @param role 角色信息
	 * @return 角色数据集合信息
	 */
	@Override
	@DataScope(deptAlias = "d")
	public List<SysRoleEntity> selectRoleList(SysRoleQueryParams role) {
		return mapper.selectRoleList(role);
	}

	/**
	 * 根据用户ID查询权限
	 *
	 * @param userId 用户ID
	 * @return 权限列表
	 */
	@Override
	public Set<String> selectRolePermissionByUserId(Long userId) {
		List<SysRoleEntity> perms = mapper.selectRolePermissionByUserId(userId);
		Set<String> permsSet = new HashSet<>();
		for (SysRoleEntity perm : perms) {
			if (ObjectUtil.isNotNull(perm)) {
				permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
			}
		}
		return permsSet;
	}

	/**
	 * 查询所有角色
	 *
	 * @return 角色列表
	 */
	@Override
	public List<SysRoleEntity> selectRoleAll() {
		return mapper.selectRoleAll();
	}

	/**
	 * 根据用户ID获取角色选择框列表
	 *
	 * @param userId 用户ID
	 * @return 选中角色ID列表
	 */
	@Override
	public List<Integer> selectRoleListByUserId(Long userId) {
		return mapper.selectRoleListByUserId(userId);
	}

	/**
	 * 通过角色ID查询角色
	 *
	 * @param roleId 角色ID
	 * @return 角色对象信息
	 */
	@Override
	public SysRoleEntity selectRoleById(Long roleId) {
		return mapper.selectRoleById(roleId);
	}

	/**
	 * 校验角色名称是否唯一
	 *
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	public String checkRoleNameUnique(SysRoleUpdateParams role) {
		Long roleId = ObjectUtil.isNull(role.getId()) ? -1L : role.getId();
		SysRoleEntity info = mapper.checkRoleNameUnique(role.getRoleName());
		if (ObjectUtil.isNotNull(info) && info.getId().longValue() != roleId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	/**
	 * 校验角色权限是否唯一
	 *
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	public String checkRoleKeyUnique(SysRoleUpdateParams role) {
		Long roleId = ObjectUtil.isNull(role.getId()) ? -1L : role.getId();
		SysRoleEntity info = mapper.checkRoleKeyUnique(role.getRoleKey());
		if (ObjectUtil.isNotNull(info) && info.getId().longValue() != roleId.longValue()) {
			return Constants.User.NOT_UNIQUE;
		}
		return Constants.User.UNIQUE;
	}

	/**
	 * 校验角色是否允许操作
	 *
	 * @param role 角色信息
	 */
	@Override
	public void checkRoleAllowed(SysRoleUpdateParams role) {
		if (ObjectUtil.isNotNull(role.getId()) && role.isweb()) {
			throw new CustomException("不允许操作超级管理员角色");
		}
	}

	/**
	 * 通过角色ID查询角色使用数量
	 *
	 * @param roleId 角色ID
	 * @return 结果
	 */
	@Override
	public int countUserRoleByRoleId(Long roleId) {
		return userRoleMapper.countUserRoleByRoleId(roleId);
	}

	/**
	 * 新增保存角色信息
	 *
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int insertRole(SysRoleEntity role) {
		// 新增角色信息
		this.insert(role);
		return insertRoleMenu(role);
	}

	/**
	 * 修改保存角色信息
	 *
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int updateRole(SysRoleEntity role) {
		// 修改角色信息
		this.updateById(role);
		// 删除角色与菜单关联
		roleMenuMapper.deleteRoleMenuByRoleId(role.getId());
		return insertRoleMenu(role);
	}

	/**
	 * 修改角色状态
	 *
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	public int updateRoleStatus(SysRoleEntity role) {
		return this.updateById(role);
	}

	/**
	 * 修改数据权限信息
	 *
	 * @param role 角色信息
	 * @return 结果
	 */
	@Override
	@Transactional
	public int authDataScope(SysRoleEntity role) {
		// 修改角色信息
		this.updateById(role);
		// 删除角色与部门关联
		roleDeptMapper.deleteRoleDeptByRoleId(role.getId());
		// 新增角色和部门信息（数据权限）
		return insertRoleDept(role);
	}

	/**
	 * 新增角色菜单信息
	 *
	 * @param role 角色对象
	 */
	public int insertRoleMenu(SysRoleEntity role) {
		int rows = 1;
		// 新增用户与角色管理
		List<SysRoleMenu> list = new ArrayList<SysRoleMenu>();
		for (Long menuId : role.getMenuIds()) {
			SysRoleMenu rm = new SysRoleMenu();
			rm.setRoleId(role.getId());
			rm.setMenuId(menuId);
			list.add(rm);
		}
		if (list.size() > 0) {
			rows = roleMenuMapper.batchRoleMenu(list);
		}
		return rows;
	}

	/**
	 * 新增角色部门信息(数据权限)
	 *
	 * @param role 角色对象
	 */
	public int insertRoleDept(SysRoleEntity role) {
		int rows = 1;
		// 新增角色与部门（数据权限）管理
		List<SysRoleDept> list = new ArrayList<SysRoleDept>();
		for (Long deptId : role.getDeptIds()) {
			SysRoleDept rd = new SysRoleDept();
			rd.setRoleId(role.getId());
			rd.setDeptId(deptId);
			list.add(rd);
		}
		if (list.size() > 0) {
			rows = roleDeptMapper.batchRoleDept(list);
		}
		return rows;
	}

	/**
	 * 通过角色ID删除角色
	 *
	 * @param roleId 角色ID
	 * @return 结果
	 */
	@Override
	public int deleteRoleById(Long roleId) {
		return this.deleteRoleById(roleId);
	}

	/**
	 * 批量删除角色信息
	 *
	 * @param roleIds 需要删除的角色ID
	 * @return 结果
	 */
	@Override
	public int deleteRoleByIds(Long[] roleIds) {
		for (Long roleId : roleIds) {
			checkRoleAllowed(new SysRoleUpdateParams(roleId));
			SysRoleEntity role = selectRoleById(roleId);
			if (countUserRoleByRoleId(roleId) > 0) {
				throw new CustomException(String.format("%1$s已分配,不能删除", role.getRoleName()));
			}
		}
		return this.deleteByIds(roleIds);
	}
}
