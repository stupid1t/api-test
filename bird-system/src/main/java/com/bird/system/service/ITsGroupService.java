package com.bird.system.service;

import com.bird.system.domain.entity.TsGroupEntity;
import com.bird.system.domain.params.TsGroupQueryParam;
import com.bird.system.domain.params.TsGroupUpdateParam;
import com.bird.system.domain.vo.TsGroupListPcVO;

import java.util.List;

/**
 * 组Service接口
 *
 * @author bird
 * @date 2020-09-23
 */
public interface ITsGroupService extends IBaseService<TsGroupEntity> {

	/**
	 * 查询组列表
	 *
	 * @param queryParam 组 查询对象
	 * @return 组集合
	 */
	List<TsGroupListPcVO> selectListPC(TsGroupQueryParam queryParam);


	/**
	 * 新增组
	 *
	 * @param updateParam 组 新增对象
	 * @return 结果
	 */
	Long insertPC(TsGroupUpdateParam updateParam, Long userId);

	/**
	 * 修改组
	 *
	 * @param updateParam 组 修改参数
	 * @return 结果
	 */
	int updateByIdPC(TsGroupUpdateParam updateParam);

	/**
	 * 根据ID查询名字
	 *
	 * @param id
	 * @return
	 */
	String selectNameById(Long id);

}
