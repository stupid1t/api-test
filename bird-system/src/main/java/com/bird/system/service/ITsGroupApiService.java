package com.bird.system.service;

import com.bird.system.domain.entity.TsGroupApiEntity;
import com.bird.system.domain.params.TsGroupApiQueryParam;
import com.bird.system.domain.params.TsGroupApiUpdateParam;
import com.bird.system.domain.vo.TsGroupApiListPcVO;

import java.util.List;

/**
 * 组接口Service接口
 *
 * @author bird
 * @date 2020-09-23
 */
public interface ITsGroupApiService extends IBaseService<TsGroupApiEntity> {

	/**
	 * 查询组接口列表
	 *
	 * @param queryParam 组接口 查询对象
	 * @return 组接口集合
	 */
	List<TsGroupApiListPcVO> selectListPC(TsGroupApiQueryParam queryParam);


	/**
	 * 新增组接口
	 *
	 * @param updateParam 组接口 新增对象
	 * @return 结果
	 */
	Long insertPC(TsGroupApiUpdateParam updateParam, Long userId);

	/**
	 * 修改组接口
	 *
	 * @param updateParam 组接口 修改参数
	 * @return 结果
	 */
	int updateByIdPC(TsGroupApiUpdateParam updateParam);

	/**
	 * 根据ID查询名字
	 *
	 * @param id
	 * @return
	 */
	String selectNameById(Long id);

	/**
	 * 根据ID查询
	 *
	 * @param id
	 * @return
	 */
	TsGroupApiEntity selectByIdPc(Long id);


	String urlParamCovertKeyValueData(String data);

	String keyValueDataCovertUrlParam(String data);

	String jsonDataCovertKeyValue(String data);

	String keyValueDataCovertJson(String data);

	void sourceDataCovertKeyValue(TsGroupApiEntity tsGroupApiEntity);

	void keyValueCovertSourceData(TsGroupApiEntity tsGroupApiEntity);
}
