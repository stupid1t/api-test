package com.bird.system.service.impl;

import com.bird.system.domain.entity.SysNoticeEntity;
import com.bird.system.domain.params.SysNoticeQueryParams;
import com.bird.system.mapper.SysNoticeMapper;
import com.bird.system.service.ISysNoticeService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 公告 服务层实现
 *
 * @author bird
 */
@Service
public class SysNoticeServiceImpl extends IBaseServiceImpl<SysNoticeEntity, SysNoticeMapper> implements ISysNoticeService {

	/**
	 * 查询公告信息
	 *
	 * @param noticeId 公告ID
	 * @return 公告信息
	 */
	@Override
	public SysNoticeEntity selectNoticeById(Long noticeId) {
		return mapper.selectByPrimaryKey(noticeId);
	}

	/**
	 * 查询公告列表
	 *
	 * @param notice 公告信息
	 * @return 公告集合
	 */
	@Override
	public List<SysNoticeEntity> selectNoticeList(SysNoticeQueryParams notice) {
		return mapper.selectNoticeList(notice);
	}

	/**
	 * 新增公告
	 *
	 * @param notice 公告信息
	 * @return 结果
	 */
	@Override
	public int insertNotice(SysNoticeEntity notice) {
		return mapper.insertSelective(notice);
	}

	/**
	 * 修改公告
	 *
	 * @param notice 公告信息
	 * @return 结果
	 */
	@Override
	public int updateNotice(SysNoticeEntity notice) {
		return mapper.updateByPrimaryKeySelective(notice);
	}

	/**
	 * 删除公告对象
	 *
	 * @param noticeId 公告ID
	 * @return 结果
	 */
	@Override
	public int deleteNoticeById(Long noticeId) {
		return mapper.deleteByPrimaryKey(noticeId);
	}

	/**
	 * 批量删除公告信息
	 *
	 * @param noticeIds 需要删除的公告ID
	 * @return 结果
	 */
	@Override
	public int deleteNoticeByIds(Long[] noticeIds) {
		return this.deleteByIds(noticeIds);
	}
}
