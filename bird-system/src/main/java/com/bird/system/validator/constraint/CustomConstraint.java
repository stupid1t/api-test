package com.bird.system.validator.constraint;

import com.bird.system.enums.BaseEnum;
import com.bird.system.validator.CustomEnumValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.FIELD, ElementType.METHOD, ElementType.ANNOTATION_TYPE, ElementType.PARAMETER, ElementType.TYPE_USE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CustomEnumValidator.class)
public @interface CustomConstraint {
	Class<? extends BaseEnum> value();

	String message() default "非枚举值";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};
}
