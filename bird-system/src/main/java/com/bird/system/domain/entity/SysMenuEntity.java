package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.common.constant.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 菜单权限表 sys_menu
 *
 * @author bird
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_menu")
public class SysMenuEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 菜单名称
	 */
	private String menuName;

	/**
	 * 父菜单名称
	 */
	@Transient
	private String parentName;

	/**
	 * 父菜单ID
	 */
	private Long parentId;

	/**
	 * 显示顺序
	 */
	private String orderNum;

	/**
	 * 路由地址
	 */
	private String path;

	/**
	 * 组件路径
	 */
	private String component;

	/**
	 * 是否为外链（0是 1否）
	 */
	private String isFrame;

	/**
	 * 类型（M目录 C菜单 F按钮）
	 */
	private String menuType;

	/**
	 * 菜单状态:0显示,1隐藏
	 */
	private String visible;

	/**
	 * 权限字符串
	 */
	private String perms;

	/**
	 * 菜单图标
	 */
	private String icon;

	/**
	 * 子菜单
	 */
	private List<SysMenuEntity> children = new ArrayList<SysMenuEntity>();

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间 推广
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date updateTime;

}
