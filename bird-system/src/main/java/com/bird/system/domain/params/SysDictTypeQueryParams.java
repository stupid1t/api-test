package com.bird.system.domain.params;

import com.bird.common.constant.BaseParams;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 字典类型表 sys_dict_type
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysDictTypeQueryParams extends BaseParams {

	/**
	 * 字典主键
	 */
	private Long id;

	/**
	 * 字典名称
	 */
	private String dictName;

	/**
	 * 字典类型
	 */
	private String dictType;

	/**
	 * 状态（0正常 1停用）
	 */
	private String status;

	/**
	 * 备注
	 */
	private String remark;

	private String endCreateTime;

	private String startCreateTime;
}
