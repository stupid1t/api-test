package com.bird.system.domain.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 异常日志对象 sys_error_log
 *
 * @author bird
 * @date 2020-07-15
 */
@ApiModel("异常日志-列表")
@Data
public class SysErrorLogListPcVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("请求URI")
	private String requestUri;

	@ApiModelProperty("请求方式")
	private String requestMethod;

	@ApiModelProperty("请求参数")
	private String requestParams;

	@ApiModelProperty("用户代理")
	private String userAgent;

	@ApiModelProperty("操作IP")
	private String ip;

	@ApiModelProperty("简要异常信息")
	private String errorSimpleInfo;
	
	@ApiModelProperty("创建时间")
	private Date createDate;

	@ApiModelProperty("创建人名")
	private String creatorName;

}
