package com.bird.system.domain.params;

/**
 * 微信登录对象
 *
 * @author ruoyi
 */
public class WxLoginBodyParam {
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * 用户名
	 */
	private String code;


}
