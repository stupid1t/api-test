package com.bird.system.domain.params;

import com.bird.system.enums.CodeRuleTypeEnum;
import com.bird.system.validator.constraint.CustomConstraint;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 系统编码规则配置对象 sys_code_rule
 *
 * @author bird
 * @date 2020-07-22
 */
@ApiModel(value = "系统编码规则配置-查询")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SysCodeRuleQueryParam {


	@ApiModelProperty("规则名称")
	private String ruleName;

	@ApiModelProperty("规则编码")
	private String ruleCode;

	@ApiModelProperty("编码类型(1日期+随机数;2日期+递增序列.;3递增序列;)")
	@CustomConstraint(CodeRuleTypeEnum.class)
	private String formatType;

	@ApiModelProperty("ids")
	private Long[] ids;
}
