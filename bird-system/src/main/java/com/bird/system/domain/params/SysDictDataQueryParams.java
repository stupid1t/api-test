package com.bird.system.domain.params;

import com.bird.common.constant.BaseParams;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 字典数据表 sys_dict_data
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysDictDataQueryParams extends BaseParams {

	/**
	 * 字典标签
	 */
	private String dictLabel;

	/**
	 * 字典标签值
	 */
	private String dictValue;

	/**
	 * 字典类型
	 */
	private String dictType;

	/**
	 * 字典类型，模糊查询
	 */
	private String dictTypeLike;

	/**
	 * 状态（0正常 1停用）
	 */
	private String status;

}
