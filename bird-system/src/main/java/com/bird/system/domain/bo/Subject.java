package com.bird.system.domain.bo;

/**
 * 登录对象
 *
 * @author: 625
 * @version: 2020年07月24日 13:43
 */
public interface Subject {

	Long getId();
}
