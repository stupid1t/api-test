package com.bird.system.domain.params;

import com.bird.system.enums.ReqStatusEnum;
import com.bird.system.validator.constraint.CustomConstraint;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 请求任务配置对象 ts_group_api_config
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel(value = "请求任务配置-查询")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TsGroupApiConfigQueryParam {


	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("接口ID")
	private Long apiId;

	@ApiModelProperty("任务状态(1未开始2已暂停3请求中)")
	@CustomConstraint(ReqStatusEnum.class)
	private String reqStatus;

	@ApiModelProperty("创建人")
	private Long creSb;

	@ApiModelProperty("ids")
	private Long[] ids;
}
