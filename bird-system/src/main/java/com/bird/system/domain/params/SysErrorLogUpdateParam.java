package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.SysErrorLogEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

import static com.bird.common.constant.ValidRule.Add;
import static com.bird.common.constant.ValidRule.Update;

/**
 * 异常日志对象 sys_error_log
 *
 * @author bird
 * @date 2020-07-15
 */
@ApiModel("异常日志-修改")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysErrorLogUpdateParam extends BeanCovert<SysErrorLogEntity> {

	@ApiModelProperty("主键")
	@NotNull(message = "主键不能为空", groups = {Add.class, Update.class})
	private Long id;

}
