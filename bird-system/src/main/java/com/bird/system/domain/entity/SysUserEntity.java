package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.common.constant.Constants;
import com.bird.system.domain.bo.Subject;
import com.bird.system.enums.SexEnum;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tk.mybatis.mapper.annotation.LogicDelete;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.Date;
import java.util.List;

/**
 * 用户对象 sys_user
 *
 * @author bird
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_user")
public class SysUserEntity extends BaseEntity implements Subject {
	private static final long serialVersionUID = 1L;

	/**
	 * 部门ID
	 */
	private Long deptId;

	/**
	 * 用户账号
	 */
	private String userName;

	/**
	 * 用户昵称
	 */
	private String nickName;

	/**
	 * 用户邮箱
	 */
	private String email;

	/**
	 * 手机号码
	 */
	private String phonenumber;

	/**
	 * 用户性别
	 */
	@JsonSerialize(converter = SexEnum.Covert.class)
	private String sex;

	/**
	 * 用户头像
	 */
	private String avatar;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 盐加密
	 */
	@Transient
	private String salt;

	/**
	 * 帐号状态（0正常 1停用）
	 */
	private String status;

	/**
	 * 删除标志（0代表存在 2代表删除）
	 */
	@LogicDelete
	private String delFlag;

	/**
	 * 最后登陆IP
	 */
	private String loginIp;

	/**
	 * 最后登陆时间
	 */
	private Date loginDate;

	/**
	 * 部门对象
	 */
	private SysDeptEntity dept;

	/**
	 * 角色对象
	 */
	private List<SysRoleEntity> roles;

	/**
	 * 角色组
	 */
	private Long[] roleIds;

	/**
	 * 岗位组
	 */
	private Long[] postIds;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间 推广
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date updateTime;


	public boolean isweb() {
		return isweb(getId());
	}

	public static boolean isweb(Long userId) {
		return userId != null && 1L == userId;
	}
}
