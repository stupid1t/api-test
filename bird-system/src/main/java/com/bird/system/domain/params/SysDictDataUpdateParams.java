package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.SysDictDataEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 字典数据表 sys_dict_data
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysDictDataUpdateParams extends BeanCovert<SysDictDataEntity> {

	/**
	 * 字典编码
	 */
	private Long id;

	/**
	 * 字典排序
	 */
	private Long dictSort;

	/**
	 * 字典标签
	 */
	private String dictLabel;

	/**
	 * 字典键值
	 */
	private String dictValue;

	/**
	 * 字典类型
	 */
	private String dictType;

	/**
	 * 样式属性（其他样式扩展）
	 */
	private String cssClass;

	/**
	 * 表格字典样式
	 */
	private String listClass;

	/**
	 * 是否默认（Y是 N否）
	 */
	private String isDefault;

	/**
	 * 状态（0正常 1停用）
	 */
	private String status;

	/**
	 * 备注
	 */
	private String remark;
}
