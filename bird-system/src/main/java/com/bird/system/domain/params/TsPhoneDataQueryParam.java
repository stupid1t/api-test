package com.bird.system.domain.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 数据对象 ts_phone_data
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel(value = "数据-查询")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TsPhoneDataQueryParam {

	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("数据名称")
	private String theName;

	@ApiModelProperty("ids")
	private Long[] ids;

	private Long userId;
}
