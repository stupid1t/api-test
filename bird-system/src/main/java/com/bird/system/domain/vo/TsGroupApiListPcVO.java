package com.bird.system.domain.vo;

import com.bird.common.utils.covert.GroupIdCovert;
import com.bird.system.enums.ReqContentTypeEnum;
import com.bird.system.enums.ReqHttpMethodEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 组接口对象 ts_group_api
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("组接口-列表")
@Data
public class TsGroupApiListPcVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("隶属组")
	@JsonSerialize(converter = GroupIdCovert.class)
	private Long groupId;

	@ApiModelProperty("名称")
	private String theName;

	@ApiModelProperty("地址")
	private String theUrl;

	@ApiModelProperty("请求协议类型")
	@JsonSerialize(converter = ReqContentTypeEnum.Covert.class)
	private String contentType;

	@ApiModelProperty("请求方式")
	@JsonSerialize(converter = ReqHttpMethodEnum.Covert.class)
	private String theMethod;

}
