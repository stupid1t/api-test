package com.bird.system.domain.bo;

import com.bird.system.domain.entity.TsReqLogEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.concurrent.CompletableFuture;

/**
 * 执行结果保存
 *
 * @author: 李涛
 * @version: 2020年09月26日 14:36
 */
@Data
@AllArgsConstructor
public class ExcuteResultFuture {

	/**
	 * 匹配内容
	 */
	private TsReqLogEntity tsReqLogEntity;

	/**
	 * 结果
	 */
	private CompletableFuture<String> resultFutrue;
}
