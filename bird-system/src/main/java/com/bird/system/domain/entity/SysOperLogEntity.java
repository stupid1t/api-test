package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.common.constant.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

/**
 * 操作日志记录表 oper_log
 *
 * @author bird
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_oper_log")
public class SysOperLogEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 操作模块
	 */
	private String title;

	/**
	 * 业务类型（0其它 1新增 2修改 3删除）
	 */
	private Integer businessType;

	/**
	 * 业务类型数组
	 */
	private Integer[] businessTypes;

	/**
	 * 请求方法
	 */
	private String method;

	/**
	 * 请求方式
	 */
	private String requestMethod;

	/**
	 * 操作类别（0其它 1后台用户 2手机端用户）
	 */
	private Integer operatorType;

	/**
	 * 操作人员
	 */
	private String operName;

	/**
	 * 部门名称
	 */
	private String deptName;

	/**
	 * 请求url
	 */
	private String operUrl;

	/**
	 * 操作地址
	 */
	private String operIp;

	/**
	 * 操作地点
	 */
	private String operLocation;

	/**
	 * 请求参数
	 */
	private String operParam;

	/**
	 * 返回参数
	 */
	private String jsonResult;

	/**
	 * 操作状态（0正常 1异常）
	 */
	private Integer status;

	/**
	 * 错误消息
	 */
	private String errorMsg;

	/**
	 * 操作时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date operTime;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间 推广
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date updateTime;

}
