package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.common.utils.covert.GroupIdCovert;
import com.bird.common.utils.covert.SystemUserCovert;
import com.bird.system.enums.ReqContentTypeEnum;
import com.bird.system.enums.ReqHttpMethodEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 组接口对象 ts_group_api
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("组接口-实体")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ts_group_api")
public class TsGroupApiEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("隶属组")
	@JsonSerialize(converter = GroupIdCovert.class)
	private Long groupId;

	@ApiModelProperty("名称")
	private String theName;

	@ApiModelProperty("地址")
	private String theUrl;

	@ApiModelProperty("URL参数")
	private String urlParam;

	@ApiModelProperty("Body参数")
	private String bodyParam;

	@ApiModelProperty("Header参数")
	private String headerParam;

	@ApiModelProperty("请求协议类型")
	@JsonSerialize(converter = ReqContentTypeEnum.Covert.class)
	private String contentType;

	@ApiModelProperty("请求方式")
	@JsonSerialize(converter = ReqHttpMethodEnum.Covert.class)
	private String theMethod;

	@ApiModelProperty("创建时间")
	private Date createTime;

	@ApiModelProperty("创建人")
	@JsonSerialize(converter = SystemUserCovert.class)
	private Long creSb;

}
