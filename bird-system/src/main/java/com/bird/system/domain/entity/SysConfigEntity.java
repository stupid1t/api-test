package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.common.constant.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

/**
 * 参数配置表 sys_config
 *
 * @author bird
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_config")
public class SysConfigEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 参数名称
	 */
	private String configName;

	/**
	 * 参数键名
	 */
	private String configKey;

	/**
	 * 参数键值
	 */
	private String configValue;

	/**
	 * 系统内置（Y是 N否）
	 */
	private String configType;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间 推广
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date updateTime;

}
