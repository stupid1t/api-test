package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.common.constant.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

/**
 * 通知公告表 sys_notice
 *
 * @author bird
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_notice")
public class SysNoticeEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 公告标题
	 */
	private String noticeTitle;

	/**
	 * 公告类型（1通知 2公告）
	 */
	private String noticeType;

	/**
	 * 公告内容
	 */
	private String noticeContent;

	/**
	 * 公告状态（0正常 1关闭）
	 */
	private String status;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间 推广
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date updateTime;
}
