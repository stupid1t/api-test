package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.SysDeptEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 部门表 sys_dept
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysDeptUpdateParams extends BeanCovert<SysDeptEntity> {

	/**
	 * 部门ID
	 */
	private Long id;

	/**
	 * 父部门ID
	 */
	private Long parentId;

	/**
	 * 祖级列表
	 */
	private String ancestors;

	/**
	 * 部门名称
	 */
	private String deptName;

	/**
	 * 显示顺序
	 */
	private String orderNum;

	/**
	 * 负责人
	 */
	private String leader;

	/**
	 * 联系电话
	 */
	private String phone;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 部门状态:0正常,1停用
	 */
	private String status;

	/**
	 * 删除标志（0代表存在 2代表删除）
	 */
	private String delFlag;

	/**
	 * 父部门名称
	 */
	private String parentName;

	/**
	 * 子部门
	 */
	private List<SysDeptUpdateParams> children = new ArrayList<SysDeptUpdateParams>();

}
