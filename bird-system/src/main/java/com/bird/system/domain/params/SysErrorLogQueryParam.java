package com.bird.system.domain.params;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 异常日志对象 sys_error_log
 *
 * @author bird
 * @date 2020-07-15
 */
@ApiModel(value = "异常日志-查询")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class SysErrorLogQueryParam {

	@ApiModelProperty("请求URI")
	private String requestUri;

	@ApiModelProperty("请求方式")
	private String requestMethod;

	@ApiModelProperty("操作IP")
	private String ip;

	@ApiModelProperty("异常信息")
	private String errorInfo;

	@ApiModelProperty("创建时间开始")
	private Date startCreateDate;

	@ApiModelProperty("创建时间结束")
	private Date endCreateDate;
	
	@ApiModelProperty("创建人名")
	private String creatorName;
	@ApiModelProperty("ids")
	private Long[] ids;
}
