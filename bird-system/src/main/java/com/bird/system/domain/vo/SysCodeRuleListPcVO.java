package com.bird.system.domain.vo;

import com.bird.system.enums.CodeRuleTypeEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 系统编码规则配置对象 sys_code_rule
 *
 * @author bird
 * @date 2020-07-22
 */
@ApiModel("系统编码规则配置-列表")
@Data
public class SysCodeRuleListPcVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("规则名称")
	private String ruleName;

	@ApiModelProperty("规则编码")
	private String ruleCode;

	@ApiModelProperty("编码前缀")
	private String rulePrefix;

	@ApiModelProperty("编码类型(1日期+随机数;2日期+递增序列.;3递增序列;)")
	@JsonSerialize(converter = CodeRuleTypeEnum.Covert.class)
	private String formatType;

	@ApiModelProperty("格式化格式")
	private String formatStr;

	@ApiModelProperty("当前日期")
	private String curFormatVal;

	@ApiModelProperty("序列占位长度")
	private String serialLen;

	@ApiModelProperty("当前序列值")
	private String serial;

}
