package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.TsPhoneDataEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.bird.common.constant.ValidRule.Add;
import static com.bird.common.constant.ValidRule.Update;

/**
 * 数据对象 ts_phone_data
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("数据-修改")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TsPhoneDataUpdateParam extends BeanCovert<TsPhoneDataEntity> {

	@ApiModelProperty("主键")
	@NotNull(message = "主键不能为空", groups = {Add.class, Update.class})
	private Long id;

	@ApiModelProperty("数据名称")
	@NotEmpty(message = "数据名称不能为空", groups = {Add.class, Update.class})
	@Size(max = 200, message = "数据名称长度超过{max}", groups = {Add.class, Update.class})
	private String theName;

	@ApiModelProperty("手机号")
	@Size(max = 2000, message = "手机号长度超过{max}", groups = {Add.class, Update.class})
	private String phone;

}
