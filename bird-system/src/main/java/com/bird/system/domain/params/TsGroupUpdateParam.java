package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.TsGroupEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.bird.common.constant.ValidRule.Add;
import static com.bird.common.constant.ValidRule.Update;

/**
 * 组对象 ts_group
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("组-修改")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TsGroupUpdateParam extends BeanCovert<TsGroupEntity> {

	@ApiModelProperty("主键")
	@NotNull(message = "主键不能为空", groups = {Add.class, Update.class})
	private Long id;

	@ApiModelProperty("组名称")
	@NotEmpty(message = "组名称不能为空", groups = {Add.class, Update.class})
	@Size(max = 20, message = "组名称长度超过{max}", groups = {Add.class, Update.class})
	private String theName;
    
}
