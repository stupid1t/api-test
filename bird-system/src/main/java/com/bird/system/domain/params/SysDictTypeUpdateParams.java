package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.SysDictTypeEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 字典类型表 sys_dict_type
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysDictTypeUpdateParams extends BeanCovert<SysDictTypeEntity> {

	/**
	 * 字典主键
	 */
	private Long id;

	/**
	 * 字典名称
	 */
	private String dictName;

	/**
	 * 字典类型
	 */
	private String dictType;

	/**
	 * 状态（0正常 1停用）
	 */
	private String status;

	/**
	 * 备注
	 */
	private String remark;
}
