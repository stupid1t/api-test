package com.bird.system.domain.params;

import com.bird.system.enums.ReqContentTypeEnum;
import com.bird.system.enums.ReqHttpMethodEnum;
import com.bird.system.validator.constraint.CustomConstraint;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 组接口对象 ts_group_api
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel(value = "组接口-查询")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TsGroupApiQueryParam {


	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("隶属组")
	private Long groupId;

	@ApiModelProperty("名称")
	private String theName;

	@ApiModelProperty("请求协议类型")
	@CustomConstraint(ReqContentTypeEnum.class)
	private String contentType;

	@ApiModelProperty("请求方式")
	@CustomConstraint(ReqHttpMethodEnum.class)
	private String theMethod;

	@ApiModelProperty("ids")
	private Long[] ids;

	private Long userId;
}
