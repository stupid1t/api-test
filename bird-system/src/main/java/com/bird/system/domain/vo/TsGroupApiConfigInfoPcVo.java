package com.bird.system.domain.vo;

import com.bird.common.utils.covert.ApiIdCovert;
import com.bird.common.utils.covert.GroupIdCovert;
import com.bird.common.utils.covert.SystemUserCovert;
import com.bird.system.enums.MatchTypeEnum;
import com.bird.system.enums.ProxyHostEnum;
import com.bird.system.enums.ReqStatusEnum;
import com.bird.system.enums.SysYesOrNoEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

/**
 * 请求任务配置对象 ts_group_api_config
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("请求任务配置-实体")
@Data
public class TsGroupApiConfigInfoPcVo {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("主鍵")
	private Long id;

	@ApiModelProperty("组ID")
	@JsonSerialize(converter = GroupIdCovert.class)
	private Long groupId;

	@ApiModelProperty("接口ID")
	@JsonSerialize(converter = ApiIdCovert.class)
	private Long apiId;

	@ApiModelProperty("延时请求时间")
	private Long intervalTime;

	@ApiModelProperty("超时时间")
	private Long timeOut;

	@ApiModelProperty("成功次数")
	private Long finishNum;

	@ApiModelProperty("终止次数")
	private Long stopNum;

	@ApiModelProperty("发起次数")
	private Long startNum;

	@ApiModelProperty("线程数")
	private Integer threadNum;

	@ApiModelProperty("请求数据ID")
	private String reqDataId;

	@ApiModelProperty("额外变量")
	private String extraVar;

	@ApiModelProperty("代理服务器")
	@JsonSerialize(converter = ProxyHostEnum.Covert.class)
	private String proxyHost;

	@ApiModelProperty("结果匹配模式(1全文匹配,2正则匹配,3json匹配)")
	@JsonSerialize(converter = MatchTypeEnum.Covert.class)
	private String matchType;

	@ApiModelProperty("结果匹配表达式")
	private String matchPattern;

	@ApiModelProperty("分页大小")
	private Long pageSize;

	@ApiModelProperty("当前页")
	private Long pageNum;

	@ApiModelProperty("总页数")
	private Long pageTotal;

	@ApiModelProperty("任务状态(1未开始2已暂停3请求中)")
	@JsonSerialize(converter = ReqStatusEnum.Covert.class)
	private String reqStatus;

	@ApiModelProperty("创建时间")
	private Date createTime;

	@ApiModelProperty("创建人")
	@JsonSerialize(converter = SystemUserCovert.class)
	private Long creSb;

	@ApiModelProperty("存储结果配置")
	private String storeVar;
	
	@ApiModelProperty("是否保留请求结果")
	@JsonSerialize(converter = SysYesOrNoEnum.Covert.class)
	private String isRetainResults;

	@ApiModelProperty("任务配置名称")
	private String configName;

}
