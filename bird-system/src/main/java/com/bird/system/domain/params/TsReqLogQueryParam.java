package com.bird.system.domain.params;

import com.bird.system.enums.FlagEnum;
import com.bird.system.enums.RspResultEnum;
import com.bird.system.validator.constraint.CustomConstraint;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 请求日志对象 ts_req_log
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel(value = "请求日志-查询")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TsReqLogQueryParam {


	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("请求配置ID")
	private Long configId;

	@ApiModelProperty("组名字")
	private String groupName;

	@ApiModelProperty("组数据")
	private String groupData;

	@ApiModelProperty("响应结果(1成功2失败)")
	@CustomConstraint(RspResultEnum.class)
	private String rspResult;

	@ApiModelProperty("是否匹配(0否1是)")
	@CustomConstraint(FlagEnum.class)
	private String matchPattern;

	private String lastReqId;

	@ApiModelProperty("ids")
	private Long[] ids;

	private Long userId;
}
