package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.common.constant.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tk.mybatis.mapper.annotation.LogicDelete;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 部门表 sys_dept
 *
 * @author bird
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_dept")
public class SysDeptEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;

	/**
	 * 父部门ID
	 */
	private Long parentId;

	/**
	 * 祖级列表
	 */
	private String ancestors;

	/**
	 * 部门名称
	 */
	private String deptName;

	/**
	 * 显示顺序
	 */
	private String orderNum;

	/**
	 * 负责人
	 */
	private String leader;

	/**
	 * 联系电话
	 */
	private String phone;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 部门状态:0正常,1停用
	 */
	private String status;

	/**
	 * 删除标志（0代表存在 2代表删除）
	 */
	@LogicDelete
	private String delFlag;

	/**
	 * 父部门名称
	 */
	@Transient
	private String parentName;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间 推广
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date updateTime;

	/**
	 * 子部门
	 */
	private List<SysDeptEntity> children = new ArrayList<SysDeptEntity>();

}
