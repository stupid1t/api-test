package com.bird.system.domain.params;


import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.SysLogininforEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 系统访问记录表 sys_logininfor
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysLogininforUpdateParams extends BeanCovert<SysLogininforEntity> {

	/**
	 * ID
	 */
	private Long id;

	/**
	 * 用户账号
	 */
	private String userName;

	/**
	 * 登录状态 0成功 1失败
	 */
	private String status;

	/**
	 * 登录IP地址
	 */
	private String ipaddr;

	/**
	 * 登录地点
	 */
	private String loginLocation;

	/**
	 * 浏览器类型
	 */
	private String browser;

	/**
	 * 操作系统
	 */
	private String os;

	/**
	 * 提示消息
	 */
	private String msg;

	/**
	 * 访问时间
	 */
	private Date loginTime;
}