package com.bird.system.domain.params;

import com.bird.system.enums.GroupTypeEnum;
import com.bird.system.validator.constraint.CustomConstraint;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 组对象 ts_group
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel(value = "组-查询")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class TsGroupQueryParam {
    
	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("组名称")
	private String theName;

	@ApiModelProperty("组类型(1私有2全局)")
	@CustomConstraint(GroupTypeEnum.class)
	private String theType;

	private Long userId;

	@ApiModelProperty("ids")
	private Long[] ids;
}
