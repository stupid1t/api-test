package com.bird.system.domain.vo;

import com.bird.common.utils.covert.ConfigIdCovert;
import com.bird.system.enums.FlagEnum;
import com.bird.system.enums.RspResultEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 请求日志对象 ts_req_log
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("请求日志-列表")
@Data
public class TsReqLogListPcVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("主键")
	private Long id;

	@JsonSerialize(converter = ConfigIdCovert.class)
	@ApiModelProperty("请求配置ID")
	private Long configId;

	@ApiModelProperty("请求配置名字")
	private String configName;

	@ApiModelProperty("组名字")
	private String groupName;

	@ApiModelProperty("组数据")
	private String groupData;

	@ApiModelProperty("开始时间")
	private Date startTime;

	@ApiModelProperty("结束时间")
	private Date endTime;

	@ApiModelProperty("请求时长")
	private Long reqTime;

	@ApiModelProperty("响应结果(1成功2失败)")
	@JsonSerialize(converter = RspResultEnum.Covert.class)
	private String rspResult;

	@ApiModelProperty("是否匹配(0否1是)")
	@JsonSerialize(converter = FlagEnum.Covert.class)
	private String matchPattern;

	private String lastReqId;

}
