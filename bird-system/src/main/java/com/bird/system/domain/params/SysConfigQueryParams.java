package com.bird.system.domain.params;

import com.bird.common.constant.BaseParams;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 参数配置表 sys_config
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysConfigQueryParams extends BaseParams {

	private static final long serialVersionUID = 1L;

	/**
	 * 参数名称
	 */
	private String configName;

	/**
	 * 参数键名
	 */
	private String configKey;

	/**
	 * 系统内置（Y是 N否）
	 */
	private String configType;

	private String endCreateTime;

	private String startCreateTime;
}
