package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.common.constant.Constants;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.util.Date;

/**
 * 岗位表 sys_post
 *
 * @author bird
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_post")
public class SysPostEntity extends BaseEntity {
	private static final long serialVersionUID = 1L;


	/**
	 * 岗位编码
	 */
	private String postCode;

	/**
	 * 岗位名称
	 */
	private String postName;

	/**
	 * 岗位排序
	 */
	private String postSort;

	/**
	 * 状态（0正常 1停用）
	 */
	private String status;

	/**
	 * 用户是否存在此岗位标识 默认不存在
	 */
	private boolean flag = false;

	/**
	 * 备注
	 */
	private String remark;

	/**
	 * 创建者
	 */
	private String createBy;

	/**
	 * 创建时间
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date createTime;

	/**
	 * 更新者
	 */
	private String updateBy;

	/**
	 * 更新时间 推广
	 */
	@JsonFormat(pattern = Constants.DatePattern.NORM_DATETIME_PATTERN)
	private Date updateTime;
}
