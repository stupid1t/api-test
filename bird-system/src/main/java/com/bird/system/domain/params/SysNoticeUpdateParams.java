package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.SysNoticeEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 通知公告表 sys_notice
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysNoticeUpdateParams extends BeanCovert<SysNoticeEntity> {

	/**
	 * 公告ID
	 */
	private Long id;

	/**
	 * 公告标题
	 */
	private String noticeTitle;

	/**
	 * 公告类型（1通知 2公告）
	 */
	private String noticeType;

	/**
	 * 公告内容
	 */
	private String noticeContent;

	/**
	 * 公告状态（0正常 1关闭）
	 */
	private String status;

	/**
	 * 备注
	 */
	private String remark;

}
