package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.SysCodeRuleEntity;
import com.bird.system.enums.CodeRuleTypeEnum;
import com.bird.system.validator.constraint.CustomConstraint;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import static com.bird.common.constant.ValidRule.Add;
import static com.bird.common.constant.ValidRule.Update;

/**
 * 系统编码规则配置对象 sys_code_rule
 *
 * @author bird
 * @date 2020-07-22
 */
@ApiModel("系统编码规则配置-修改")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysCodeRuleUpdateParam extends BeanCovert<SysCodeRuleEntity> {

	@ApiModelProperty("主键")
	@NotNull(message = "主键不能为空", groups = {Add.class, Update.class})
	private Long id;

	@ApiModelProperty("规则名称")
	@NotEmpty(message = "规则名称不能为空", groups = {Add.class, Update.class})
	@Size(max = 20, message = "规则名称长度超过{max}", groups = {Add.class, Update.class})
	private String ruleName;

	@ApiModelProperty("规则编码")
	@NotEmpty(message = "规则编码不能为空", groups = {Add.class, Update.class})
	@Size(max = 20, message = "规则编码长度超过{max}", groups = {Add.class, Update.class})
	private String ruleCode;

	@ApiModelProperty("编码前缀")
	@Size(max = 20, message = "编码前缀长度超过{max}", groups = {Add.class, Update.class})
	private String rulePrefix;

	@ApiModelProperty("编码类型(1日期+随机数;2日期+递增序列.;3递增序列;)")
	@NotEmpty(message = "编码类型不能为空", groups = {Add.class, Update.class})
	@Size(max = 2, message = "编码类型长度超过{max}", groups = {Add.class, Update.class})
	@CustomConstraint(CodeRuleTypeEnum.class)
	private String formatType;

	@ApiModelProperty("格式化格式")
	@NotEmpty(message = "格式化格式不能为空", groups = {Add.class, Update.class})
	@Size(max = 20, message = "格式化格式长度超过{max}", groups = {Add.class, Update.class})
	private String formatStr;

	@ApiModelProperty("当前日期")
	@NotEmpty(message = "当前日期不能为空", groups = {Add.class, Update.class})
	@Size(max = 20, message = "当前日期长度超过{max}", groups = {Add.class, Update.class})
	private String curFormatVal;

	@ApiModelProperty("序列占位长度")
	@NotEmpty(message = "序列占位长度不能为空", groups = {Add.class, Update.class})
	@Size(max = 20, message = "序列占位长度长度超过{max}", groups = {Add.class, Update.class})
	private Integer serialLen;

	@ApiModelProperty("当前序列值")
	@NotEmpty(message = "当前序列值不能为空", groups = {Add.class, Update.class})
	@Size(max = 20, message = "当前序列值长度超过{max}", groups = {Add.class, Update.class})
	private Integer serial;

	@ApiModelProperty("创建人")
	private Long creater;
}
