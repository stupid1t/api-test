package com.bird.system.domain.vo;

import com.bird.common.utils.covert.SystemUserCovert;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 组对象 ts_group
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("组-列表")
@Data
public class TsGroupListPcVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("组名称")
	private String theName;
	
	@ApiModelProperty("创建时间")
	private Date createTime;

	@ApiModelProperty("创建人")
	@JsonSerialize(converter = SystemUserCovert.class)
	private Long creSb;

}
