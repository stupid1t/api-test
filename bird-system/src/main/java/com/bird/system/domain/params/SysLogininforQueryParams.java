package com.bird.system.domain.params;


import com.bird.common.constant.BaseParams;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 系统访问记录表 sys_logininfor
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysLogininforQueryParams extends BaseParams {

	/**
	 * ID
	 */
	private Long id;

	/**
	 * 用户账号
	 */
	private String userName;

	/**
	 * 登录类型
	 */
	private String loginType;

	/**
	 * 登录状态 0成功 1失败
	 */
	private String status;

	/**
	 * 登录IP地址
	 */
	private String ipaddr;

	/**
	 * 登录地点
	 */
	private String loginLocation;

	/**
	 * 浏览器类型
	 */
	private String browser;

	/**
	 * 操作系统
	 */
	private String os;

	/**
	 * 提示消息
	 */
	private String msg;

	/**
	 * 访问时间
	 */
	private Date loginTime;

	private String endLoginTime;

	private String startLoginTime;

}
