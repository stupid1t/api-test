package com.bird.system.domain.params;

import com.bird.common.constant.BaseParams;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单权限表 sys_menu
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysMenuQueryParams extends BaseParams {

	/**
	 * 菜单ID
	 */
	private Long id;

	/**
	 * 菜单名称
	 */
	private String menuName;

	/**
	 * 父菜单名称
	 */
	private String parentName;

	/**
	 * 父菜单ID
	 */
	private Long parentId;

	/**
	 * 显示顺序
	 */
	private String orderNum;

	/**
	 * 路由地址
	 */
	private String path;

	/**
	 * 组件路径
	 */
	private String component;

	/**
	 * 是否为外链（0是 1否）
	 */
	private String isFrame;

	/**
	 * 类型（M目录 C菜单 F按钮）
	 */
	private String menuType;

	/**
	 * 菜单状态:0显示,1隐藏
	 */
	private String visible;

	/**
	 * 权限字符串
	 */
	private String perms;

	/**
	 * 菜单图标
	 */
	private String icon;

	/**
	 * 子菜单
	 */
	private List<SysMenuQueryParams> children = new ArrayList<SysMenuQueryParams>();

	/**
	 * 备注
	 */
	private String remark;
}
