package com.bird.system.domain.params;

import com.bird.common.constant.BaseParams;
import com.bird.system.domain.entity.SysDeptEntity;
import com.bird.system.domain.entity.SysRoleEntity;
import com.bird.system.enums.SexEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * 用户对象 sys_user
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUserQueryParams extends BaseParams {

	/**
	 * 用户ID
	 */
	private Long userId;

	/**
	 * 部门ID
	 */
	private Long deptId;

	/**
	 * 用户账号
	 */
	private String userName;

	/**
	 * 用户昵称
	 */
	private String nickName;

	/**
	 * 用户邮箱
	 */
	private String email;

	/**
	 * 手机号码
	 */
	private String phonenumber;

	/**
	 * 用户性别
	 */
	@JsonSerialize(converter = SexEnum.Covert.class)
	private String sex;

	/**
	 * 用户头像
	 */
	private String avatar;

	/**
	 * 密码
	 */
	private String password;

	/**
	 * 盐加密
	 */
	private String salt;

	/**
	 * 帐号状态（0正常 1停用）
	 */
	private String status;

	/**
	 * 删除标志（0代表存在 2代表删除）
	 */
	private String delFlag;

	/**
	 * 最后登陆IP
	 */
	private String loginIp;

	/**
	 * 最后登陆时间
	 */
	private Date loginDate;

	/**
	 * 部门对象
	 */
	private SysDeptEntity dept;

	/**
	 * 角色对象
	 */
	private List<SysRoleEntity> roles;

	/**
	 * 角色组
	 */
	private Long[] roleIds;

	/**
	 * 岗位组
	 */
	private Long[] postIds;

	/**
	 * 备注
	 */
	private String remark;

	private String startCreateTime;

	private String endCreateTime;

}
