package com.bird.system.domain.params;

/**
 * 登录对象
 *
 * @author: 625
 * @version: 2020年07月10日 15:38
 */

import javax.validation.constraints.NotEmpty;

/**
 * 用户登录对象
 *
 * @author ruoyi
 */
public class LoginBodyParam {
	/**
	 * 用户名
	 */
	@NotEmpty
	private String username;

	/**
	 * 用户密码
	 */
	@NotEmpty
	private String password;

	/**
	 * 验证码
	 */
	private String code;

	/**
	 * 唯一标识
	 */
	private String uuid = "";

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
