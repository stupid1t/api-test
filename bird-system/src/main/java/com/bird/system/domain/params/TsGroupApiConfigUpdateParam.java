package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.TsGroupApiConfigEntity;
import com.bird.system.enums.MatchTypeEnum;
import com.bird.system.enums.ProxyHostEnum;
import com.bird.system.validator.constraint.CustomConstraint;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.bird.common.constant.ValidRule.Add;
import static com.bird.common.constant.ValidRule.Update;

/**
 * 请求任务配置对象 ts_group_api_config
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("请求任务配置-修改")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TsGroupApiConfigUpdateParam extends BeanCovert<TsGroupApiConfigEntity> {

	@ApiModelProperty("主键")
	@NotNull(message = "主键不能为空", groups = {Add.class, Update.class})
	private Long id;

	@ApiModelProperty("接口ID")
	@NotNull(message = "接口ID不能为空", groups = {Add.class, Update.class})
	private Long apiId;

	@ApiModelProperty("延时请求时间")
	@NotNull(message = "超时时间不能为空", groups = {Add.class, Update.class})
	private Long intervalTime;

	@ApiModelProperty("超时时间")
	@NotNull(message = "超时时间不能为空", groups = {Add.class, Update.class})
	private Long timeOut;

	@ApiModelProperty("线程数")
	@NotNull(message = "线程数不能为空", groups = {Add.class, Update.class})
	private Integer threadNum;

	@ApiModelProperty("请求数据ID")
	@NotEmpty(message = "请求数据ID不能为空", groups = {Add.class, Update.class})
	@Size(max = 200, message = "请求数据ID长度超过{max}", groups = {Add.class, Update.class})
	private String reqDataId;
	
	@ApiModelProperty("存储结果配置")
	private String storeVar;

	@ApiModelProperty("额外变量")
	@Size(max = 200, message = "额外变量长度超过{max}", groups = {Add.class, Update.class})
	@Pattern(regexp = "([^:]+:[^\\n:]+)+", message = "额外变量参数格式不正确!")
	private String extraVar;

	@ApiModelProperty("结果匹配模式(1全文匹配,2正则匹配,3json匹配)")
	@NotEmpty(message = "结果匹配模式不能为空", groups = {Add.class, Update.class})
	@Size(max = 2, message = "结果匹配模式长度超过{max}", groups = {Add.class, Update.class})
	@CustomConstraint(MatchTypeEnum.class)
	private String matchType;

	@ApiModelProperty("代理服务器")
	@CustomConstraint(ProxyHostEnum.class)
	private String proxyHost;

	@ApiModelProperty("结果匹配表达式")
	@NotEmpty(message = "结果匹配表达式不能为空", groups = {Add.class, Update.class})
	@Size(max = 2000, message = "结果匹配表达式长度超过{max}", groups = {Add.class, Update.class})
	private String matchPattern;

	@ApiModelProperty("分页大小")
	@NotNull(message = "分页大小不能为空", groups = {Add.class, Update.class})
	private Long pageSize;

	@ApiModelProperty("是否保留请求结果")
	private String isRetainResults;

	@ApiModelProperty("任务配置名称")
	@NotEmpty(message = "任务配置名称不能为空", groups = {Add.class, Update.class})
	@Size(max = 255, message = "任务配置名称长度超过{max}", groups = {Add.class, Update.class})
	private String configName;

}
