package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.system.enums.CodeRuleTypeEnum;
import com.bird.system.enums.FlagEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import tk.mybatis.mapper.annotation.LogicDelete;

import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 系统编码规则配置对象 sys_code_rule
 *
 * @author bird
 * @date 2020-07-22
 */
@ApiModel("系统编码规则配置-实体")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_code_rule")
public class SysCodeRuleEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("规则名称")
	private String ruleName;

	@ApiModelProperty("规则编码")
	private String ruleCode;

	@ApiModelProperty("编码前缀")
	private String rulePrefix;

	@ApiModelProperty("编码类型(1日期+随机数;2日期+递增序列.;3递增序列;)")
	@JsonSerialize(converter = CodeRuleTypeEnum.Covert.class)
	private String formatType;

	@ApiModelProperty("格式化格式")
	private String formatStr;

	@ApiModelProperty("当前日期")
	private String curFormatVal;

	@ApiModelProperty("序列占位长度")
	private Integer serialLen;

	@ApiModelProperty("当前序列值")
	private Integer serial;

	@ApiModelProperty("创建人")
	private Long creater;

	@ApiModelProperty("创建时间")
	private Date createTime;

	@ApiModelProperty("删除标志")
	@JsonSerialize(converter = FlagEnum.Covert.class)
	@LogicDelete
	private String deleted;

}
