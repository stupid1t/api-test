package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.TsReqLogEntity;
import com.bird.system.enums.FlagEnum;
import com.bird.system.enums.RspResultEnum;
import com.bird.system.validator.constraint.CustomConstraint;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

import static com.bird.common.constant.ValidRule.Add;
import static com.bird.common.constant.ValidRule.Update;

/**
 * 请求日志对象 ts_req_log
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("请求日志-修改")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TsReqLogUpdateParam extends BeanCovert<TsReqLogEntity> {

	@ApiModelProperty("主键")
	@NotNull(message = "主键不能为空", groups = {Add.class, Update.class})
	private Long id;

	@ApiModelProperty("请求配置ID")
	@NotNull(message = "请求配置ID不能为空", groups = {Add.class, Update.class})
	private Long configId;

	@ApiModelProperty("url参数")
	@Size(max = 500, message = "url参数长度超过{max}", groups = {Add.class, Update.class})
	private String urlParam;

	@ApiModelProperty("body参数")
	@Size(max = 500, message = "body参数长度超过{max}", groups = {Add.class, Update.class})
	private String bodyParam;

	@ApiModelProperty("header参数")
	@Size(max = 500, message = "header参数长度超过{max}", groups = {Add.class, Update.class})
	private String headerParam;

	@ApiModelProperty("开始时间")
	@NotNull(message = "开始时间不能为空", groups = {Add.class, Update.class})
	private Date startTime;

	@ApiModelProperty("结束时间")
	private Date endTime;

	@ApiModelProperty("请求时长")
	@NotNull(message = "请求时长不能为空", groups = {Add.class, Update.class})
	private Long reqTime;

	@ApiModelProperty("响应结果(1成功2失败)")
	@NotEmpty(message = "响应结果不能为空", groups = {Add.class, Update.class})
	@Size(max = 2, message = "响应结果长度超过{max}", groups = {Add.class, Update.class})
	@CustomConstraint(RspResultEnum.class)
	private String rspResult;

	@ApiModelProperty("是否匹配(0否1是)")
	@NotEmpty(message = "是否匹配不能为空", groups = {Add.class, Update.class})
	@Size(max = 2, message = "是否匹配长度超过{max}", groups = {Add.class, Update.class})
	@CustomConstraint(FlagEnum.class)
	private String matchPattern;

	@ApiModelProperty("响应内容")
	@NotEmpty(message = "响应内容不能为空", groups = {Add.class, Update.class})
	@Size(max = 5000, message = "响应内容长度超过{max}", groups = {Add.class, Update.class})
	private String rspBody;

}
