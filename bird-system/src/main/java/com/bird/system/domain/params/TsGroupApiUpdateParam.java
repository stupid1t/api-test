package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.TsGroupApiEntity;
import com.bird.system.enums.ReqContentTypeEnum;
import com.bird.system.enums.ReqHttpMethodEnum;
import com.bird.system.validator.constraint.CustomConstraint;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import static com.bird.common.constant.ValidRule.Add;
import static com.bird.common.constant.ValidRule.Update;

/**
 * 组接口对象 ts_group_api
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("组接口-修改")
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TsGroupApiUpdateParam extends BeanCovert<TsGroupApiEntity> {

	@ApiModelProperty("主键")
	private Long id;

	@ApiModelProperty("隶属组")
	@NotNull(message = "隶属组不能为空", groups = {Add.class, Update.class})
	private Long groupId;

	@ApiModelProperty("名称")
	@NotEmpty(message = "名称不能为空", groups = {Add.class, Update.class})
	@Size(max = 20, message = "名称长度超过{max}", groups = {Add.class, Update.class})
	private String theName;

	@ApiModelProperty("地址")
	@NotEmpty(message = "地址不能为空", groups = {Add.class, Update.class})
	@Size(max = 3000, message = "地址长度超过{max}", groups = {Add.class, Update.class})
	@Pattern(regexp = "[a-zA-z]+://[^\\s]*", message = "请求地址不正确!")
	private String theUrl;

	@ApiModelProperty("URL参数")
	@Size(max = 3000, message = "URL参数长度超过{max}", groups = {Add.class, Update.class})
	@Pattern(regexp = "([^:]+:[^\\n:]+)+", message = "URL参数格式不正确!")
	private String urlParam;

	@ApiModelProperty("Body参数")
	@Size(max = 3000, message = "Body参数长度超过{max}", groups = {Add.class, Update.class})
	@Pattern(regexp = "([^:]+:[^\\n:]+)+", message = "Body参数格式不正确!")
	private String bodyParam;

	@ApiModelProperty("Header参数")
	@Size(max = 3000, message = "Header参数长度超过{max}", groups = {Add.class, Update.class})
	@Pattern(regexp = "([^:]+:[^\\n:]+)+", message = "UHeader参数格式不正确!")
	private String headerParam;

	@ApiModelProperty("请求协议类型")
	@NotEmpty(message = "请求协议类型不能为空", groups = {Add.class, Update.class})
	@Size(max = 200, message = "请求协议类型长度超过{max}", groups = {Add.class, Update.class})
	@CustomConstraint(ReqContentTypeEnum.class)
	private String contentType;

	@ApiModelProperty("请求方式")
	@NotEmpty(message = "请求方式不能为空", groups = {Add.class, Update.class})
	@Size(max = 20, message = "请求方式长度超过{max}", groups = {Add.class, Update.class})
	@CustomConstraint(ReqHttpMethodEnum.class)
	private String theMethod;

}
