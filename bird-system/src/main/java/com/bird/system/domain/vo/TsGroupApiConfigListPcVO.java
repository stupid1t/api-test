package com.bird.system.domain.vo;

import com.bird.common.utils.covert.ApiIdCovert;
import com.bird.common.utils.covert.GroupIdCovert;
import com.bird.system.enums.ReqStatusEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 请求任务配置对象 ts_group_api_config
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("请求任务配置-列表")
@Data
public class TsGroupApiConfigListPcVO implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("主键")
	private Long id;

	private String configName;

	@ApiModelProperty("组Id")
	@JsonSerialize(converter = GroupIdCovert.class)
	private Long groupId;

	@ApiModelProperty("接口ID")
	@JsonSerialize(converter = ApiIdCovert.class)
	private Long apiId;

	@ApiModelProperty("线程数")
	private Integer threadNum;

	@ApiModelProperty("成功次数")
	private Long finishNum;

	@ApiModelProperty("终止次数")
	private Long stopNum;

	@ApiModelProperty("发起次数")
	private Long startNum;

	@ApiModelProperty("当前页")
	private Long pageNum;

	@ApiModelProperty("总页数")
	private Long pageTotal;

	@ApiModelProperty("任务状态(1未开始2已暂停3请求中)")
	@JsonSerialize(converter = ReqStatusEnum.Covert.class)
	private String reqStatus;

	private String lastReqId;
}
