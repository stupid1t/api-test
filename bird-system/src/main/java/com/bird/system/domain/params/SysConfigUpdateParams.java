package com.bird.system.domain.params;

import com.bird.common.constant.BeanCovert;
import com.bird.system.domain.entity.SysConfigEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 参数配置表 sys_config
 *
 * @author bird
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysConfigUpdateParams extends BeanCovert<SysConfigEntity> {

	/**
	 * 参数主键
	 */
	private Long id;

	/**
	 * 参数名称
	 */
	private String configName;

	/**
	 * 参数键名
	 */
	private String configKey;

	/**
	 * 参数键值
	 */
	private String configValue;

	/**
	 * 系统内置（Y是 N否）
	 */
	private String configType;

	/**
	 * 备注
	 */
	private String remark;


	/**
	 * 创建者
	 */
	private String createBy;


	/**
	 * 更新者
	 */
	private String updateBy;

}
