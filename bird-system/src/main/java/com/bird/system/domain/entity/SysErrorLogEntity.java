package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 异常日志对象 sys_error_log
 *
 * @author bird
 * @date 2020-07-15
 */
@ApiModel("异常日志-实体")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sys_error_log")
public class SysErrorLogEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("请求URI")
	private String requestUri;

	@ApiModelProperty("请求方式")
	private String requestMethod;

	@ApiModelProperty("请求参数")
	private String requestParams;

	@ApiModelProperty("用户代理")
	private String userAgent;

	@ApiModelProperty("操作IP")
	private String ip;

	@ApiModelProperty("操作IP地址")
	private String ipAddress;

	@ApiModelProperty("异常信息")
	private String errorInfo;

	@ApiModelProperty("简要异常信息")
	private String errorSimpleInfo;
	
	@ApiModelProperty("创建者")
	private Long creator;

	@ApiModelProperty("创建时间")
	private Date createDate;

	@ApiModelProperty("创建人名")
	private String creatorName;

}
