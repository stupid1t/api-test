package com.bird.system.domain.entity;

import com.alibaba.fastjson.JSONObject;
import com.bird.common.constant.BaseEntity;
import com.bird.system.enums.FlagEnum;
import com.bird.system.enums.RspResultEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.util.Date;

/**
 * 请求日志对象 ts_req_log
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("请求日志-实体")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ts_req_log")
public class TsReqLogEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("请求配置ID")
	private Long configId;

	@ApiModelProperty("请求配置名字")
	private String configName;

	@ApiModelProperty("数据组")
	private String groupName;

	@ApiModelProperty("数据")
	private String groupData;

	@ApiModelProperty("请求Url")
	private String theUrl;

	@ApiModelProperty("url参数")
	private String urlParam;

	@ApiModelProperty("body参数")
	private String bodyParam;

	@ApiModelProperty("header参数")
	private String headerParam;

	@ApiModelProperty("开始时间")
	private Date startTime;

	@ApiModelProperty("结束时间")
	private Date endTime;

	@ApiModelProperty("请求时长")
	private Long reqTime;

	@ApiModelProperty("响应结果(1成功2失败)")
	@JsonSerialize(converter = RspResultEnum.Covert.class)
	private String rspResult;

	@ApiModelProperty("是否匹配(0否1是)")
	@JsonSerialize(converter = FlagEnum.Covert.class)
	private String matchPattern;

	@ApiModelProperty("响应内容")
	private String rspBody;

	@ApiModelProperty("保存内容")
	private String storeVal;

	@ApiModelProperty("保存内容")
	@Transient
	private JSONObject storeValObject;

	@ApiModelProperty("请求标志")
	private String lastReqId;

	@ApiModelProperty("创建人")
	private Long creSb;
}
