package com.bird.system.domain.entity;

import com.bird.common.constant.BaseEntity;
import com.bird.system.enums.MatchTypeEnum;
import com.bird.system.enums.ProxyHostEnum;
import com.bird.system.enums.ReqStatusEnum;
import com.bird.system.enums.SysYesOrNoEnum;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Table;
import java.io.Serializable;
import java.util.Date;

/**
 * 请求任务配置对象 ts_group_api_config
 *
 * @author bird
 * @date 2020-09-23
 */
@ApiModel("请求任务配置-实体")
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "ts_group_api_config")
public class TsGroupApiConfigEntity extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@ApiModelProperty("接口ID")
	private Long apiId;

	@ApiModelProperty("延时请求时间")
	private Long intervalTime;

	@ApiModelProperty("超时时间")
	private Long timeOut;

	@ApiModelProperty("线程数")
	private Integer threadNum;

	@ApiModelProperty("请求数据ID")
	private String reqDataId;

	@ApiModelProperty("额外变量")
	private String extraVar;

	@ApiModelProperty("结果匹配模式(1全文匹配,2正则匹配,3json匹配)")
	@JsonSerialize(converter = MatchTypeEnum.Covert.class)
	private String matchType;

	@ApiModelProperty("代理服务器")
	@JsonSerialize(converter = ProxyHostEnum.Covert.class)
	private String proxyHost;

	@ApiModelProperty("结果匹配表达式")
	private String matchPattern;

	@ApiModelProperty("存储结果配置")
	private String storeVar;

	@ApiModelProperty("成功次数")
	private Long finishNum;

	@ApiModelProperty("终止次数")
	private Long stopNum;

	@ApiModelProperty("发起次数")
	private Long startNum;

	@ApiModelProperty("分页大小")
	private Long pageSize;

	@ApiModelProperty("当前页")
	private Long pageNum;

	@ApiModelProperty("总页数")
	private Long pageTotal;

	@ApiModelProperty("任务状态(1未开始2已暂停3请求中)")
	@JsonSerialize(converter = ReqStatusEnum.Covert.class)
	private String reqStatus;

	@ApiModelProperty("最近一次请求ID标志")
	private String lastReqId;

	@ApiModelProperty("创建时间")
	private Date createTime;

	@ApiModelProperty("创建人")
	private Long creSb;

	@ApiModelProperty("是否保留请求结果")
	@JsonSerialize(converter = SysYesOrNoEnum.Covert.class)
	private String isRetainResults;

	@ApiModelProperty("任务配置名称")
	private String configName;

}
