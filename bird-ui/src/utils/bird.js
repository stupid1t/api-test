/**
 * 通用js方法封装处理
 * Copyright (c) 2019 bird
 */

const baseURL = process.env.VUE_APP_BASE_API

// 日期格式化
export function parseTime(time, pattern) {
  if (arguments.length === 0 || !time) {
    return null
  }
  const format = pattern || '{y}-{m}-{d} {h}:{i}:{s}'
  let date
  if (typeof time === 'object') {
    date = time
  } else {
    if ((typeof time === 'string') && (/^[0-9]+$/.test(time))) {
      time = parseInt(time)
    }
    if ((typeof time === 'number') && (time.toString().length === 10)) {
      time = time * 1000
    }
    date = new Date(time)
  }
  const formatObj = {
    y: date.getFullYear(),
    m: date.getMonth() + 1,
    d: date.getDate(),
    h: date.getHours(),
    i: date.getMinutes(),
    s: date.getSeconds(),
    a: date.getDay()
  }
  const time_str = format.replace(/{(y|m|d|h|i|s|a)+}/g, (result, key) => {
    let value = formatObj[key]
    // Note: getDay() returns 0 on Sunday
    if (key === 'a') {
      return ['日', '一', '二', '三', '四', '五', '六'][value]
    }
    if (result.length > 0 && value < 10) {
      value = '0' + value
    }
    return value || 0
  })
  return time_str
}

// 表单重置
export function resetForm(refName) {
  if (this.$refs[refName]) {
    this.$refs[refName].resetFields();
    this.$refs[refName].clearValidate();
  }
}

// 表单重置
export function clearForm(refName) {
  if (this.$refs[refName]) {
    this.$refs[refName].clearValidate();
  }
}

// 对象属性重置
export function resetPropField(params, extraConfig) {
  for (let field in params) {
    params[field] = undefined;
  }
  Object.assign(params, params, extraConfig);
}

// 添加日期范围
export function addDateRangeField(params, dateRange, fieldName) {
  if (null != dateRange && '' != dateRange) {
    params["start" + fieldName] = dateRange[0];
    params["end" + fieldName] = dateRange[1];
  }
  return params;
}

// 添加日期范围
export function handleSort(queryParam, sorter) {
  if (!sorter.order) {
    queryParam.orderByColumn = 'id';
    queryParam.isAsc = 'desc';
  } else {
    queryParam.orderByColumn = sorter.prop;
    queryParam.isAsc = sorter.order.replace('ending', '');
  }
}

// 回显数据字典
export function selectDictLabel(datas, value) {
  var actions = [];
  Object.keys(datas).map((key) => {
    if (datas[key].dictValue == ('' + value)) {
      actions.push(datas[key].dictLabel);
      return false;
    }
  })
  return actions.join('');
}

// 通用下载方法
export function download(url, query) {
  window.location.href = baseURL + url + transParams(query);
}

/**
 * 参数处理
 * @param {*} params  参数
 */
export function transParams(params) {
  let result = '?'
  Object.keys(params).forEach((key) => {
    if (!Object.is(params[key], undefined) && !Object.is(params[key], null)) {
      result += encodeURIComponent(key) + '=' + encodeURIComponent(params[key]) + '&'
    }
  })
  return result
}

// 字符串格式化(%s )
export function sprintf(str) {
  var args = arguments, flag = true, i = 1;
  str = str.replace(/%s/g, function () {
    var arg = args[i++];
    if (typeof arg === 'undefined') {
      flag = false;
      return '';
    }
    return arg;
  });
  return flag ? str : '';
}

// 转换字符串，undefined,null等转化为""
export function praseStrEmpty(str) {
  if (!str || str == "undefined" || str == "null") {
    return "";
  }
  return str;
}
