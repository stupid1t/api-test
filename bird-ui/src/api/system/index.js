import request from '@/utils/request'

// 查询用户列表
export function indexInfo() {
  return request({
    url: '/system/index/info',
    method: 'get'
  })
}

