import request from '@/utils/request'

// 查询数据规则列表
export function listRule(query) {
  return request({
    url: '/system/rule/list',
    method: 'get',
    params: query
  })
}

// 查询数据规则详细
export function getRule(theKey) {
  return request({
    url: '/system/rule/' + theKey,
    method: 'get'
  })
}

// 新增数据规则
export function addRule(data) {
  return request({
    url: '/system/rule',
    method: 'post',
    data: data
  })
}

// 修改数据规则
export function updateRule(data) {
  return request({
    url: '/system/rule',
    method: 'put',
    data: data
  })
}

// 删除数据规则
export function delRule(theKey) {
  return request({
    url: '/system/rule/' + theKey,
    method: 'delete'
  })
}

// 导出数据规则
export function exportRule(query) {
  return request({
    url: '/system/rule/export',
    method: 'get',
    params: query
  })
}