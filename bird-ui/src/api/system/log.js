import request from '@/utils/request'

// 查询请求日志列表
export function listLog(query) {
  return request({
    url: '/system/reqLog/list',
    method: 'get',
    params: query
  })
}

// 查询请求日志详细
export function getLog(id) {
  return request({
    url: '/system/reqLog/' + id,
    method: 'get'
  })
}

// 新增请求日志
export function addLog(data) {
  return request({
    url: '/system/reqLog',
    method: 'post',
    data: data
  })
}

// 修改请求日志
export function updateLog(data) {
  return request({
    url: '/system/reqLog',
    method: 'put',
    data: data
  })
}

// 删除请求日志
export function delLog(id) {
  return request({
    url: '/system/reqLog/' + id,
    method: 'delete'
  })
}

// 删除请求日志
export function delLogData(pwd) {
  return request({
    url: '/system/reqLog/clean/' + pwd,
    method: 'delete'
  })
}
