import request from '@/utils/request'

// 查询数据列表
export function listData(query) {
  return request({
    url: '/system/data/list',
    method: 'get',
    params: query
  })
}

// 查询数据详细
export function getData(id) {
  return request({
    url: '/system/data/' + id,
    method: 'get'
  })
}

// 新增数据
export function addData(data) {
  return request({
    url: '/system/data',
    method: 'post',
    data: data
  })
}

// 修改数据
export function updateData(data) {
  return request({
    url: '/system/data',
    method: 'put',
    data: data
  })
}

// 删除数据
export function delData(id) {
  return request({
    url: '/system/data/' + id,
    method: 'delete'
  })
}

// 删除数据
export function delDataGroup(name, pwd) {
  return request({
    url: '/system/data/group/' + name + "/" + pwd,
    method: 'delete'
  })
}

// 查询数据列表
export function dropDownData() {
  return request({
    url: '/system/data/dropdown',
    method: 'get'
  })
}
