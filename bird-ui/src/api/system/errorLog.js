import request from '@/utils/request'
import {download} from '@/utils/bird'

// 查询异常日志列表
export function listErrorLog(query) {
  return request({
    url: '/system/errorLog/list',
    method: 'get',
    params: query
  })
}

// 导出异常日志列表
export function exportErrorLog(query) {
  download("/system/errorLog/export/excel",query);
}

// 查询异常日志详细
export function getErrorLog(id) {
  return request({
    url: '/system/errorLog/' + id,
    method: 'get'
  })
}

// 删除异常日志
export function delErrorLog(id) {
  return request({
    url: '/system/errorLog/' + id,
    method: 'delete'
  })
}
