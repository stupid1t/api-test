import request from '@/utils/request'

// 查询请求任务配置列表
export function listConfig(query) {
  return request({
    url: '/system/reqConfig/list',
    method: 'get',
    params: query
  })
}

// 查询请求任务配置详细
export function getConfig(id) {
  return request({
    url: '/system/reqConfig/' + id,
    method: 'get'
  })
}

// 新增请求任务配置
export function addConfig(data) {
  return request({
    url: '/system/reqConfig',
    method: 'post',
    data: data
  })
}

// 修改请求任务配置
export function updateConfig(data) {
  return request({
    url: '/system/reqConfig',
    method: 'put',
    data: data
  })
}

// 删除请求任务配置
export function delConfig(id) {
  return request({
    url: '/system/reqConfig/' + id,
    method: 'delete'
  })
}

// 开始执行
export function startConfig(id) {
  return request({
    url: '/system/reqConfig/start/' + id,
    method: 'post'
  })
}

// 暂停执行
export function pauseConfig(id) {
  return request({
    url: '/system/reqConfig/pause/' + id,
    method: 'post'
  })
}

// 继续执行
export function continueConfig(id) {
  return request({
    url: '/system/reqConfig/continue/' + id,
    method: 'post'
  })
}

// 停止执行
export function stopConfig(id) {
  return request({
    url: '/system/reqConfig/stop/' + id,
    method: 'post'
  })
}
