import request from '@/utils/request'

// 查询系统编码规则配置列表
export function listCodeRule(query) {
  return request({
    url: '/system/codeRule/list',
    method: 'get',
    params: query
  })
}

// 查询系统编码规则配置详细
export function getCodeRule(id) {
  return request({
    url: '/system/codeRule/' + id,
    method: 'get'
  })
}

// 新增系统编码规则配置
export function addCodeRule(data) {
  return request({
    url: '/system/codeRule',
    method: 'post',
    data: data
  })
}

// 修改系统编码规则配置
export function updateCodeRule(data) {
  return request({
    url: '/system/codeRule',
    method: 'put',
    data: data
  })
}

// 删除系统编码规则配置
export function delCodeRule(id) {
  return request({
    url: '/system/codeRule/' + id,
    method: 'delete'
  })
}