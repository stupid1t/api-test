import request from '@/utils/request'

// 查询组接口列表
export function listApi(query) {
  return request({
    url: '/system/reqApi/list',
    method: 'get',
    params: query
  })
}

// 查询组接口详细
export function getApi(id) {
  return request({
    url: '/system/reqApi/' + id,
    method: 'get'
  })
}

// 新增组接口
export function addApi(data) {
  return request({
    url: '/system/reqApi',
    method: 'post',
    data: data
  })
}

// 修改组接口
export function updateApi(data) {
  return request({
    url: '/system/reqApi',
    method: 'put',
    data: data
  })
}

// 删除组接口
export function delApi(id) {
  return request({
    url: '/system/reqApi/' + id,
    method: 'delete'
  })
}
