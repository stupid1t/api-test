
## 开发

```bash
# 进入项目目录
cd bird-ui

# 安装yarn
npm install -g yarn

# 配置镜像
yarn config set registry https://registry.npm.taobao.org -g
yarn config set sass_binary_site http://cdn.npm.taobao.org/dist/node-sass -g

# 安装依赖
yarn install

# 启动服务
yarn run dev
```

## 发布


1. 静态资源路径确定位置vue.config.js(目前不需要改)
    ```
    publicPath: './' //请根据自己路径来配置更改
    ```
2. 打包环境确定,修改.env(目前不需要改)

3. 打包编译项目，发布服务器

    ```$xslt
    yarn run build:prod
    ```

4. tomcat部署静态文件，`切记发布时，不要删除WEB-INF，只删除index.html和static就可以了`
    1. 在webapps目录下创建应用文件夹
    2. 新建建WEB-INF文件夹，在里面添加web.xml文件
    3. 复制bird-ui\dist目录到tomcat的webapps目录，并更名为api-test
    ```
    <?xml version="1.0" encoding="UTF-8"?>
    <web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee" 
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
            http://xmlns.jcp.org/xml/ns/javaee/web-app_3_1.xsd"
            version="3.1" metadata-complete="true">
         <display-name>Router for Tomcat</display-name>
         <error-page>
            <error-code>404</error-code>
            <location>/index.html</location>
        </error-page>
    </web-app>
    ```
   
 5. nginx部署静态文件, 在nginx.conf添加以下location，之后复制bird-ui\dist目录到nginx的html目录，并更名为api-test
    ```
    location /api-test {
        root html;
        try_files $uri $uri/ @api-test;
    }

    location @api-test {
        rewrite ^/(api-test)/(.+)$ /$1/index.html last;
    }
    ```